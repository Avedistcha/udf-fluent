# User DEFINE_PROFILE
You can use DEFINE_PROFILE to define a custom boundary profile or cell zone condition that
varies as a function of spatial coordinates or time. Some of the variables you can customize are:

• velocity, pressure, temperature, turbulence kinetic energy, turbulence dissipation rate

• mass flux

• target mass flow rate as a function of physical flow time

• species mass fraction (species transport)

• volume fraction (multiphase models)

• wall thermal conditions (temperature, heat flux, heat generation rate, heat transfer coefficients, and
external emissivity, and so on)

• shell layer heat generation rate

• wall roughness conditions

• wall shear and stress conditions

• porosity

• porous resistance direction vector

• wall adhesion contact angle (VOF multiphase model)

• source terms

• fixed variables

Note that DEFINE_PROFILE allows you to modify only a single value for wall heat flux. Single
values are used in the explicit source term which ANSYS Fluent does not linearize. If you want to
linearize your source term for wall heat flux and account for conductive and radiative heat transfer
separately, you will need to use DEFINE_HEAT_FLUX to specify your UDF. :rocket:

## User Defined Physical properties Used for our sample code

| Argument Type     | Description|
| -------------     | -------------                                                  |
| **symbol name:**  | UDF name                                                       |
| **Thread *t**     | Pointer to thread on which boundary condition is to be applied.|
| **int i**         | Index that identifies the variable that is to be defined. i is set when you hook the UDF int i with a variable in a boundary conditions dialog box through the graphical user interface. This index is subsequently passed to your UDF by the ANSYS Fluent solver so that your function knows which variable to operate on.  |
| **Function returns**  | void  |


Note that unlike source term and property UDFs, profile UDFs (defined using DEFINE_PROFILE)
are not called by ANSYS Fluent from within a loop on threads in the boundary zone. The solver
passes only the pointer to the thread associated with the boundary zone to the DEFINE_PROFILE
macro. Your UDF will need to do the work of looping over all of the faces in the thread, computing
the face value for the boundary variable, and then storing the value in memory. 

F_PROFILE is typically used along with DEFINE_PROFILE and is a predefined macro supplied
by ANSYS Fluent. F_PROFILE stores a boundary condition in memory for a given face and thread
and is nested within the face loop as shown in the examples below. It is important to note that
the index i that is an argument to DEFINE_PROFILE is the same argument to F_PROFILE.
F_PROFILE uses the thread pointer t, face identifier f, and index i to set the appropriate
boundary face value in memory. 

# Note 

In multiphase cases a DEFINE_PROFILE UDF may be called more than once (particularly if the
profile is used in a mixture domain thread). If this must be avoided, then add the prefix MP_ to the
UDF name. The function will then be called only once even if it is used for more than one profile.


<img src="Images/rocket-launch.svg" width="300" height="300">
