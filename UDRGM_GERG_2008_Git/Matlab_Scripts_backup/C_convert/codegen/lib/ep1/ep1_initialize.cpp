//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// ep1_initialize.cpp
//
// Code generation for function 'ep1_initialize'
//

// Include files
#include "ep1_initialize.h"
#include "rt_nonfinite.h"

// Function Definitions
void ep1_initialize()
{
}

// End of code generation (ep1_initialize.cpp)
