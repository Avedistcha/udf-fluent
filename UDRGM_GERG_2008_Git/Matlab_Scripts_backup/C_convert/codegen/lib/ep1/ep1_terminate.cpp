//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// ep1_terminate.cpp
//
// Code generation for function 'ep1_terminate'
//

// Include files
#include "ep1_terminate.h"
#include "rt_nonfinite.h"

// Function Definitions
void ep1_terminate()
{
  // (no terminate code required)
}

// End of code generation (ep1_terminate.cpp)
