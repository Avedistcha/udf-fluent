//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// ep1_terminate.h
//
// Code generation for function 'ep1_terminate'
//

#ifndef EP1_TERMINATE_H
#define EP1_TERMINATE_H

// Include files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Function Declarations
extern void ep1_terminate();

#endif
// End of code generation (ep1_terminate.h)
