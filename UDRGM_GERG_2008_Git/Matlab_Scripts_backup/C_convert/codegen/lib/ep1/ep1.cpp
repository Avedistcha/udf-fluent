//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// ep1.cpp
//
// Code generation for function 'ep1'
//

// Include files
#include "ep1.h"
#include "rt_nonfinite.h"
#include <cmath>

// Function Declarations
static double rt_powd_snf(double u0, double u1);

// Function Definitions
static double rt_powd_snf(double u0, double u1)
{
  double y;
  if (std::isnan(u0) || std::isnan(u1)) {
    y = rtNaN;
  } else {
    double d;
    double d1;
    d = std::abs(u0);
    d1 = std::abs(u1);
    if (std::isinf(u1)) {
      if (d == 1.0) {
        y = 1.0;
      } else if (d > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = rtNaN;
    } else {
      y = std::pow(u0, u1);
    }
  }
  return y;
}

double ep1(double rho, double temp, double press)
{
  double b_y_tmp;
  double c_y_tmp;
  double d_y_tmp;
  double e_y_tmp;
  double f_y_tmp;
  double g_y_tmp;
  double h_y_tmp;
  double i_y_tmp;
  double j_y_tmp;
  double k_y_tmp;
  double l_y_tmp;
  double m_y_tmp;
  double n_y_tmp;
  double o_y_tmp;
  double p_y_tmp;
  double y_tmp;
  //  13eFNyWqxMj5LDRpqXdneS3rKqj4ET1YNo   LOLLLL it took me some time
  y_tmp = std::exp(-(3.5184372088832E+13 * rho) / 5.723089963823353E+15);
  b_y_tmp = 47641.0 / (250.0 * temp);
  c_y_tmp = rt_powd_snf(b_y_tmp, 1.125);
  d_y_tmp = rt_powd_snf(rho, 3.0);
  e_y_tmp = rt_powd_snf(b_y_tmp, 0.625);
  f_y_tmp = rho * rho;
  g_y_tmp =
      std::exp(-(1.2379400392853803E+27 * f_y_tmp) / 3.2753758734015589E+31);
  h_y_tmp = 2.4758800785707605E+27 * f_y_tmp / 3.2753758734015589E+31;
  i_y_tmp =
      std::exp(-(4.3556142965880123E+40 * d_y_tmp) / 1.8745270788813612E+47);
  j_y_tmp = 1.3066842889764037E+41 * d_y_tmp / 1.8745270788813612E+47;
  k_y_tmp = 3.5184372088832E+13 * rho / 5.723089963823353E+15;
  l_y_tmp = rt_powd_snf(rho, 6.0);
  m_y_tmp =
      std::exp(-(1.8971375900641885E+81 * l_y_tmp) / 3.5138517694594881E+94);
  n_y_tmp = 1.1382825540385131E+82 * l_y_tmp / 3.5138517694594881E+94;
  o_y_tmp = rt_powd_snf(rho, 5.0);
  p_y_tmp = rt_powd_snf(b_y_tmp, 4.5);
  return 1.42463478837067E+14 * rho * temp *
             (3.5184372088832E+13 * rho *
                  (((((((((((((((((((((((8.432645086898033E+15 * rho *
                                             rt_powd_snf(b_y_tmp, 0.375) /
                                             2.9302220614775567E+18 -
                                         3.953687836883237E+15 * rho * c_y_tmp /
                                             1.4651110307387784E+18) +
                                        5.164341124930299E+15 *
                                            rt_powd_snf(b_y_tmp, 0.125) /
                                            9.007199254740992E+15) -
                                       7.548342608634375E+15 * c_y_tmp /
                                           4.503599627370496E+15) +
                                      2.6141362792385794E+39 * d_y_tmp *
                                          rt_powd_snf(b_y_tmp, 1.5) /
                                          1.8745270788813612E+47) +
                                     2.8519171063819396E+39 * d_y_tmp *
                                         e_y_tmp / 1.8745270788813612E+47) -
                                    y_tmp * e_y_tmp *
                                        (8.91627063187057E+14 * rho /
                                             1.4651110307387784E+18 -
                                         0.098990489492918)) -
                                   y_tmp * rt_powd_snf(b_y_tmp, 2.625) *
                                       (2.629326254009491E+15 * rho /
                                            7.3255551536938918E+17 -
                                        0.58382770929055)) +
                                  y_tmp *
                                      (6.736202177543581E+15 * rho /
                                           1.4651110307387784E+18 -
                                       0.7478686756039) *
                                      rt_powd_snf(b_y_tmp, 2.75)) -
                                 2.3166719883517E+13 * rho * i_y_tmp *
                                     rt_powd_snf(b_y_tmp, 7.5) *
                                     (j_y_tmp - 2.0) / 7.3255551536938918E+17) -
                                1.0580402511005433E+39 * d_y_tmp * g_y_tmp *
                                    p_y_tmp * (h_y_tmp - 4.0) /
                                    1.8745270788813612E+47) -
                               1.574255569880779E+26 * f_y_tmp * g_y_tmp *
                                   rt_powd_snf(b_y_tmp, 4.75) *
                                   (h_y_tmp - 3.0) / 3.2753758734015589E+31) -
                              2.705159431197539E+15 * rho * y_tmp *
                                  rt_powd_snf(b_y_tmp, 2.125) *
                                  (k_y_tmp - 2.0) / 1.4651110307387784E+18) +
                             2.5123865486090966E+45 * f_y_tmp * g_y_tmp *
                                 (2.4758800785707605E+27 * (rho * rho) /
                                      3.2753758734015589E+31 -
                                  3.0) /
                                 (8.2608710004357171E+39 *
                                  rt_powd_snf(temp, 5.0))) +
                            7.1697044645141634E+53 * d_y_tmp * g_y_tmp *
                                (2.4758800785707605E+27 * (rho * rho) /
                                     3.2753758734015589E+31 -
                                 4.0) /
                                (3.4383787574569481E+52 *
                                 rt_powd_snf(temp, 4.0))) -
                           2.1869015617100785E+37 * d_y_tmp * i_y_tmp *
                               rt_powd_snf(b_y_tmp, 11.5) * (j_y_tmp - 4.0) /
                               1.7041155262557826E+46) +
                          1.0023789661631891E+66 * o_y_tmp * y_tmp *
                              rt_powd_snf(b_y_tmp, 1.75) * (k_y_tmp - 6.0) /
                              6.1397807682059109E+78) +
                         5.7828070488929945E+141 * l_y_tmp * m_y_tmp *
                             (n_y_tmp - 7.0) /
                             (1.4093456840194685E+121 *
                              rt_powd_snf(temp, 16.0))) +
                        2.9238011700047391E+161 * rt_powd_snf(rho, 4.0) *
                            m_y_tmp * (n_y_tmp - 5.0) /
                            (2.4244425022544578E+113 *
                             rt_powd_snf(temp, 26.0))) -
                       9.6198067548731718E+182 * o_y_tmp * m_y_tmp *
                           (n_y_tmp - 6.0) /
                           (3.9418473160566094E+131 *
                            rt_powd_snf(temp, 28.0))) +
                      2.2993888076714936E+192 * o_y_tmp * m_y_tmp *
                          (1.1382825540385131E+82 * rt_powd_snf(rho, 6.0) /
                               3.5138517694594881E+94 -
                           6.0) /
                          (3.0795682156692259E+136 * rt_powd_snf(temp, 30.0))) +
                     3.69757919918803E+84 * f_y_tmp * i_y_tmp *
                         (j_y_tmp - 3.0) /
                         (6.1548285189537087E+58 * rt_powd_snf(temp, 14.0))) +
                    5.686265908375535E+15 * rho * g_y_tmp * p_y_tmp *
                        (h_y_tmp - 2.0) / 5.8604441229551135E+18) -
                   1.2182494209440505E+33 * f_y_tmp * y_tmp * (k_y_tmp - 3.0) /
                       (4.2295659522230873E+33 * (temp * temp))) /
                  5.723089963823353E+15 +
              1.0) /
             2.74877906944E+11 -
         press;
}

// End of code generation (ep1.cpp)
