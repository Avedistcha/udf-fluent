//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// ep1.h
//
// Code generation for function 'ep1'
//

#ifndef EP1_H
#define EP1_H

// Include files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Function Declarations
extern double ep1(double rho, double temp, double press);

#endif
// End of code generation (ep1.h)
