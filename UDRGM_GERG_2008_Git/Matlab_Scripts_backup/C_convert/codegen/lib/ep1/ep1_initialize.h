//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// ep1_initialize.h
//
// Code generation for function 'ep1_initialize'
//

#ifndef EP1_INITIALIZE_H
#define EP1_INITIALIZE_H

// Include files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Function Declarations
extern void ep1_initialize();

#endif
// End of code generation (ep1_initialize.h)
