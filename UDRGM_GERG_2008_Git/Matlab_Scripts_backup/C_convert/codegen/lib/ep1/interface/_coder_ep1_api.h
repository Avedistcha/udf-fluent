//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// _coder_ep1_api.h
//
// Code generation for function 'ep1'
//

#ifndef _CODER_EP1_API_H
#define _CODER_EP1_API_H

// Include files
#include "emlrt.h"
#include "tmwtypes.h"
#include <algorithm>
#include <cstring>

// Variable Declarations
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

// Function Declarations
real_T ep1(real_T rho, real_T temp, real_T press);

void ep1_api(const mxArray *const prhs[3], const mxArray **plhs);

void ep1_atexit();

void ep1_initialize();

void ep1_terminate();

void ep1_xil_shutdown();

void ep1_xil_terminate();

#endif
// End of code generation (_coder_ep1_api.h)
