
#include<stdio.h>
#include <math.h>
#include <iostream>
using namespace std;
#define PCRIT 4.5992e+06 // (Pa) for Methane
int main() {
    double P=1.5*PCRIT;
    double Temp =370.588235;
    // double Visco1[] = {4.03072165890651e-07,-0.00100654210667834,1.02852622284534,-549.534050643264,161608.279291020,-24724435.7361364,1542991970.67037};
    // double Vis1 = Visco1[0]*pow(Temp,6)+ Visco1[1]*pow(Temp,5)+ Visco1[2]*pow(Temp,4)+ Visco1[3]*pow(Temp,3)+ Visco1[4]*pow(Temp,2)+ Visco1[5]*Temp+ Visco1[6];
    // double Visco2[] = {-3.74936615490581e-05,0.0834882835203674,-73.3263963492992,31725.2351116501,-6735022.08648053,573768341.066175};
    // double Vis2 = Visco2[0]*pow(Temp,5)+ Visco2[1]*pow(Temp,4)+ Visco2[2]*pow(Temp,3)+ Visco2[3]*pow(Temp,2)+ Visco2[4]*Temp+ Visco2[5];
    // double P1 = 1.0*PCRIT;
    // double P2 = 2.5*PCRIT;
    // double mu = (P2-P)*(Vis2-Vis1)/(P2-P1);
    double Visco1[] = {4.03072165890650e-13,-1.00654210667834e-09,1.02852622284534e-06,-0.000549534050643263,0.161608279291020,-24.7244357361364,1542.99197067037};
    double Vis1 = (Visco1[0]*pow(Temp,6)+ Visco1[1]*pow(Temp,5)+ Visco1[2]*pow(Temp,4)+ Visco1[3]*pow(Temp,3)+ Visco1[4]*pow(Temp,2)+ Visco1[5]*Temp+ Visco1[6]);
    double Visco2[] = {-3.74936615490582e-11,8.34882835203675e-08,-7.33263963492993e-05,0.0317252351116502,-6.73502208648054,573.768341066175};
    double Vis2 = (Visco2[0]*pow(Temp,5)+ Visco2[1]*pow(Temp,4)+ Visco2[2]*pow(Temp,3)+ Visco2[3]*pow(Temp,2)+ Visco2[4]*Temp+ Visco2[5]);
    double P1 = 1.0*PCRIT;
    double P2 = 2.5*PCRIT;
    double mu = (P-P2)*(Vis2-Vis1)/(P2-P1)+Vis2;

    cout << "PI = " << Visco1[0] << endl;
    printf("Temp %f, Vis1 %f, Vis2 %f, mu %f\n",Temp, Vis1, Vis2, mu);
    return 0;
}