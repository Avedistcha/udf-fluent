% In this script we intend to plot Cp vs Temperture for two different cases:
% 1) Cp Vs T for constnt Densities (figure 1)
% 2) Cp Vs T for Constant pressure (figure 2)
% It should be noted that for case 2 you are verifing the GERG Density and
% Cp models at the same time since to cmput cp for a constant P you need the
% right Density for that speciic P.
% So validating Fluent data Vs NIST for case 2 means validating the 
% Cp and Density


clear all; clc; close all;
PCRIT = 4.5992e+06;%Pa
Tmin=   150;
Tmax=   600;
rho = 300;%available data of NIST saved for 5,10,20,40,150,200,300,370 (Kg/m^3)

figure(1)

% .*****************NIST Data plot START*********************************
double pressure_factor;
pressure_factor = 1;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");

Viscosity_NIST = table2array(NIST(1:48,12));
Temp_NIST = table2array(NIST(1:48,1));
plot(Temp_NIST,Viscosity_NIST,  '--b')
hold on
% *****************NIST Data plot END**********************************

% *****************Polyfit Data plot START*********************************
syms T
k = 6;
format long
Visco1 = polyfit(Temp_NIST,Viscosity_NIST,k)
T = Temp_NIST;
Visco1 = [3.66414198468758e-13,-9.29488859860330e-10,9.63315568471851e-07,-0.000521178582330469,0.154945699846419,-23.9241331203175,1504.63781375729];
Visco1 = Visco1(1)*T.^(k)+ Visco1(2)*T.^(k-1)+ Visco1(3)*T.^(k-2)+ Visco1(4)*T.^(k-3)+ Visco1(5)*T.^(k-4)+ Visco1(6)*T.^(k-5)+ Visco1(7)*T.^(k-6);%+ Visco1(8)*T.^(k-7)+ Visco1(9)*T.^(k-8)+ Visco1(10)*T.^(k-9);
plot(Temp_NIST,Visco1, 'LineWidth',1.5);
% *****************Polyfit Data plot END*********************************

% *****************NIST Data plot START*********************************
pressure_factor = 1.5;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Viscosity_NIST = table2array(NIST(1:430,12));
Temp_NIST = table2array(NIST(1:430,1));
plot(Temp_NIST,Viscosity_NIST,  '--k')
hold on
% *****************NIST Data plot END**********************************


% *****************NIST Data plot START*********************************
pressure_factor = 2;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Viscosity_NIST = table2array(NIST(1:430,12));
Temp_NIST = table2array(NIST(1:430,1));
plot(Temp_NIST,Viscosity_NIST,  '--r')
hold on
% *****************NIST Data plot END**********************************


% *****************NIST Data plot START*********************************
pressure_factor = 2.5;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Viscosity_NIST = table2array(NIST(1:451,12));
Temp_NIST = table2array(NIST(1:451,1));
plot(Temp_NIST,Viscosity_NIST,  '--g')
hold on
% *****************NIST Data plot END**********************************

% *****************Polyfit Data plot START*********************************
pressure_factor = 2.5;
press = PCRIT*pressure_factor
k = 5;
Visco22 = polyfit(Temp_NIST,Viscosity_NIST,k);
Temp = Temp_NIST;
Visco2 = [-3.64280681827070e-11,8.16190884558272e-08,-7.20666369075405e-05,0.0313191470216509,-6.67263853262221,570.123618954823]
Visco2 = Visco2(1)*Temp.^5+ Visco2(2)*Temp.^4+ Visco2(3)*Temp.^3+ Visco2(4)*Temp.^2+ Visco2(5)*Temp.^1+ Visco2(6);
plot(Temp_NIST,Visco2, 'LineWidth',1.5);
% *****************Polyfit Data plot END*********************************

% *****************Calculation of Gas Viscosity by Sutherland's Formula START ********************************
%Sutherland's law with three coefficients has the form
double mu;
double mu0; % Viscosity of the gas at T0
mu0=1.1966e-05*1e6;
double T0; % Refference Temperature 
T0= 300;
double C; % Sutherland constant
C= 169.0; 
Temp=Tmin:1:Tmax;
mu = mu0*(T0+C)./(Temp+C).*(Temp/T0).^(3/2);
plot(Temp,mu,  '.-g')
% Sutherland's law with 2 coefficients has the form
% mu2= mu0.*Temp.^(3/2)./(Temp+T0);
% plot(Temp,mu2,  '--r')
% *****************Calculation of Gas Viscosity by Sutherland's Formula END **********************************

% *****************FLUENT Data plot START*********************************
double pressure_factor_FLUENT;
pressure_factor_FLUENT = 1.0;
FLUENT = readtable("FLUENT_Methane_T_Press_"+pressure_factor_FLUENT+"xPc.txt");
Viscosity_FLUENT = table2array(FLUENT(1:4999,6))*1e6;
Temp_FLUENT = table2array(FLUENT(1:4999,5));
scatter(Temp_FLUENT,Viscosity_FLUENT,  '*')
% *****************FLUENT Data plot END**********************************

title("Viscosity Vs Temperature, CH4 for different pressures or viscosity models")
legend("NIST Viscosity (CH4) at pressure 1xPc Pa", "polynomial fit Model at pressure 1xPc Pa","NIST Viscosity (CH4) at pressure 1.5xPc Pa ","NIST Viscosity (CH4) at pressure 2xPc Pa","NIST Viscosity (CH4) at pressure 2.5xPc Pa", "polynomial fit Model at pressure 2.5xPc Pa","Viscosity Sutherland Model with 3 coefficients","FLUENT Viscosity (CH4) at pressure "+pressure_factor_FLUENT+"xPc Pa",'Location','northeast','Orientation','horizontal','NumColumns',1,'FontSize',8);
xlabel('Temperature (K)')
ylabel('Viscosity (muPa.s)')
% xlim([Tmin Tmax])
% ylim([0 70])
grid on
print -dmeta
% Temp = 370.588235;
% Visco1 = [4.03072165890650e-13,-1.00654210667834e-09,1.02852622284534e-06,-0.000549534050643263,0.161608279291020,-24.7244357361364,1542.99197067037];
% Visco1 =  Visco1(1)*power(Temp,6)+ Visco1(2)*power(Temp,5)+ Visco1(3)*power(Temp,4)+ Visco1(4)*power(Temp,3)+ Visco1(5)*power(Temp,2)+ Visco1(6)*Temp+ Visco1(7)
% Visco2 = [-3.74936615490582e-11,8.34882835203675e-08,-7.33263963492993e-05,0.0317252351116502,-6.73502208648054,573.768341066175];
% Visco2 = Visco2(1)*Temp.^5+ Visco2(2)*Temp.^4+ Visco2(3)*Temp.^3+ Visco2(4)*Temp.^2+ Visco2(5)*Temp.^1+ Visco2(6)
% double P1;
% P1 = 1.0*PCRIT;
% double P2;
% P2 = 2.5*PCRIT;
% double mu;
% pressure_factor = 1.5;
% P = PCRIT*pressure_factor;
% mu = (P-P2)*(Visco2-Visco1)/(P2-P1)+Visco2



% figure(2)
% grid on
% % .*****************NIST Data plot for P=pressure_factorxPc START*********************************
% NIST = readtable("NSIT_Methane_S_O_S_vs_T_Press_"+pressure_factor+"xPc.txt");
% S_O_S_NIST = table2array(NIST(1:450,10));
% Temp_NIST = table2array(NIST(1:450,1));
% scatter(Temp_NIST,S_O_S_NIST, 'LineWidth',0.5);
% hold on
% % *****************NIST Data plot for P=pressure_factorxPc  END**********************************
% 
% % .*****************FLUENT Data plot for P=pressure_factorxPc START*********************************
% FLUENT = readtable("FLUENT_Methane_S_O_S_vs_T_Press_"+pressure_factor+"xPc.txt");
% 
% S_O_S_FLUENT = table2array(FLUENT(1:5000,6));
% Temp_FLUENT = table2array(FLUENT(1:5000,5));
% scatter(Temp_FLUENT,S_O_S_FLUENT,'*')
% hold on
% % *****************FLUENT Data plot for P=pressure_factorxPc  END**********************************
% 
% 
% title("Comparison of Speed Of Sound Vs Temperature for CH4 at Pressure "+pressure_factor+"xPc Pa")
% legend("NIST Speed Of Sound (CH4) for P="+pressure_factor+"xPc","Fluent Exported Data for P="+pressure_factor+"xPc")
% xlabel('Temperature (K)')
% ylabel('Speed Of Sound (m/s)')
% grid on

