clc
clear all
close all

format long e

%% Substance No

% 1-"N2"    
% 2-"AR"    
% 3-"HE"    
% 4-"O2"    
% 5-"O"    
% 6-"H"    
% 7-"HO2"    
% 8-"OH"    
% 9-"H2"    
% 10-"H2O2"    
% 11-"H2O"    
% 12-"CO2"
% 13-"CO"    
% 14-"HCO"    
% 15-"CH2O"    
% 16-"CH3"    
% 17-"CH3O"    
% 18-"CH4"    
% 19-"HCCO"    
% 20-"C2H3"    
% 21-"C2H4"    
% 22-"C2H5"
% 23-"CH3O2"    
% 24-"C2H6"

%%

mi  = [28.0134, 39.948, 4.002602, 31.9988, 15.9994, 1.00797, 33.00677, 17.00737, 2.01594, 34.01474, 18.01534, 44.00995, 28.01055, 29.01852, 30.02649, 15.03506, 31.03446, 16.04303, 41.02967, 27.04621, 28.05418, 29.06215, 47.03386, 30.07012];

R=8314.4621; %J/kg-kmol

%% Calculations

Table_Val = readcell('thermdat_DLR.xlsx');

for i=3:4:95

    cp2((i+1)/4,1)=R/mi((i+1)/4)*cell2mat(Table_Val(i+1,2));
    cp2((i+1)/4,2)=R/mi((i+1)/4)*cell2mat(Table_Val(i+1,3));
    cp2((i+1)/4,3)=R/mi((i+1)/4)*cell2mat(Table_Val(i+1,4));
    cp2((i+1)/4,4)=R/mi((i+1)/4)*cell2mat(Table_Val(i+1,5));
    cp2((i+1)/4,5)=R/mi((i+1)/4)*cell2mat(Table_Val(i+1,6));    

    cp1((i+1)/4,1)=R/mi((i+1)/4)*cell2mat(Table_Val(i+2,4));
    cp1((i+1)/4,2)=R/mi((i+1)/4)*cell2mat(Table_Val(i+2,5));
    cp1((i+1)/4,3)=R/mi((i+1)/4)*cell2mat(Table_Val(i+2,6));
    cp1((i+1)/4,4)=R/mi((i+1)/4)*cell2mat(Table_Val(i+3,2));
    cp1((i+1)/4,5)=R/mi((i+1)/4)*cell2mat(Table_Val(i+3,3));

    T_min((i+1)/4)=cell2mat(Table_Val(i,7));
    T_change((i+1)/4)=cell2mat(Table_Val(i,9));
    T_max((i+1)/4)=cell2mat(Table_Val(i,8));

end

cp1 = flip(cp1,2);
cp2 = flip(cp2,2);

%%

substance_no=18;

%%

syms T

T = 90:1:625;

figure 

plot(T,polyval(cp1(substance_no,:),T), 'LineWidth',1.5)
hold on

%%

NIST = readtable('methane_crit_nist.txt');

T_1 = table2array(NIST(1:536,1));
cp_ch4_1 = 1000*table2array(NIST(1:536,9));

T_2 = table2array(NIST(537:1069,1));
cp_ch4_2 = 1000*table2array(NIST(537:1069,9));

T_3 = table2array(NIST(1070:1601,1));
cp_ch4_3 = 1000*table2array(NIST(1070:1601,9));

T_4 = table2array(NIST(1602:2133,1));
cp_ch4_4 = 1000*table2array(NIST(1602:2133,9));

plot(T_1,cp_ch4_1, T_2, cp_ch4_2, T_3,cp_ch4_3, T_4,cp_ch4_4, 'LineWidth',1.5)
xlim([90 650])
title('Comparison of NIST and Calculated C_p for CH4')
legend('Calculated C_p (CH4)','NIST 1*P_c (CH4)','NIST 1.5*P_c (CH4)','NIST 2*P_c (CH4)','NIST 2.5*P_c (CH4)')
xlabel('T (K)')
ylabel('C_p (J/kgK)')
xlim([90 625])
ylim([1500 5000])
grid on

