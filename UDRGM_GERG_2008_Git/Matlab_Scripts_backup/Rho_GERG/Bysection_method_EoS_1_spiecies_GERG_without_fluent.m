% This script intend to solving GERG equation for density by using
% Bysection method,and plot Density Vs Temperature for different pressure
% for Methane CH4.
% The plots will be also compares with Peng and Rob and Ideal Gas. 
% The issue you will face especially for T<Tcrucial at press < 1.0*PCRIT (graphical
% aproximation), multiple soution exist, your result depend on where are
% your starting predictin range for the density.
% CH4 "i=1" GERG-2008
clear all; clc; close all;
syms f delta rho rho_r Temp Temp_r taw alpha_delta_r press x inverse_rho_r rho_c DalphaOi_r_Ddelta_taw_1 

PCRIT = 4.5992e+06;%Ps
M = 16.04246./1000;%Kg./mol
RGASU = 8.314472;%J./mol.K
R = RGASU./M;%J./Kg.K

double a;
double b;

Tmin=   150;
Tmax=   600;%600
delta_T = 1;
T = Tmin:delta_T:Tmax;
pressure_factor = 2.0;

press = PCRIT*pressure_factor;

rho_bysection_max = 378;
rho_bysection_min = 5;

a = rho_bysection_min;
b = rho_bysection_max;
Tcrucial = 177;

%equation the bysection search boundary for density*******END*******
i = 1;
for Temp = T  
    a = rho_bysection_min;
    b = rho_bysection_max;
%     if Temp < Tcrucial  && press >= 1.0*PCRIT
%         a = 300;
%         b = rho_bysection_max;
%     end    
%     if Temp < Tcrucial && press < 1.0*PCRIT
%         a=  -0.053023256678360*Temp^2 +14.678429720438771*Temp^1  -6.457093279284205e+02*Temp^0-5;
%         b= -0.028875000000002*Temp^2 +7.037050000000574*Temp^1 -40.199500000048992*Temp^0+30;
%     end

    Ti(i)= Temp;
    if (density(a,Temp,press) * density(b,Temp,press) >= 0)
        disp('Incorrect a and b, wrong initial guessing, Please check your calibration parameters') 
    end
    c = a;
    while ((b - a) >= 0.1)
        c = (a + b) ./ 2;
        if (density(c,Temp,press) == 0.0)
            break;
        elseif (density(c,Temp,press)*density(a,Temp,press) < 0)
            b = c;
        else
            a = c;
        end
    end    
    rho(i)= c;
    i = i +1;
end                                                                                                                                                             

%print out an element T and Rho
j= 1;
T = double(Ti(j))
Rho= double(rho(j))

%*****************Methane Peng and Robinson START*********************
rgas = 8314.472471/16.04303;
TCRIT = 190.564;
PCRIT = 4.5992e+06;
w = 0.01142;

n = 0.37464+1.54226.*w-0.26992.*w^2;
a0 = 0.457247.*rgas.*rgas.*TCRIT.*TCRIT./PCRIT;
b0 = 0.0778.*rgas.*TCRIT./PCRIT;
p = press%--------------------------Variable
T_range = T;


j = 1;
density_Peng_Rob=[] ;
for T= Tmin:1:Tmax
    Tj(j)= T;
    v= ((((T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^(1/3) - ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2))/((((T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^(1/3) + (T.*TCRIT.*rgas - TCRIT.*b0.*p)/(3.*TCRIT.*p);
    density_Peng_Rob(j) = 1./v;
    j = j+1;
end



%*****************Methane Peng and Robinson END*********************


figure 
%ploting the bysection search boundary for density*******START*******
% T = Tmin:delta_T:Tmax;
% rho_bysection_guidence_lower = A_lower*T+B_lower;
% rho_bysection_guidence_upper = A_upper*T+B_upper;
% plot(T,rho_bysection_guidence_lower, 'LineWidth',1.5)
% hold on
% plot(T,rho_bysection_guidence_upper, 'LineWidth',1.5)
% hold on
%ploting the bysection search boundary for density*******END*******

%******************Calculated Data plot START*********************
plot(Ti,rho, '.-g')
hold on
%******************Calculated Data plot END*********************

% .*****************Peng & Rob Data plot START*********************************
plot(Tj,density_Peng_Rob, 'LineWidth',1.5);
hold on
% *****************Peng & Rob plot END**********************************

% .*****************NIST Data plot START*********************************
if press == 0.5*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.5xPc.txt');
elseif press == 0.6*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.6xPc.txt');
elseif press == 0.7*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.7xPc.txt');
elseif press == 0.8*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.8xPc.txt');
elseif press == 0.85*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.85xPc.txt');
elseif press == 0.9*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.9xPc.txt');
elseif press == 1*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.0xPc.txt');
elseif press == 1.1*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.1xPc.txt');
elseif press == 1.2*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.2xPc.txt');
elseif press == 1.5*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.5xPc.txt');
elseif press == 2.0*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_2.0xPc.txt');
elseif press == 2.5*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_2.5xPc.txt');
end

rho_NIST = table2array(NIST(1:320,3));
Temp_NIST = table2array(NIST(1:320,1));
plot(Temp_NIST,rho_NIST,  '--r')
% *****************NIST Data plot END**********************************

% % *****************Fluent Data plot START**********************************
% Fluent = readtable("Methane_data_Fluent_"+pressure_factor+"xPc.txt");
% Temp_Fluent = table2array(Fluent(1:5000,6));
% rho_Fluent = table2array(Fluent(1:5000,5));
% %plot(Temp_Fluent,rho_Fluent, 'LineWidth',1.5)
% scatter(Temp_Fluent,rho_Fluent,'*')
% % *****************Fluent Data plot END**********************************

title("Comparison of Density Vs Temperature for CH4 at P= Pc x" + pressure_factor)
legend('Calculated Density GERG equattion using Bysection method (CH4)','Peng & Robinson','NIST Density (CH4)','Location','northeast','Orientation','horizontal','NumColumns',1,'FontSize',8)
xlabel('Temperature (K)')
ylabel('Density (kg/m3)')
xlim([Tmin Tmax])
%ylim([0 400])
grid on
print -dmeta




function f = density(rho,Temp,press) 
    M = 16.04246./1000;%Kg./mol
    RGASU = 8.314472;%J./mol.K
    R = RGASU./M;%J./Kg.K
    Temp_c_1 = 190.564;%K
    rho_c_1 = 10.139342719.*M.*1000;%Kg./m.^3
    Kpol = 6;
    Kexp = 18;
    no_1_k = [0.57335704239162,-0.16760687523730.*10,0.23405291834916,-0.21947376343441,0.16369201404128./10,0.15004406389280./10,0.98990489492918./10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061./10,-0.15782558339049,0.12716735220791,-0.32019743894346./10,-0.68049729364536./10,0.24291412853736./10,0.51440451639444./100,-0.19084949733532./10,0.55229677241291./100,-0.44197392976085./100,0.40061416708429./10,-0.33752085907575./10,-0.25127658213357./100];
    do_1_k = [1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7]; 
    co_1_k = [0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6];
    to_1_k = [0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000];

    %EQUATION (16)Start
    rho_r = rho_c_1;
    delta = rho./rho_r;
    %EQUATION (16)ENDS

    %EQUATION (17)Start
    Temp_r = Temp_c_1;
    %EQUATION (17)ENDS
    
    
    taw = Temp_r./Temp;

    %EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
    %.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*1.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*
    DalphaOi_r_Ddelta_taw_1 = 0;
    for i = [1:1:Kpol]%1--->6   
        DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i).*do_1_k(i).*power(delta,do_1_k(i)-1).*power(taw,to_1_k(i));
    end
    for i = [Kpol+1:1:Kpol+Kexp]%7--->24 (-18 solvable)
        DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i).*power(delta,do_1_k(i)-1).*(do_1_k(i)-co_1_k(i).*power(delta,co_1_k(i))).*power(taw,to_1_k(i)).*exp(-power(delta,co_1_k(i)));
    end

    %EQUATION (alpha_delta_r)Starts
    alpha_delta_r = DalphaOi_r_Ddelta_taw_1 ;
    %EQUATION (alpha_delta_r)ENDS
    % press = rho.*R.*Temp.*(1+delta.*alpha_delta_r);
    f = rho.*R.*Temp.*(1+delta.*alpha_delta_r)-press;
end
  
% the output of the above f is the following, note I face an issue when
% putting 121824942094405 0448863115438194688 together you get 
% 13eFNyWqxMj5LDRpqXdneS3rKqj4ET1YNo   LOLLLL it took me some time
%  f = (142463478837067.*rho.*Temp.*((35184372088832.*rho.*((8432645086898033.*rho.*(47641./(250.*Temp)).^(3./8))./2930222061477556736 - (3953687836883237.*rho.*(47641./(250.*Temp)).^(9./8))./1465111030738778368 + (5164341124930299.*(47641./(250.*Temp)).^(1./8))./9007199254740992 - (7548342608634375.*(47641./(250.*Temp)).^(9./8))./4503599627370496 + (2614136279238579447865120950029010337792.*rho.^3.*(47641./(250.*Temp)).^(3./2))./187452707888136103598775165960458727746515607977 + (2851917106381939611674070553993093840896.*rho.^3.*(47641./(250.*Temp)).^(5./8))./187452707888136103598775165960458727746515607977 - exp(-(35184372088832.*rho)./5723089963823353).*(47641./(250.*Temp)).^(5./8).*((891627063187057.*rho)./1465111030738778368 - 891627063187057./9007199254740992) - exp(-(35184372088832.*rho)./5723089963823353).*(47641./(250.*Temp)).^(21./8).*((2629326254009491.*rho)./732555515369389184 - 2629326254009491./4503599627370496) + exp(-(35184372088832.*rho)./5723089963823353).*((6736202177543581.*rho)./1465111030738778368 - 6736202177543581./9007199254740992).*(47641./(250.*Temp)).^(11./4) - (23166719883517.*rho.*exp(-(43556142965880123323311949751266331066368.*rho.^3)./187452707888136103598775165960458727746515607977).*(47641./(250.*Temp)).^(15./2).*((130668428897640369969935849253798993199104.*rho.^3)./187452707888136103598775165960458727746515607977 - 2))./732555515369389184 - (1058040251100543305545349416662928982016.*rho.^3.*exp(-(1237940039285380274899124224.*rho.^2)./32753758734015587950289788162609).*(47641./(250.*Temp)).^(9./2).*((2475880078570760549798248448.*rho.^2)./32753758734015587950289788162609 - 4))./187452707888136103598775165960458727746515607977 - (157425556988077902923825152.*rho.^2.*exp(-(1237940039285380274899124224.*rho.^2)./32753758734015587950289788162609).*(47641./(250.*Temp)).^(19./4).*((2475880078570760549798248448.*rho.^2)./32753758734015587950289788162609 - 3))./32753758734015587950289788162609 - (2705159431197539.*rho.*exp(-(35184372088832.*rho)./5723089963823353).*(47641./(250.*Temp)).^(17./8).*((35184372088832.*rho)./5723089963823353 - 2))./1465111030738778368 + (2512386548609096458318294102795417245603332096.*rho.^2.*exp(-(1237940039285380274899124224.*rho.^2)./32753758734015587950289788162609).*((2475880078570760549798248448.*rho.^2)./32753758734015587950289788162609 - 3))./(8260871000435717357361795261505126953125.*Temp.^5) + (716970446451416329091058137591495907464798221727956992.*rho.^3.*exp(-(1237940039285380274899124224.*rho.^2)./32753758734015587950289788162609).*((2475880078570760549798248448.*rho.^2)./32753758734015587950289788162609 - 4))./(34383787574569480403959217319357715310848356201171875.*Temp.^4) - (21869015617100785470958581523991756800.*rho.^3.*exp(-(43556142965880123323311949751266331066368.*rho.^3)./187452707888136103598775165960458727746515607977).*(47641./(250.*Temp)).^(23./2).*((130668428897640369969935849253798993199104.*rho.^3)./187452707888136103598775165960458727746515607977 - 4))./17041155262557827599888651450950793431501418907 + (1002378966163189135917954348305628818395139199887324915161026789376.*rho.^5.*exp(-(35184372088832.*rho)./5723089963823353).*(47641./(250.*Temp)).^(7./4).*((35184372088832.*rho)./5723089963823353 - 6))./6139780768205910601721357370750751642093802593355740494549805602287595373531993 + (5782807048892994540257141365496100182741707149821349235975717829519133500164244590879749495555689089998710379077938568454478434747559509491712.*rho.^6.*exp(-(1897137590064188545819787018382342682267975428761855001222473056385648716020711424.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529).*((11382825540385131274918722110294056093607852572571130007334838338313892296124268544.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529 - 7))./(14093456840194685363184251566728214693945162718234978976884206543443641749562158554814806876720467698760330677032470703125.*Temp.^16) + (292380117000473921970299022102394891340493024432512157871126566402705283300570799861938502187764363946160864675941220101298718359216260524161494162067003784822784.*rho.^4.*exp(-(1897137590064188545819787018382342682267975428761855001222473056385648716020711424.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529).*((11382825540385131274918722110294056093607852572571130007334838338313892296124268544.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529 - 5))./(242444250225445791228094206754365507081219686388272332700186742897668587017057095778227449045516550540924072265625.*Temp.^26) - (961980675487317180110255867748585116573894139596735085098964124913376231964190644903725870273834100312041690483045081405934486439774493494671951159067185916736881390950647548468002816.*rho.^5.*exp(-(1897137590064188545819787018382342682267975428761855001222473056385648716020711424.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529).*((11382825540385131274918722110294056093607852572571130007334838338313892296124268544.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529 - 6))./(394184731605660936288059905352096900404477533045049028589170064332103830051913550787658579131755853808272149763070046901702880859375.*Temp.^28) + (2299388807671493616398441272265215286227808275453924362955340360507598019368050311354054651327569800883617488269560214135797317001259772722175652673614199186367337060076791606975789839736111104.*rho.^5.*exp(-(1897137590064188545819787018382342682267975428761855001222473056385648716020711424.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529).*((11382825540385131274918722110294056093607852572571130007334838338313892296124268544.*rho.^6)./35138517694594883396831432002938781252852553543310467559437877649462373013726483511269946032529 - 6))./(30795682156692260647504680105632570344099807269144455358528911275945611722805746155285826494668426078771261700239847414195537567138671875.*Temp.^30) + (3697579199188029755553081201052800135860281523134264407471460219355805265358854291456.*rho.^2.*exp(-(43556142965880123323311949751266331066368.*rho.^3)./187452707888136103598775165960458727746515607977).*((130668428897640369969935849253798993199104.*rho.^3)./187452707888136103598775165960458727746515607977 - 3))./(61548285189537088255299592476376346894539892673492431640625.*Temp.^14) + (5686265908375535.*rho.*exp(-(1237940039285380274899124224.*rho.^2)./32753758734015587950289788162609).*(47641./(250.*Temp)).^(9./2).*((2475880078570760549798248448.*rho.^2)./32753758734015587950289788162609 - 2))./5860444122955113472 - (1218249420944050448863115438194688.*rho.^2.*exp(-(35184372088832.*rho)./5723089963823353).*((35184372088832.*rho)./5723089963823353 - 3))./(4229565952223087286969239173890625.*Temp.^2)))./5723089963823353 + 1))./274877906944 - press;
