clear all; clc; close all;
x = linspace(150,600);
y1 = 1*x.^0;
y2 = 2*x.^0;
figure(1)
plot(x, y1)
hold on
plot(x, y2)
patch([x fliplr(x)], [y1 fliplr(y2)], 'g')
hold off