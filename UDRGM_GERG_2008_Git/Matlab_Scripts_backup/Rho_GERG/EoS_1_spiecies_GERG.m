% This script intend writing GERG equation for density and we plot Rho Vs P
% for different T and we compare these plot with Peng and Rob and Ideal Gas
% reducing variables : delta and tau 
% CH4 is "i=1" C4H10 "i=2" 
clear all; clc; close all;
syms f delta rho rho_r temp temp_r taw alpha_delta_r press x inverse_rho_r rho_c DalphaOi_r_Ddelta_taw_1 


rho = 0.1:1:500;
PCRIT = 4.5992e+06;
%press = 0.5*PCRIT:10000:2*PCRIT;% we want it range 40 to 80bar
temp = 150;


M = 16.04246/1000;%Kg/mol
RGASU = 8.314472;%J/mol.K
R = RGASU/M;%J/Kg.K
temp_c_1 = 190.564;%K
rho_c_1 = 10.139342719.*M.*1000;%Kg/m^3

Kpol = 6;
Kexp = 18;
no_1_k = [0.57335704239162,-0.16760687523730.*10,0.23405291834916,-0.21947376343441,0.16369201404128/10,0.15004406389280/10,0.98990489492918/10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061/10,-0.15782558339049,0.12716735220791,-0.32019743894346/10,-0.68049729364536/10,0.24291412853736/10,0.51440451639444/100,-0.19084949733532/10,0.55229677241291/100,-0.44197392976085/100,0.40061416708429/10,-0.33752085907575/10,-0.25127658213357/100];
do_1_k = [1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7]; 
co_1_k = [0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6];
to_1_k = [0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000];

%EQUATION (16)Start
rho_r = rho_c_1;
delta = rho/rho_r;
%EQUATION (16)ENDS

%EQUATION (17)Start
temp_r = temp_c_1;
%EQUATION (17)ENDS

taw = temp_r./temp;

%EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
%.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*1.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*
DalphaOi_r_Ddelta_taw_1 = 0;
for i = [1:1:Kpol]%1--->6   
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i).*do_1_k(i).*power(delta,do_1_k(i)-1).*power(taw,to_1_k(i));
end
for i = [Kpol+1:1:Kpol+Kexp]%7--->24 (-18 solvable)
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i).*power(delta,do_1_k(i)-1).*(do_1_k(i)-co_1_k(i).*power(delta,co_1_k(i))).*power(taw,to_1_k(i)).*exp(-power(delta,co_1_k(i)));
end

%EQUATION (alpha_delta_r)Starts
alpha_delta_r = DalphaOi_r_Ddelta_taw_1; 
%EQUATION (alpha_delta_r)ENDS
press = rho.*R.*temp.*(1+delta.*alpha_delta_r);
% f = rho.*R.*temp.*(1+delta.*alpha_delta_r)-press


% temp = 600;
% press = 1000000;
%rho = 1;
% the output of the above f is the following
% f = (142463478837067*rho*temp*((35184372088832*rho*((8432645086898033*rho*(47641/(250*temp))^(3/8))/2930222061477556736 - (3953687836883237*rho*(47641/(250*temp))^(9/8))/1465111030738778368 + (5164341124930299*(47641/(250*temp))^(1/8))/9007199254740992 - (7548342608634375*(47641/(250*temp))^(9/8))/4503599627370496 + (2614136279238579447865120950029010337792*rho^3*(47641/(250*temp))^(3/2))/187452707888136103598775165960458727746515607977 + (2851917106381939611674070553993093840896*rho^3*(47641/(250*temp))^(5/8))/187452707888136103598775165960458727746515607977))/5723089963823353 + 1))/274877906944 - press;
% f;

% rho = solve(f,rho)
%temp = solve(f,temp,'MaxDegree',5)


figure 
plot(press,rho, 'LineWidth',1.5)
hold on

% *****************NIST Data plot START*********************************
if temp == 100
    NIST = readtable('Methane_Density_vs_Press_T_100K.txt');
elseif temp == 150
    NIST = readtable('Methane_Density_vs_Press_T_150K.txt');
elseif temp == 192
    NIST = readtable('Methane_Density_vs_Press_T_192K.txt');
elseif temp == 200
    NIST = readtable('Methane_Density_vs_Press_T_200K.txt');
elseif temp == 250
    NIST = readtable('Methane_Density_vs_Press_T_250K.txt');
elseif temp == 300
    NIST = readtable('Methane_Density_vs_Press_T_300K.txt');
elseif temp == 400
    NIST = readtable('Methane_Density_vs_Press_T_400K.txt');
elseif temp == 500
    NIST = readtable('Methane_Density_vs_Press_T_500K.txt');
end
    
rho_NIST = table2array(NIST(1:220,3));
press_NIST = 1000000.*table2array(NIST(1:220,2));
plot(press_NIST,rho_NIST, 'LineWidth',1.5)
% *****************NIST Data plot END**********************************

% *****************IDEAL Gas plot START********************************
rho_Ideal = rho;
press_Ideal = rho_Ideal.*R.*temp;
plot(press_Ideal,rho_Ideal, 'LineWidth',1.5)
% *****************IDEAL Gas plot END**********************************

% *****************Peng&Robinson plot START****************************
rgas = 8314.472471/16.04303;
TCRIT = 190.564;
PCRIT = 4.5992e+06;
w = 0.01142;

n = 0.37464+1.54226.*w-0.26992.*w^2;
a0 = 0.457247.*rgas.*rgas.*TCRIT.*TCRIT./PCRIT;
b0 = 0.0778.*rgas.*TCRIT./PCRIT;
v = 1./(rho);
% p = 2*PCRIT%--------------------------Variable
a = a0.*(1+n.*(1-(temp./TCRIT)^0.5))^2;
press_Peng_Rob = rgas.*temp./(v-b0)-a./(v.*(v+b0)+b0.*(v-b0));
plot(press_Peng_Rob,rho, 'LineWidth',1.5)
% *****************Peng&Robinson plot END******************************
title('Comparison of Density Vs Pressure for CH4 at T=',temp)
legend('Calculated Density Simplefied GERG eq (CH4)','NIST Density (CH4)','Ideal Gas Density (CH4)','Peng&Robinson')
xlabel('Pressure (Pa)')
ylabel('Density (kg/m3)')
% xlim([0.5*PCRIT 2.5*PCRIT])
% ylim([5 400])
grid on



