clear all; clc; close all;
PCRIT = 4.5992e+06;%Pa
Tmin=   150;
Tmax=   600;
Tbreak= 192;
rho = 300;%available data of NIST saved for 5,10,20,40,150,200,300,370 (Kg/m^3)

figure(1)
pressure_factor = 2.5;
% .*****************NIST Data plot first part START*********************************
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Entropy_NIST = table2array(NIST(1:601,7))*1e+03;
Temp_NIST = table2array(NIST(1:601,1));
plot(Temp_NIST,Entropy_NIST, 'LineWidth',1.5)
hold on
% *****************NIST Data plot first part  END**********************************

%*****************FLUENT Data plot START*********************************
FLUENT = readtable("FLUENT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Entropy_FLUENT = table2array(FLUENT(1:3914,6));
Temp_FLUENT = table2array(FLUENT(1:3914,5));
scatter(Temp_FLUENT,Entropy_FLUENT,  '*')
%*****************FLUENT Data plot END*******************

title("Entropy Vs Temperature CH4, NIST GERG Vs Fluent ")
% legend("NIST Thermal Conductivity (CH4) at pressure 1xPc Pa", "polynomial fit Model at pressure 1xPc Pa","NIST Thermal_Conduct_NIST (CH4) at pressure 1.5xPc Pa ","NIST Thermal_Conduct_NIST (CH4) at pressure 2xPc Pa","NIST Thermal_Conduct_NIST (CH4) at pressure 2.5xPc Pa", "polynomial fit Model at pressure 2.5xPc Pa","Viscosity Sutherland Model with 3 coefficients","FLUENT Viscosity (CH4) at pressure "+pressure_factor_FLUENT+"xPc Pa");,"polynomial fit Therm. Cond. Model at pressure 1.5xPc Pa"
legend("NIST Entropy (CH4) at pressure "+pressure_factor+"xPc Pa","Fluent Entropy (CH4) at pressure "+pressure_factor+"xPc Pa ",'Location','northeast','Orientation','horizontal','NumColumns',1,'FontSize',8);%,"NIST Entropy (CH4) at pressure 2xPc Pa","NIST Entropy (CH4) at pressure 2.5xPc Pa", "polynomial fit Therm. Cond. Model at pressure 2.5xPc Pa");
xlabel('Temperature (K)')
ylabel('Entropy (J/Kg/K)')
xlim([Tmin Tmax])
% ylim([6305 6325])
grid on
print -dmeta


