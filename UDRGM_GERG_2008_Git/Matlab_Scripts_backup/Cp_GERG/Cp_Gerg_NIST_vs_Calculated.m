% In this script we intend to plot Cp vs Temperture for two different cases:
% 1) Cp Vs T for constnt Densities (figure 1)
% 2) Cp Vs T for Constant pressure (figure 2)
% It should be noted that for case 2 you are verifing the GERG Density and
% Cp models at the same time since to cmput cp for a constant P you need the
% right Density for that speciic P.
% So validating Fluent data Vs NIST for case 2 means validating the 
% Cp and Density


clear all; clc; close all;
PCRIT = 4.5992e+06;%Pa
double pressure_factor;
pressure_factor = 2.5;
press = PCRIT*pressure_factor

Tmin=   150;
Tmax=   600;
rho = 5;%available data of NIST saved for 5,10,20,40,80,150,200,300,370 (Kg/m^3)

% .*****************Calculated from C++ Data plot START*********************************
GERG = readtable("Methane_Cp_vs_T_Rho_"+rho+"_Calculated.txt");
Temp_GERG = table2array(GERG(1:449,2));
Cp_GERG = table2array(GERG(1:449,3));

figure(1)

plot(Temp_GERG,Cp_GERG, 'LineWidth',0.5)
hold on
% *****************Calculated from C++ Data plot END**********************************

% .*****************NIST Data plot START*********************************
NIST = readtable("NIST_Methane_Cp_vs_T_Rho_"+rho+".txt");

cp_NIST = table2array(NIST(1:430,13));
Temp_NIST = table2array(NIST(1:430,1));
plot(Temp_NIST,cp_NIST,  '--r')
hold on
% *****************NIST Data plot END**********************************

% .*****************FLUENT Data plot START*********************************
FLUENT = readtable("FLUENT_Methane_Cp_vs_T_Rho_"+rho+".txt");

cp_FLUENT = table2array(FLUENT(1:5000,6))/1e+3;
Temp_FLUENT = table2array(FLUENT(1:5000,5));
scatter(Temp_FLUENT,cp_FLUENT,'*')

% *****************FLUENT Data plot END**********************************

title("Comparison of Cp Vs Temperature for CH4 at Density " + rho + " kg/m3")
legend('Calculated Density GERG equation written in C++',"NIST Cp (CH4) at Density " + rho + " kg/m3","Fluent Exported Data at Density " + rho + " kg/m3",'Location','northwest','Orientation','horizontal','NumColumns',1,'FontSize',12)
xlabel('Temperature (K)')
ylabel('Cp (J/g*K)')
% xlim([Tmin Tmax])
ylim([2 3.5])
grid on



figure(2)
grid on
% .*****************NIST Data plot for P=pressure_factorxPc START*********************************
NIST = readtable("NIST_Methane_Cp_vs_T_Press_"+pressure_factor+"xPc.txt");
cp_NIST = table2array(NIST(1:450,9));
Temp_NIST = table2array(NIST(1:450,1));
scatter(Temp_NIST,cp_NIST, 'LineWidth',0.5);
hold on
% *****************NIST Data plot for P=pressure_factorxPc  END**********************************

% .*****************FLUENT Data plot for P=pressure_factorxPc START*********************************
FLUENT = readtable("FLUENT_Methane_Cp_vs_T_Press_"+pressure_factor+"xPc.txt");

cp_FLUENT = table2array(FLUENT(1:5000,6))/1e+3;
Temp_FLUENT = table2array(FLUENT(1:5000,5));
scatter(Temp_FLUENT,cp_FLUENT,'*')
hold on
% *****************FLUENT Data plot for P=pressure_factorxPc  END**********************************


title("Comparison of Cp Vs Temperature for CH4 at Pressure "+pressure_factor+"xPc Pa")
legend("NIST Cp (CH4) for P="+pressure_factor+"xPc","Fluent Exported Data for P="+pressure_factor+"xPc",'Location','northwest','Orientation','horizontal','NumColumns',1,'FontSize',12)
xlabel('Temperature (K)')
ylabel('Cp (J/g*K)')
grid on
print -dmeta