% reducing variables : delta and tau 
% CH4 is "i=1" C4H10 "i=2" 
clear all; clc; close all;
syms f delta rho rho_r temp temp_r taw alpha_delta_r press x inverse_rho_r rho_c DalphaOi_r_Ddelta_taw_1 

% temp = 500;
% rho = 0:1:525;
% press = 1000;

M = 16.04246/1000;%Kg/mol
RGASU = 8.314472;%J/mol.K
R = RGASU/M;%J/Kg.K
temp_c_1 = 190.564;%K
rho_c_1 = 10.139342719*M*1000;%Kg/m^3

Kpol = 6;
Kexp = 18;
no_1_k = [0.57335704239162,-0.16760687523730*10,0.23405291834916,-0.21947376343441,0.16369201404128/10,0.15004406389280/10,0.98990489492918/10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061/10,-0.15782558339049,0.12716735220791,-0.32019743894346/10,-0.68049729364536/10,0.24291412853736/10,0.51440451639444/100,-0.19084949733532/10,0.55229677241291/100,-0.44197392976085/100,0.40061416708429/10,-0.33752085907575/10,-0.25127658213357/100];
do_1_k = [1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7]; 
co_1_k = [0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6];
to_1_k = [0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000];

%EQUATION (16)Start
rho_r = rho_c_1;
delta = rho/rho_r;
%EQUATION (16)ENDS

%EQUATION (17)Start
temp_r = temp_c_1;
%EQUATION (17)ENDS

taw = temp_r/temp;

%EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
%****************************1**********************************
DalphaOi_r_Ddelta_taw_1 = 0;
for i = [1:1:Kpol]%1--->6
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i)*do_1_k(i)*power(delta,do_1_k(i)-1)*power(taw,to_1_k(i));
end
% for i = [Kpol+1:1:Kpol+Kexp]%7--->24 (-18 acceptable)
%     DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i)*power(delta,do_1_k(i)-1)*(do_1_k(i)-co_1_k(i)*power(delta,co_1_k(i)))*power(taw,to_1_k(i))*exp(-power(delta,co_1_k(i)));
% end
%EQUATION (alpha_delta_r)Starts
alpha_delta_r = DalphaOi_r_Ddelta_taw_1;
%EQUATION (alpha_delta_r)ENDS


%press = rho*R*temp*(1+delta*alpha_delta_r);

f = rho*R*temp*(1+delta*alpha_delta_r)-press
rho = solve(f,rho,'MaxDegree',5)




