% syms z temp press f rho
clear all; clc; close all;
PCRIT = 4.5992e+06;
% temp = 100:1:700;
M = 16.04246/1000;%Kg/mol
RGASU = 8.314472;%J/mol.K
R = RGASU/M;%J/Kg.K
% press = 1000000:10:1000000000;
% temp = 500;
press = PCRIT;
i=1;
for temp = [100:1:600]
    temp;
    i;
    a5= 3659571305209504611147676226089409485860152958323609380101838281375744.*temp.*(47641/(250.*temp))^(5/8)+ 3354451675331214283471327691520635047961342965678135468304237475135488.*temp.*(47641/(250.*temp))^(3/2);
    a4= 0;
    a3= - 649107866612901865012080085619116831694222545331726853047899232082960842752.*temp.*(47641/(250.*temp))^(9/8)+ 692226661295440506002448923837689670056150093709129235571970670599113539584.*temp.*(47641/(250.*temp))^(3/8);
    a2= - 403159467916513015627469792455201998874424282517403768206123345529480532606250.*temp.*(47641/(250.*temp))^(9/8)+ 137914581254211566264612517073439189601272204639514585466251042468741649424241.*temp.*(47641/(250.*temp))^(1/8);
    a1= 39126031648033713590550064479528024647918727974543902023586482844479272555014912.*temp;
    a0= - 75492201750431638642778384772989537328568585335594630647525450924543898025984.*press;
    p = [a5 a4 a3 a2 a1 a0];
    r = roots(p);
    % 
    temp(i)= temp;
    rho = r(imag(r)==0)
    x = length(rho)
%     rho = rho(1);
% 
    for y = 1:1:length(rho)
        y
        rhoy = rho(y)
        if rho(1)< rho(y)
            rho(1) = rho(1);
        else
            rho(1) = rho(y);
        end
        
    end
    rho(i) = rho(1)    
        
    
%     for x = [1:1:length(rho)]
%         if rho(x) < rho(1)
%             rho = x;
%     end  
  
%     press_ideal(i) = rho(i)*R*temp;%Ideal Gas Density (CH4)
    i=i+1;
end

figure 
% plot(temp,rho, 'LineWidth',1.5)%Calculated simplified GERG Density for CH4

hold on


NIST = readtable('NIST_Methane_Density_vs_T_1xPc.txt');%NIST
temp_1 = table2array(NIST(1:400,1));
% press_1 = 1000000*table2array(NIST(1:600,2))
density_1 = table2array(NIST(1:400,3));
% plot(temp_1,density_1,'LineWidth',1.5)
hold on

rho_ideal = press./(R.*temp_1)
% plot(temp_1,rho_ideal, 'LineWidth',1.5)
% press_ideal = rho.*R.*temp;%Ideal Gas Density (CH4)
hold on




%*****************Methane Peng And Robenson*********************
R = R;
TCRIT = 190.564;
PCRIT = 4.5992e+06;
w = 0.01142;

n = 0.37464+1.54226.*w-0.26992.*w^2;
a0 = 0.457247.*R.*R.*TCRIT.*TCRIT./PCRIT;
b0 = 0.0778.*R.*TCRIT./PCRIT;
p = 1*PCRIT%--------------------------Variable
T_range = 1:1:700;

% p = [1000000:2000000:1000000000];
% T = temp;
density=[];

i=1
for T = [100:1:600]
    v= ((((T.*TCRIT.*R - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*R + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*R - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R)/(3.*TCRIT.*p) - (T.*TCRIT.*R - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*R - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*R + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*R - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R))/(6.*TCRIT.^2.*p.^2)).^(1/3) - ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R)/(3.*TCRIT.*p) - (T.*TCRIT.*R - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2))/((((T.*TCRIT.*R - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*R + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*R - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R)/(3.*TCRIT.*p) - (T.*TCRIT.*R - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*R - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*R + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*R - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*R))/(6.*TCRIT.^2.*p.^2)).^(1/3) + (T.*TCRIT.*R - TCRIT.*b0.*p)/(3.*TCRIT.*p);
%     density(T) = 1./v;
    T(i)= T;
    density(i) = 1./v;
    i=i+1;
end
density

plot(T,density);



% title('Comparison of NIST and Calculated simplified GERG Density for CH4 and Ideal Gas,Peng & Robenson, T= 500K')
% legend('Calculated Density Simplified GERG(CH4)','NIST Density (CH4)','Ideal Gas Density (CH4)','Peng & Robenson')
% xlabel('Pressure (Pa)')
% ylabel('Density (kg/m3)')
% xlim([0 600])
% ylim([0 500])
% grid on


