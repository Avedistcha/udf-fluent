clc
clear all
close all
%*****************Methane*********************
rgas = 8314.472471/16.04303;
TCRIT = 190.564;
PCRIT = 4.5992e+06;
w = 0.01142;

n = 0.37464+1.54226.*w-0.26992.*w^2;
a0 = 0.457247.*rgas.*rgas.*TCRIT.*TCRIT./PCRIT;
b0 = 0.0778.*rgas.*TCRIT./PCRIT;
p = 2*PCRIT%--------------------------Variable
T_range = 1:1:700;


density=[] ;
for T= T_range
    v= ((((T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^(1/3) - ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2))/((((T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^(1/3) + (T.*TCRIT.*rgas - TCRIT.*b0.*p)/(3.*TCRIT.*p);
    density(T) = 1./v;
end

density_1 = density;
length (T_range);
length (density);
plot(T_range,density);%1
hold on

% p = p/10^6

% ideal gas
T_ran = 30:1:700;
density_2 =p./(rgas.*T_ran)
plot(T_ran,density_2);%2


NIST1 = readtable('Density_CH4_2xPcrit.txt');
T_1 = table2array(NIST1(1:525,1));
density_ch4_1 = table2array(NIST1(1:525,3));

plot(T_1,density_ch4_1, 'LineWidth',1.5) %3
title('Comparison of NIST and Calculated Density for CH4 2*P_c')
legend('Calculated Density (CH4) 2*P_c (CH4)','Ideal Gas Density 2*P_c (CH4)','NIST Density 2*P_c (CH4)')

xlabel('T (K)')
ylabel('Density (kg/m3)')
xlim([100 700])
grid on