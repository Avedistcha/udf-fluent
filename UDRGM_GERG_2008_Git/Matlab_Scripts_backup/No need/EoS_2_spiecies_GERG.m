% reducing variables : delta and tau 

% evaluation of alpha_ij^r done in a function -> "alpha_ij_r";
% evaluation of alpha_0i^r done in a function -> "alpha_0i_r";
% evaluation of alpha_0i^0 done in a function -> "alpha_0i_0";

% CH4 is "1" C4H10 "2" 
clear all; clc; close all;
syms f delta rho rho_r temp temp_r taw alpha_delta_r press x inverse_rho_r rho_c DalphaOi_r_Ddelta_taw_1 DalphaOi_r_Ddelta_taw_2 z

RGASU = 3.14472;
betta_mu = 0.979105972;
gamma_mu = 1.045375122;
betta_T = 0.994174910;
gamma_T = 1.171607691;

temp_c_1 = 190.564;
temp_c_2 = 425.125;
temp_c = [temp_c_1,temp_c_2];

x1 = 0.95;
x2 = 0.05;
x = [x1,x2]; 

rho_c_1 = 10.139342719;
rho_c_2 = 3.920016792;
rho_c = [rho_c_1,rho_c_2];

Kpol = [6,6];
Kexp = [18,6];
no_1_k = [0.57335704239162,-0.16760687523730*10,0.23405291834916,-0.21947376343441,0.16369201404128/10,0.15004406389280/10,0.98990489492918/10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061/10,-0.15782558339049,0.12716735220791,-0.32019743894346/10,-0.68049729364536/10,0.24291412853736/10,0.51440451639444/100,-0.19084949733532/10,0.55229677241291/100,-0.44197392976085/100,0.40061416708429/10,-0.33752085907575/10,-0.25127658213357/100];
no_2_k = [0.10403973107358*10,-0.28318404081403*10,0.84393809606294,-0.76559591850023/10,0.94697373057280/10,0.24796475497006/1000,0.27743760422870,-0.43846000648377/10,-0.26991064784350,-0.69313413089860/10,-0.29632145981653/10,0.14040126751380/10];
do_1_k = [1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7];
do_2_k = [1,1,1,2,3,7,2,5,1,4,3,4]; 
co_1_k = [0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6];
co_2_k = [0,0,0,0,0,0,1,1,2,2,3,3];
to_1_k = [0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000];
to_2_k = [0.250,1.125,1.500,1.375,0.250,0.875,0.625,1.750,3.625,3.625,14.500,12.000];


%EQUATION (16)Start
inverse_rho_r =0;
for i = [1:1:length(x)]
    % disp(x(i))
    inverse_rho_r = inverse_rho_r + power(x(i),2)/rho_c(i); 
end
A1= power(1/rho_c(1),1/3)+power(1/rho_c(2),1/3);
inverse_rho_r = inverse_rho_r + 2*x(1)*x(2)*betta_mu*gamma_mu*(x(1)+x(2))/(power(betta_mu,2)*x(1)+x(2))*power(A1,3)/8;
%EQUATION (16)ENDS
rho_r = 1/inverse_rho_r;
delta = rho/rho_r;

%EQUATION (17)Start
temp_r = 0;
for i = [1:1:length(x)]
    temp_r = temp_r + power(x(i),2)*temp_c(i); 
end
temp_r = temp_r + 2*x(1)*x(2)*betta_T*gamma_T*(x(1)+x(2))/(power(betta_T,2)*x(1)+x(2))*power(temp_c(1)*temp_c(2),0.5); 

%EQUATION (17)ENDS
taw = temp_r/temp;




%EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
%****************************1**********************************
DalphaOi_r_Ddelta_taw_1 = 0;
for i = [1:1:Kpol(1)]%1--->6
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i)*do_1_k(i)*power(delta,do_1_k(i)-1)*power(taw,to_1_k(i));
end
for i = [Kpol(1)+1:1:Kpol(1)+Kexp(1)]%7--->24
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i)*power(delta,do_1_k(i)-1)*(do_1_k(i)-co_1_k(i)*power(delta,co_1_k(i)))*power(taw,to_1_k(i))*exp(-power(delta,co_1_k(i)));
end

%****************************1**********************************
%EQUATION (DalphaOi_r_Ddelta_taw_1)ENDS
%EQUATION (DalphaOi_r_Ddelta_taw_2)Starts
%****************************2**********************************
%****************************2**********************************
%EQUATION (DalphaOi_r_Ddelta_taw_2)ENDS


%EQUATION (alpha_delta_r)Starts
alpha_delta_r = 0;
alpha_delta_r = alpha_delta_r + x(1)*DalphaOi_r_Ddelta_taw_1; % only for now + x(2)*DalphaOi_r_Ddelta_taw_2;
%EQUATION (alpha_delta_r)ENDS
alpha_delta_r 

f = rho*RGASU*temp*(1+delta*alpha_delta_r)-press;
 
f1 = alpha_delta_r;

rho = solve (f1, rho)

%rho = solve (f1, rho,'MaxDegree',12)

% eqn = z^6 + (2342905409471279357552832422921*z^5)/50964945626386881369593610240 + 
% (2976530842794912297774546517909184964944861403*z^4)/19127142510996129065715738851529694558289920 
% - (5381529772810866361786641603506858822406000775200197645205667681840520285944614257884195729*z^3*((41826180467817271165841935200464165404672*(7209055618450643/(35184372088832*temp))^(3/2))/11599062217277380353480567084502537857877587635 + (45630673702111033786785128863889501454336*(7209055618450643/(35184372088832*temp))^(5/8))/11599062217277380353480567084502537857877587635 - 26646149089640462492705561145901198606336/11599062217277380353480567084502537857877587635))/976294398911379757476733342933626346946023818091099087560770703175238204446998528 - (253076541477069434981585776609294277806214857193370218517559843777316203288483*z^2)/284139066617755381587407709720319354367100115077075050084610325044264960 - (5381529772810866361786641603506858822406000775200197645205667681840520285944614257884195729*z*((8432645086898033*(7209055618450643/(35184372088832*temp))^(3/8))/3388890917382407680 - (3953687836883237*(7209055618450643/(35184372088832*temp))^(9/8))/1694445458691203840 + 5319705575482757/6777781834764815360))/976294398911379757476733342933626346946023818091099087560770703175238204446998528 + (2708108698916182028912996211282311029108838952785899223806322244473515504730110359153845948057840526505625*(7209055618450643/(35184372088832*temp))^(9/8))/5862452121521589668595516398720589997982696377768404991021609501596594394169687154353525630173184 - (9264018506921321997453063837447841509904783227789246715743853238781311453669521593267360644145089599497657*(7209055618450643/(35184372088832*temp))^(1/8))/58624521215215896685955163987205899979826963777684049910216095015965943941696871543535256301731840 + 1576579975284211543194946658902472020114563652437943144828296663835994001500173228001290496792248484379059/87936781822823845028932745980808849969740445666526074865324142523948915912545307315302884452597760;
% eqn
% S = solve(eqn,z)


% for i = [1:1:length(x)-1]
%     for j = [1+1:1:length(x)]
%         inverse_rho_r = inverse_rho_r +2*x(i)*x(j)*betta_mu(i,j)*gamma_mu(i,j)*(x(i)+x(j))/(power(betta_mu(i,j),2)*x(i)+x(j));
%     end
% end















% a=0;
% for i = 1:N
%     if x(i) == 0
%     alpha_0 = 0;
%     else
%     alpha_0i0 = alpha_0i_0(rho,T,rhoc,Tc,i);
%     alpha_0 = x(i)*(alpha_0i0 + log(x(i)));
%     end
%     a = a + alpha_0;
% end
% alpha_0 = a;  % ideal-gas Helmholtz free energy 
% 
% % To consider also the non-ideal contribution, the evaluation of the
% % departure function ('D_alpha') is needed
% 
% D_alpha=0;
% for i=1:(N-1)
%    for j = (i+1):N
%        F_ij = tableA6(i,j);   % parameters from paper
%        if F_ij == 0 
%            D_alpha = D_alpha + 0;
%        else
%            alpha_ijr = alpha_ij_r(D,tau,i,j);
%            D_alpha = D_alpha + x(i)*x(j)*F_ij*alpha_ijr;
%        end
%    end
% end
% 
% 
% % And now the evaluation of the non-ideal contribution
% % ('alpha_r') is possible
% a=0;
% for i = 1:N
%     if x(i) == 0 
%         alpha_r1 =  0;
%     else
%         alpha_0ir = alpha_0i_r(D,tau,i);
%         alpha_r1 = x(i)*alpha_0ir;
%     end
%     a = a + alpha_r1;
% end
% 
% alpha_r = a + D_alpha ; % Residual part of the Helmholtz free energy
% 
% end

