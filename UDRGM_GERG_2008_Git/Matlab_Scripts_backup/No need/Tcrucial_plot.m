clear all; clc; close all;
PCRIT = 4.5992e+06;
pressure_factor = 0.60;
%press = PCRIT*pressure_factor

press(1) = PCRIT*0.5; %169.66
Tcrucial(1) = 169.66;
press(2) = PCRIT*0.6; %174.82
Tcrucial(2) = 174.82;
press(3) = PCRIT*0.7; %179.39
Tcrucial(3) = 179.39;
press(4) = PCRIT*0.8 %183.49
Tcrucial(4) = 183.49;
press(5) = PCRIT*0.9 %187.2
Tcrucial(5) = 187.2;
press(6) = PCRIT*1.0 %190.56
Tcrucial(6) = 190.56;

% press(7) = PCRIT*1.1/10^6 %191
% Tcrucial(7) = 191;
% press12 = PCRIT*1.2/10^6
k = 3;
Tcrucial_polynomial_fit = polyfit(press,Tcrucial,k)
Tcrucial_polynomial_fit(1)
Tcrucial_polynomial_fit(2)
Tcrucial_polynomial_fit(3)
Tcrucial_polynomial_fit(4)
% Tcrucial_polynomial_fit1 = 1.0e+07 *(-0.0007*x^2 + 0.1123*x -6.1980)
i=1;
for s=PCRIT*0.5:1000:PCRIT*1.5;
    pr(i)=s;
%     pr(i) = polyval(Tcrucial_polynomial_fit,s);
    T(i) = -1.0502e-12*s^2+ 1.6299e-05*s +137.7757;
    i= i+1;
end
plot(pr,T);
grid on
hold on
plot(press,Tcrucial, 'LineWidth',1.5);
% hold on