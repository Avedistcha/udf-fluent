% This script intend to plot NIST curves Density Vs Temperature for
% multiple Pressure, the goal is to visually indentify the region where the
% density exit Vs Temperature, this information can be later used to specify 
%the range of the roots for the bisection method 
clear all; clc; close all;

NIST_05 = readtable('Methane_Density_vs_T_Press_0.5xPc.txt');



NIST_06 = readtable('Methane_Density_vs_T_Press_0.6xPc.txt');
NIST_07 = readtable('Methane_Density_vs_T_Press_0.7xPc.txt');
NIST_08 = readtable('Methane_Density_vs_T_Press_0.8xPc.txt');
NIST_085 = readtable('Methane_Density_vs_T_Press_0.85xPc.txt');
NIST_09 = readtable('Methane_Density_vs_T_Press_0.9xPc.txt');
NIST_10 = readtable('Methane_Density_vs_T_Press_1.0xPc.txt');
NIST_11 = readtable('Methane_Density_vs_T_Press_1.1xPc.txt');
NIST_12 = readtable('Methane_Density_vs_T_Press_1.2xPc.txt');
NIST_15 = readtable('Methane_Density_vs_T_Press_1.5xPc.txt');
NIST_20 = readtable('Methane_Density_vs_T_Press_2.0xPc.txt');
NIST_25 = readtable('Methane_Density_vs_T_Press_2.5xPc.txt');


Tcrucial = 177;
Tmin=150;
Tmax=240;
T = Tmin:1:Tmax;

figure 

rho_NIST_05 = table2array(NIST_05(1:320,3));
Temp_NIST_05 = table2array(NIST_05(1:320,1));
plot(Temp_NIST_05,rho_NIST_05,'LineWidth',1.5)
hold on




rho_NIST_06 = table2array(NIST_06(1:320,3));
Temp_NIST_06 = table2array(NIST_06(1:320,1));
plot(Temp_NIST_06,rho_NIST_06, 'LineWidth',1.5)
hold on
rho_NIST_07 = table2array(NIST_07(1:320,3));
Temp_NIST_07 = table2array(NIST_07(1:320,1));
plot(Temp_NIST_07,rho_NIST_07, 'LineWidth',1.5)
hold on
rho_NIST_08 = table2array(NIST_08(1:320,3));
Temp_NIST_08 = table2array(NIST_08(1:320,1));
plot(Temp_NIST_08,rho_NIST_08, 'LineWidth',1.5)
hold on
rho_NIST_085 = table2array(NIST_085(1:320,3));
Temp_NIST_085 = table2array(NIST_085(1:320,1));
plot(Temp_NIST_085,rho_NIST_085, 'LineWidth',1.5)
hold on
rho_NIST_09 = table2array(NIST_09(1:320,3));
Temp_NIST_09 = table2array(NIST_09(1:320,1));
plot(Temp_NIST_09,rho_NIST_09, 'LineWidth',1.5)
hold on
rho_NIST_10 = table2array(NIST_10(1:320,3));
Temp_NIST_10 = table2array(NIST_10(1:320,1));
plot(Temp_NIST_10,rho_NIST_10,'LineWidth',1.5)
hold on
rho_NIST_11 = table2array(NIST_11(1:320,3));
Temp_NIST_11 = table2array(NIST_11(1:320,1));
plot(Temp_NIST_11,rho_NIST_11, 'LineWidth',1.5)
hold on
rho_NIST_12 = table2array(NIST_12(1:320,3));
Temp_NIST_12 = table2array(NIST_12(1:320,1));
plot(Temp_NIST_12,rho_NIST_12, 'LineWidth',1.5)
hold on
rho_NIST_15 = table2array(NIST_15(1:320,3));
Temp_NIST_15 = table2array(NIST_15(1:320,1));
plot(Temp_NIST_15,rho_NIST_15, 'LineWidth',1.5)
hold on
rho_NIST_20 = table2array(NIST_20(1:320,3));
Temp_NIST_20 = table2array(NIST_20(1:320,1));
plot(Temp_NIST_20,rho_NIST_20, 'LineWidth',1.5)
hold on
rho_NIST_25 = table2array(NIST_25(1:320,3));
Temp_NIST_25 = table2array(NIST_25(1:320,1));
plot(Temp_NIST_25,rho_NIST_25, 'LineWidth',1.5)



%*******Lower part Polyfit boundary used for the bysection method STARTS******
NIST_08 = readtable('Methane_Density_vs_T_Press_0.8xPc_expand.txt');
limit_fit = 5;
rho_NIST = table2array(NIST_08(1:limit_fit,3));
Temp_NIST = table2array(NIST_08(1:limit_fit,1));
k = 2;
format long
rho_polynomial_fit = polyfit(Temp_NIST,rho_NIST,k)
% rho_polynomial_fit_1 = rho_polynomial_fit(1)
% rho_polynomial_fit_2 = rho_polynomial_fit(2)
% rho_polynomial_fit_3 =rho_polynomial_fit(3)
i=1
for s=T;
    Ti(i)=s;
    rho_poly(i)=0;
    for j = 1:1:k+1;
        rho_poly(i) = rho_poly(i) + rho_polynomial_fit(j)*s^(k+1-j);
    end
%     rho_poly(i) = rho_polynomial_fit(1)*s^(4)+ rho_polynomial_fit(2)*s^3 + rho_polynomial_fit(3)*s^2 +rho_polynomial_fit(4)*s^1 + rho_polynomial_fit(5)*s^0;
    i= i+1;
end

plot(Ti,rho_poly-4,'*-b');
hold on
%*******Lower part Polyfit boundary used for the bysection method ENDS******


%*******Upper part Polyfit boundary used for the bysection method STARTS******
NIST_10 = readtable('Methane_Density_vs_T_Press_1.0xPc_expand.txt');
limit_fit = 4;
rho_NIST = table2array(NIST_10(1:limit_fit,3));
Temp_NIST = table2array(NIST_10(1:limit_fit,1));
k = 2;
rho_polynomial_fit_upper = polyfit(Temp_NIST,rho_NIST,k);
% rho_polynomial_fit_upper_1 = rho_polynomial_fit_upper(1)
% rho_polynomial_fit_upper_2 = rho_polynomial_fit_upper(2)
% rho_polynomial_fit_upper_3 =rho_polynomial_fit_upper(3)
i= 1;
for s=T;
    Ti(i)=s;
    rho_poly_upper(i)=0;
    for j = 1:1:k+1;
        rho_poly_upper(i) = rho_poly_upper(i) + rho_polynomial_fit_upper(j)*s^(k+1-j);
    end
    i= i+1;
end
plot(Ti,rho_poly_upper+30,'*-r');
hold on
%*******Upper part Polyfit boundary used for the bysection method ENDS******

xline(Tcrucial,'*-b')
hold on

title("Density Vs Temperature for CH4 at different Pressure" )
legend('NIST P=0.5*Pc','NIST P=0.6*Pc','NIST P=0.7*Pc','NIST P=0.8*Pc','NIST P=0.85*Pc','NIST P=0.9*Pc','NIST P=1.0*Pc','NIST P=1.1*Pc','NIST P=1.2*Pc','NIST P=1.5*Pc','NIST P=2.0*Pc','NIST P=2.5*Pc','rho bysection guidence lower','rho bysection guidence upper','T=Tcrucial')
xlabel('Temperature (K)')
ylabel('Density (kg/m3)')
grid on
xlim([150 200])
% ylim([5 400])