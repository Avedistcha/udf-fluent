% This script intend to solving GERG equation for density by using
% Bysection method,and plot Density Vs Temperature for different pressure
% for Methane CH4.
% The plots will be also compares with Peng and Rob and Ideal Gas. 
% The issue you will face especially for T<Tcrucial at press < 1.0*PCRIT (graphical
% aproximation), multiple soution exist, your result depend on where are
% your starting predictin range for the density.
% CH4 "i=1" GERG-2008
clear all; clc; close all;
syms f delta rho rho_r Temp Temp_r taw alpha_delta_r press x inverse_rho_r rho_c DalphaOi_r_Ddelta_taw_1 

PCRIT = 4.5992e+06;%Ps
M = 16.04246./1000;%Kg./mol
RGASU = 8.314472;%J./mol.K
R = RGASU./M;%J./Kg.K

double a;
double b;

Tmin=   150;
Tmax=   600;%600
delta_T = 1;
T = Tmin:delta_T:Tmax;
pressure_factor = 2.5;

press = PCRIT*pressure_factor;

rho_bysection_max = 378;
rho_bysection_min = 5;

a = rho_bysection_min;
b = rho_bysection_max;
Tcrucial = 177;

%equation the bysection search boundary for density*******END*******
i = 1;
for Temp = T  
    a = rho_bysection_min;
    b = rho_bysection_max;
    if Temp < Tcrucial  && press >= 1.0*PCRIT
        a = 300;
        b = rho_bysection_max;
    end    
    if Temp < Tcrucial && press < 1.0*PCRIT
        a=  -0.053023256678360*Temp^2 +14.678429720438771*Temp^1  -6.457093279284205e+02*Temp^0-5;
        b= -0.028875000000002*Temp^2 +7.037050000000574*Temp^1 -40.199500000048992*Temp^0+30;
    end

    Ti(i)= Temp;
    if (density(a,Temp,press) * density(b,Temp,press) >= 0)
        disp('Incorrect a and b, wrong initial guessing, Please check your calibration parameters') 
    end
    c = a;
    while ((b - a) >= 0.1)
        c = (a + b) ./ 2;
        if (density(c,Temp,press) == 0.0)
            break;
        elseif (density(c,Temp,press)*density(a,Temp,press) < 0)
            b = c;
        else
            a = c;
        end
    end    
    rho(i)= c;
    i = i +1;
end                                                                                                                                                             

%print out an element T and Rho
j= 1;
T = double(Ti(j))
Rho= double(rho(j))

%*****************Methane Peng and Robinson START*********************
rgas = 8314.472471/16.04303;
TCRIT = 190.564;
PCRIT = 4.5992e+06;
w = 0.01142;

n = 0.37464+1.54226.*w-0.26992.*w^2;
a0 = 0.457247.*rgas.*rgas.*TCRIT.*TCRIT./PCRIT;
b0 = 0.0778.*rgas.*TCRIT./PCRIT;
p = press%--------------------------Variable
T_range = T;


j = 1;
density_Peng_Rob=[] ;
for T= Tmin:1:Tmax
    Tj(j)= T;
    v= ((((T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^(1/3) - ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2))/((((T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^2 + ((TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas)/(3.*TCRIT.*p) - (T.*TCRIT.*rgas - TCRIT.*b0.*p).^2/(9.*TCRIT.^2.*p.^2)).^3).^(1/2) + (T.*TCRIT.*rgas - TCRIT.*b0.*p).^3/(27.*TCRIT.^3.*p.^3) + (TCRIT.*a0.*b0 - TCRIT.*b0.^3.*p - T.*TCRIT.*b0.^2.*rgas + T.*a0.*b0.*n.^2 + TCRIT.*a0.*b0.*n.^2 + 2.*TCRIT.*a0.*b0.*n - 2.*TCRIT.*a0.*b0.*n.^2.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*b0.*n.*(T/TCRIT).^(1/2))/(2.*TCRIT.*p) - ((T.*TCRIT.*rgas - TCRIT.*b0.*p).*(TCRIT.*a0 + 2.*TCRIT.*a0.*n + T.*a0.*n.^2 + TCRIT.*a0.*n.^2 - 3.*TCRIT.*b0.^2.*p - 2.*TCRIT.*a0.*n.*(T/TCRIT).^(1/2) - 2.*TCRIT.*a0.*n.^2.*(T/TCRIT).^(1/2) - 2.*T.*TCRIT.*b0.*rgas))/(6.*TCRIT.^2.*p.^2)).^(1/3) + (T.*TCRIT.*rgas - TCRIT.*b0.*p)/(3.*TCRIT.*p);
    density_Peng_Rob(j) = 1./v;
    j = j+1;
end



%*****************Methane Peng and Robinson END*********************


figure 

%******************Calculated Data plot START*********************
plot(Ti,rho, '.-g')
hold on
%******************Calculated Data plot END*********************

% .*****************Peng & Rob Data plot START*********************************
plot(Tj,density_Peng_Rob, 'LineWidth',1.5);
hold on
% *****************Peng & Rob plot END**********************************

% .*****************NIST Data plot START*********************************
if press == 0.5*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.5xPc.txt');
elseif press == 0.6*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.6xPc.txt');
elseif press == 0.7*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.7xPc.txt');
elseif press == 0.8*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.8xPc.txt');
elseif press == 0.85*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.85xPc.txt');
elseif press == 0.9*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_0.9xPc.txt');
elseif press == 1*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.0xPc.txt');
elseif press == 1.1*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.1xPc.txt');
elseif press == 1.2*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.2xPc.txt');
elseif press == 1.5*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_1.5xPc.txt');
elseif press == 2.0*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_2.0xPc.txt');
elseif press == 2.5*PCRIT
    NIST = readtable('Methane_Density_vs_T_Press_2.5xPc.txt');
end

rho_NIST = table2array(NIST(1:320,3));
Temp_NIST = table2array(NIST(1:320,1));
plot(Temp_NIST,rho_NIST,  '--r')
% *****************NIST Data plot END**********************************


title("Comparison of Density Vs Temperature for CH4 at P= Pc x" + pressure_factor)
% legend('rho bysection guidence lower','rho bysection guidence upper','Calculated Density GERG equattion using Bysection method (CH4)','Peng & Robinson','NIST Density (CH4)')
legend('Calculated Density GERG equattion using Bysection method (CH4)','Peng & Robinson','NIST Density (CH4)')
xlabel('Temperature (K)')
ylabel('Density (kg/m3)')
xlim([Tmin Tmax])
%ylim([0 400])
grid on





function f = density(rho,Temp,press) 
    M = 16.04246./1000;%Kg./mol
    RGASU = 8.314472;%J./mol.K
    R = RGASU./M;%J./Kg.K
    Temp_c_1 = 190.564;%K
    rho_c_1 = 10.139342719.*M.*1000;%Kg./m.^3
    Kpol = 6;
    Kexp = 18;
    no_1_k = [0.57335704239162,-0.16760687523730.*10,0.23405291834916,-0.21947376343441,0.16369201404128./10,0.15004406389280./10,0.98990489492918./10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061./10,-0.15782558339049,0.12716735220791,-0.32019743894346./10,-0.68049729364536./10,0.24291412853736./10,0.51440451639444./100,-0.19084949733532./10,0.55229677241291./100,-0.44197392976085./100,0.40061416708429./10,-0.33752085907575./10,-0.25127658213357./100];
    do_1_k = [1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7]; 
    co_1_k = [0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6];
    to_1_k = [0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000];

    %EQUATION (16)Start
    rho_r = rho_c_1;
    delta = rho./rho_r;
    %EQUATION (16)ENDS

    %EQUATION (17)Start
    Temp_r = Temp_c_1;
    %EQUATION (17)ENDS
    
    
    taw = Temp_r./Temp;

    %EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
    %.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*1.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*
    DalphaOi_r_Ddelta_taw_1 = 0;
    for i = [1:1:Kpol]%1--->6   
        DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i).*do_1_k(i).*power(delta,do_1_k(i)-1).*power(taw,to_1_k(i));
    end
    for i = [Kpol+1:1:Kpol+Kexp]%7--->24 (-18 solvable)
        DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + no_1_k(i).*power(delta,do_1_k(i)-1).*(do_1_k(i)-co_1_k(i).*power(delta,co_1_k(i))).*power(taw,to_1_k(i)).*exp(-power(delta,co_1_k(i)));
    end

    %EQUATION (alpha_delta_r)Starts
    alpha_delta_r = DalphaOi_r_Ddelta_taw_1 ;
    %EQUATION (alpha_delta_r)ENDS
    % press = rho.*R.*Temp.*(1+delta.*alpha_delta_r);
    f = rho.*R.*Temp.*(1+delta.*alpha_delta_r)-press;
end
  
