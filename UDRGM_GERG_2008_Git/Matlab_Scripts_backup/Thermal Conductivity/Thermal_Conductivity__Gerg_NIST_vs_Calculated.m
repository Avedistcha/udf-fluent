% In this script we intend to plot Cp vs Temperture for two different cases:
% 1) Cp Vs T for constnt Densities (figure 1)
% 2) Cp Vs T for Constant pressure (figure 2)
% It should be noted that for case 2 you are verifing the GERG Density and
% Cp models at the same time since to cmput cp for a constant P you need the
% right Density for that speciic P.
% So validating Fluent data Vs NIST for case 2 means validating the 
% Cp and Density


clear all; clc; close all;
PCRIT = 4.5992e+06;%Pa
Tmin=   150;
Tmax=   600;
Tbreak= 192;
rho = 300;%available data of NIST saved for 5,10,20,40,150,200,300,370 (Kg/m^3)

figure(1)

% .*****************NIST Data plot first part START*********************************
double pressure_factor;
pressure_factor = 1;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");

% Thermal_Conduct_NIST = table2array(NIST(1:54,13));
% Temp_NIST = table2array(NIST(1:54,1));
Thermal_Conduct_NIST = table2array(NIST(1:603,13));
Temp_NIST = table2array(NIST(1:603,1));
plot(Temp_NIST,Thermal_Conduct_NIST,  '--b')
hold on
% *****************NIST Data plot first part  END**********************************

% *****************Polyfit Data plot first part START*********************************
syms T
k = 3;
% format long
double Thermal_Conduct;
% Thermal_Conduct_1 = polyfit(Temp_NIST,Thermal_Conduct_NIST,k)
Thermal_Conduct_1 = [-2.04549326505191e-08,5.59199640835146e-06,-0.00163074177582917,0.323301386207505];
Temp = 150:1:Tbreak;
Thermal_Conduct_1 = Thermal_Conduct_1(1)*Temp.^k+ Thermal_Conduct_1(2)*Temp.^(k-1)+ Thermal_Conduct_1(3)*Temp.^(k-2)+ Thermal_Conduct_1(4)*Temp.^(k-3);%+ Thermal_Conduct_1(5)*Temp.^(k-4)+ Thermal_Conduct_1(6)*Temp.^(k-5)+ Thermal_Conduct_1(7)*Temp.^(k-6)+ Thermal_Conduct_1(8)*Temp.^(k-7)+ Thermal_Conduct_1(9)*Temp.^(k-8);
plot(Temp,Thermal_Conduct_1, 'LineWidth',1.5);
% *****************Polyfit Data plot first part END*********************************

% *****************Polyfit Data plot second part START*********************************
double pressure_factor;
pressure_factor = 1;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Thermal_Conduct_NIST = table2array(NIST(65:603,13));
Temp_NIST = table2array(NIST(65:603,1));
k = 5;
% format long
double Thermal_Conduct;
% Thermal_Conduct = polyfit(Temp_NIST,Thermal_Conduct_NIST,k)
Thermal_Conduct_2 = [-2.69500369782704e-14,5.73147260086157e-11,-4.80105164971084e-08,1.99419751429934e-05,-0.00397243698220043,0.331941044163246];
Temp = Tbreak:1:600;
Thermal_Conduct_2 = Thermal_Conduct_2(1)*Temp.^k+ Thermal_Conduct_2(2)*Temp.^(k-1)+ Thermal_Conduct_2(3)*Temp.^(k-2)+ Thermal_Conduct_2(4)*Temp.^(k-3)+ Thermal_Conduct_2(5)*Temp.^(k-4)+ Thermal_Conduct_2(6)*Temp.^(k-5);%+ Thermal_Conduct_2(7)*Temp.^(k-6)+ Thermal_Conduct_2(8)*Temp.^(k-7)+ Thermal_Conduct_2(9)*Temp.^(k-8);
plot(Temp,Thermal_Conduct_2, 'LineWidth',1.5);
% *****************Polyfit Data plot second part END*********************************

% *****************NIST Data plot START*********************************
pressure_factor = 1.5;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Thermal_Conduct_NIST = table2array(NIST(1:451,13));
Temp_NIST = table2array(NIST(1:451,1));
plot(Temp_NIST,Thermal_Conduct_NIST,  '--k')
hold on
% *****************NIST Data plot END**********************************
% % *****************Polyfit Data plot 1.5PCRIT START*********************************
% pressure_factor = 1.5;
% press = PCRIT*pressure_factor
% k = 8;
% Thermal_Conduct_3 = polyfit(Temp_NIST,Thermal_Conduct_NIST,k)
% % Thermal_Conduct_3 = [-9.49432907548459e-21,3.17380346905899e-17,-4.54546200191642e-14,3.63085127662804e-11,-1.76116261050216e-08,5.27565513416557e-06,-0.000942271427550855,0.0899070302007644,-3.32137410604067];
% Temp = Temp_NIST;
% Thermal_Conduct_3 = Thermal_Conduct_3(1)*Temp.^k+ Thermal_Conduct_3(2)*Temp.^(k-1)+ Thermal_Conduct_3(3)*Temp.^(k-2)+ Thermal_Conduct_3(4)*Temp.^(k-3)+ Thermal_Conduct_3(5)*Temp.^(k-4)+ Thermal_Conduct_3(6)*Temp.^(k-5)+ Thermal_Conduct_3(7)*Temp.^(k-6)+ Thermal_Conduct_3(8)*Temp.^(k-7)+ Thermal_Conduct_3(9)*Temp.^(k-8);%+ Thermal_Conduct_3(10)*Temp.^(k-9)+ Thermal_Conduct_3(11)*Temp.^(k-10);
% plot(Temp_NIST,Thermal_Conduct_3, 'LineWidth',1.5);
% % *****************Polyfit Data 1.5PCRI plot END*********************************


% *****************NIST Data plot START*********************************
pressure_factor = 2;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Thermal_Conduct_NIST = table2array(NIST(1:430,13));
Temp_NIST = table2array(NIST(1:430,1));
plot(Temp_NIST,Thermal_Conduct_NIST,  '--r')
hold on
% *****************NIST Data plot END**********************************


% *****************NIST Data plot START*********************************
pressure_factor = 2.5;
press = PCRIT*pressure_factor
NIST = readtable("NSIT_Methane_T_Press_"+pressure_factor+"xPc.txt");
Thermal_Conduct_NIST = table2array(NIST(1:451,13));
Temp_NIST = table2array(NIST(1:451,1));
plot(Temp_NIST,Thermal_Conduct_NIST,  '--g')
hold on
% *****************NIST Data plot END**********************************

% *****************Polyfit Data plot START*********************************
pressure_factor = 2.5;
press = PCRIT*pressure_factor
k = 8;
% Thermal_Conduct_2 = polyfit(Temp_NIST,Thermal_Conduct_NIST,k)
Thermal_Conduct_2 = [-9.49432907548459e-21,3.17380346905899e-17,-4.54546200191642e-14,3.63085127662804e-11,-1.76116261050216e-08,5.27565513416557e-06,-0.000942271427550855,0.0899070302007644,-3.32137410604067];
Temp = Temp_NIST;
Thermal_Conduct_2 = Thermal_Conduct_2(1)*Temp.^k+ Thermal_Conduct_2(2)*Temp.^(k-1)+ Thermal_Conduct_2(3)*Temp.^(k-2)+ Thermal_Conduct_2(4)*Temp.^(k-3)+ Thermal_Conduct_2(5)*Temp.^(k-4)+ Thermal_Conduct_2(6)*Temp.^(k-5)+ Thermal_Conduct_2(7)*Temp.^(k-6)+ Thermal_Conduct_2(8)*Temp.^(k-7)+ Thermal_Conduct_2(9)*Temp.^(k-8);
plot(Temp_NIST,Thermal_Conduct_2, 'LineWidth',1.5);
% *****************Polyfit Data plot END*********************************

%*****************FLUENT Data plot START*********************************
% double pressure_factor_FLUENT;
% pressure_factor_FLUENT = 2.5;
% FLUENT = readtable("FLUENT_Methane_T_Press_"+pressure_factor_FLUENT+"xPc.txt");
% Thermal_Conduc_FLUENT = table2array(FLUENT(1:4999,6));
% Temp_FLUENT = table2array(FLUENT(1:4999,5));
% scatter(Temp_FLUENT,Thermal_Conduc_FLUENT,  '*')
%*****************FLUENT Data plot END**********************************

title("Therm. Cond. Vs Temperature CH4 for different pressures or models ")
% legend("NIST Thermal Conductivity (CH4) at pressure 1xPc Pa", "polynomial fit Model at pressure 1xPc Pa","NIST Thermal_Conduct_NIST (CH4) at pressure 1.5xPc Pa ","NIST Thermal_Conduct_NIST (CH4) at pressure 2xPc Pa","NIST Thermal_Conduct_NIST (CH4) at pressure 2.5xPc Pa", "polynomial fit Model at pressure 2.5xPc Pa","Viscosity Sutherland Model with 3 coefficients","FLUENT Viscosity (CH4) at pressure "+pressure_factor_FLUENT+"xPc Pa");,"polynomial fit Therm. Cond. Model at pressure 1.5xPc Pa"
legend("NIST Thermal Conductivity (CH4) at pressure 1xPc Pa", "polynomial fit Therm. Cond. Model at pressure 1xPc Pa for T<"+Tbreak+" Vapor", "polynomial fit Therm. Cond. Model at pressure 1xPc Pa for T>"+Tbreak+" Liquid","NIST Thermal Conductivity (CH4) at pressure 1.5xPc Pa ","NIST Thermal Conductivity (CH4) at pressure 2xPc Pa","NIST Thermal Conductivity (CH4) at pressure 2.5xPc Pa", "polynomial fit Therm. Cond. Model at pressure 2.5xPc Pa",'Location','northeast','Orientation','horizontal','NumColumns',1,'FontSize',8);
xlabel('Temperature (K)')
ylabel('Therm. Cond. NIST (W/m*K)')
% xlim([Tmin Tmax])
% ylim([0 70])
grid on
print -dmeta


