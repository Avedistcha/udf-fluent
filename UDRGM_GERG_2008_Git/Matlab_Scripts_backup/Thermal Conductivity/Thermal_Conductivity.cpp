
#include<stdio.h>
#include <math.h>
#include <iostream>
using namespace std;
#define PCRIT 4.5992e+06 // (Pa) for Methane
int main() {
    double P=1*PCRIT;
    double Temp =582.692308;

    double Thermal_Conduct1;
    if (Temp <= 192) //curve fit for pressure 1*Pc for Temp <= 192 
    {
        double Thermal_Conduct_1[] = {-2.04549326505191e-08,5.59199640835146e-06,-0.00163074177582917,0.323301386207505};
        Thermal_Conduct1 = (Thermal_Conduct_1[0]*pow(Temp,3)+ Thermal_Conduct_1[1]*pow(Temp,2)+ Thermal_Conduct_1[2]*pow(Temp,1)+ Thermal_Conduct_1[3]);
    }
    else //curve fit for pressure 1*Pc for Temp > 192 
    {
        double Thermal_Conduct_1[] = {-2.69500369782704e-14,5.73147260086157e-11,-4.80105164971084e-08,1.99419751429934e-05,-0.00397243698220043,0.331941044163246};
        Thermal_Conduct1 = (Thermal_Conduct_1[0]*pow(Temp,5)+ Thermal_Conduct_1[1]*pow(Temp,4)+ Thermal_Conduct_1[2]*pow(Temp,3)+ Thermal_Conduct_1[3]*pow(Temp,2)+ Thermal_Conduct_1[4]*pow(Temp,1)+ Thermal_Conduct_1[5]);    
    }

    double Thermal_Conduct_2[] = {-9.49432907548459e-21,3.17380346905899e-17,-4.54546200191642e-14,3.63085127662804e-11,-1.76116261050216e-08,5.27565513416557e-06,-0.000942271427550855,0.0899070302007644,-3.32137410604067}; //curve fit for pressure 2.5*Pc
    double Thermal_Conduct2 = (Thermal_Conduct_2[0]*pow(Temp,8)+ Thermal_Conduct_2[1]*pow(Temp,7)+ Thermal_Conduct_2[2]*pow(Temp,6)+ Thermal_Conduct_2[3]*pow(Temp,5)+ Thermal_Conduct_2[4]*pow(Temp,4)+ Thermal_Conduct_2[5]*pow(Temp,3)+ Thermal_Conduct_2[6]*pow(Temp,2)+ Thermal_Conduct_2[7]*pow(Temp,1)+ Thermal_Conduct_2[8]);

    double P1 = 1.0*PCRIT;
    double P2 = 2.5*PCRIT;
    double ktc = (P-P2)*(Thermal_Conduct2-Thermal_Conduct1)/(P2-P1)+Thermal_Conduct2;
    printf("Temp %f, Thermal_Conduct1 %f, Thermal_Conduct2 %f, ktc %f\n",Temp, Thermal_Conduct1, Thermal_Conduct2, ktc);

    // cout << "PI = " << Visco1[0] << endl;
    // printf("Temp %f, Vis1 %f, Vis2 %f, mu %f\n",Temp, Vis1, Vis2, mu);
    return 0;
}