# UDRGM for CH4 (One Radical) 

UDRGM for CH4 will be elaborated here, read below the sections that will develloped here.

## Code_Template
The following code can be used but it also serves as a **Template** to model other spiecie instead of CH4 using GERG-2008 model, if the same methods is applied and the specific intrinsic parameters are used for the relevent spiecie, for that purpose please read the Thesis report :rocket:.
 

## GERG_UDRGM_ note
In this section we look forward to develop our own custom Physical Properties usin GERG-2008..\
You will find a seperate C++ Script folder and a Matlab Script folder, these were used to develop or verify the main UDF_GERG_2008_Model.c code.


### User Defined Physical properties Used for our sample codes 

| Properties                  | Model/Law        | 
| -------------               | -------------    | 
| **Density**                 | GERG-2008.       | 
| **enthalpy:**               | GERG-2008        | 
| **entropy:**                | GERG-2008        | 
| **specific_heat:**          | GERG-2008        | 
| **molecular_weight:**       | Constant         | 
| **speed_of_sound:**         | GERG-2008        | 
| **viscosity:**              | Variable polynomial model NIST   | 
| **thermal_conductivity:**   | Variable polynomial model NIST   | 
| **drho/dT at const p:**       | GERG-2008        | 
| **drho/dp at const T:**       | Ideal Gas        | 
| **dh/dT at const p:**         | Ideal Gas        |       
| **dh/dp at const T:**         | GERG-2008        |       


### Returned Physical properties to Fluent which is common to all code involving Multiple Spiecies
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow. (check the code)

### Loading the code using TUI common to all multiple spiecies UDRGM
```
define/user-defined/real-gas-models/user-defined-real-gas-model
```
```
use user-defined real gas? [no]  
```
type yes
```
use user-defined real gas? [no] yes
```
```
User-Defined Realgas Library Name [""]  
```
type the generated lib folder name, by default it is libudf unless you named it differently
```
User-Defined Realgas Library Name [""] libudf
```
<img src="Images/rocket-launch.svg" width="300" height="300">
