﻿#include <math.h>
#include <stdio.h>  

#define UNIVERSAL_GAS_CONSTANT (8.314*1000)//KJ./mol.K (8.314472*1000) //!!!!!!!!!!!!!!!!!Optional!!!!!!!!!!!!!!!!!!!!!!!!

#define MW 16.04246  // (Kg/Kmol) molec. wt. for single gas, In our Case Methane CH4 
#define RGAS (UNIVERSAL_GAS_CONSTANT/MW)// UNIVERSAL_GAS_CONSTANT=8.314*1000 (J./Kg.K)
#define PCRIT 4.5992e+06 // (Pa) for Methane
#define M (MW/1000)//Kg./mol

double Temp;
double rho;
double R = RGAS; //J/Kg.K
double Temp_c_1 = 190.564;//K
double rho_c_1 = 10.139342719*M*1000;//Kg/m^3
int Kpol = 6;
int Kexp = 18;
double n_o_1_k [] = {0.57335704239162,-0.16760687523730*10,0.23405291834916,-0.21947376343441,0.16369201404128/10,0.15004406389280/10,0.98990489492918/10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061/10,-0.15782558339049,0.12716735220791,-0.32019743894346/10,-0.68049729364536/10,0.24291412853736/10,0.51440451639444/100,-0.19084949733532/10,0.55229677241291/100,-0.44197392976085/100,0.40061416708429/10,-0.33752085907575/10,-0.25127658213357/100};
double d_o_1_k []= {1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7}; 
double c_o_1_k []= {0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6};
double t_o_1_k []= {0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000};
double n_o_1_k__o [] = { 19.597508817, -83.959667892, 3.00088, 0.76315, 0.00460, 8.74432 , -4.46921 };
double theta_o_1_k__o[] = { 0, 0, 0, 4.306474465, 0.936220902, 5.577233895, 5.722644361 };
double R_star = 8.314510/M; // J/(mol*K)

double alpha_delta__r__function(double rho, double Temp);
double alpha_delta_delta__r__function(double rho, double Temp);
double alpha_delta_taw__r__function(double rho, double Temp);
double alpha_taw_taw__r__function(double rho, double Temp);
double alpha_taw_taw__o__function(double rho, double Temp);

//the folowing 2 function are common for Enthalpy and entropy
double alpha_taw__o__function(double rho, double Temp);
double alpha_taw__r__function(double rho, double Temp);
//the folowing 2 function are for  entropy
double alpha_o_i__o__function(double rho, double Temp);
double alpha_o_i__r__function(double rho, double Temp);

double Cp_Methane_GERG(double density, double Temp)
{
    // double R = 8.314472; // J/(mol*K)

    double delta = density/rho_c_1;
    double taw = Temp_c_1/Temp;

    double cpi = R * (-pow(taw, 2) * (alpha_taw_taw__o__function(density,Temp) + alpha_taw_taw__r__function(density,Temp)) + pow(1 + delta * alpha_delta__r__function(density,Temp) - delta * taw * alpha_delta_taw__r__function(density,Temp), 2) / (1 + 2 * delta * alpha_delta__r__function(density,Temp) + pow(delta, 2)*alpha_delta_delta__r__function(density,Temp)));

    return cpi;
}

int main()
{
    double rho = 21.92756163; 
    double Temp= 300;
    // double cp = Cp_Methane_GERG(rho, T); //cp (J/mol*K) 42.089 for rho = 40; T= 300;
    // alpha_o_i__o__function( rho,  Temp);
    // alpha_o_i__r__function( rho,  Temp);

    double delta = rho/rho_c_1;
    double taw = Temp_c_1/Temp;
    // double enthalpy;
    // double h0 = -910990; //Refference Enthalpy
    // enthalpy = R * Temp*(1 + taw * (alpha_taw__o__function( rho, Temp)+ alpha_taw__r__function( rho, Temp))+ delta*alpha_delta__r__function( rho, Temp))-h0;//H=h+h0 ->> h=H-h0

    double s0 = -6676.467652; //Refference Entropy
    double entropy = R * (taw*(alpha_taw__o__function( rho, Temp)+alpha_taw__r__function( rho, Temp))-alpha_o_i__o__function(rho, Temp)-alpha_o_i__r__function(rho,Temp))-s0;
    printf(" the value of entropy  %f (J/Kg) \n ", entropy);
    // printf(" the value of enthalpy  %f (J/Kg) \n ", enthalpy);// at 180 575160 -335794.871332
    // printf(" Difference: C++: %f NIST: %f (J/Kg) \n ", 741510.00-575160.00, 335794.871332-169499.780115);
    // at 250    	 -169499.780115
    // h250-h180= H250-H180 , H=h-h180+H180
    // int i= 0;
    // double cp[600-150];
    // for (int T = 150; T < 600; T++){-
    // // double T = 249.69;
    //     cp[i] = Cp_Methane_GERG(rho, T);
    //     // printf("%d\n ", T);
    //     // printf("%d\n ", cp[i]);
    //     printf("%d, %d, %f\n",i, T, cp[i]/1000);
    //     i=i+1;
    // }

    // printf(" the value of Cp  %f (J/Kg/K) \n ", cp);
 
    return 0;
}
// *****************Internal GERG Prameter Function Starts*****************

double alpha_delta__r__function(double rho, double Temp)
{
  //EQUATION (16)Start
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;
  //EQUATION (16)ENDS

  //EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
  double DalphaOi_r_Ddelta_taw_1 = 0;
  for (int i = 0; i<Kpol; ++i) { //1--->6  
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + n_o_1_k[i]*d_o_1_k[i]*pow(delta,d_o_1_k[i]-1)*pow(taw,t_o_1_k[i]);
    //Message0("DalphaOi_r_Ddelta_taw_1  first i =  %d   .\n",i);
  }  
  for (int i = Kpol; i<Kpol+Kexp; ++i){ //  7--->24 
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + n_o_1_k[i]*pow(delta,d_o_1_k[i]-1)*(d_o_1_k[i]-c_o_1_k[i]*pow(delta,c_o_1_k[i]))*pow(taw,t_o_1_k[i])*exp(-pow(delta,c_o_1_k[i]));
    //Message0("DalphaOi_r_Ddelta_taw_1  second i =  %d   .\n",i);
  }
  
  //EQUATION (alpha_delta__r)Starts
  double alpha_delta__r = DalphaOi_r_Ddelta_taw_1; 
  //EQUATION (alpha_delta__r)ENDS
  // printf(" Temp %f\n ", Temp);
  // printf(" the value of alpha_delta__r %f\n ", alpha_delta__r);
  return alpha_delta__r;
}

double alpha_delta_delta__r__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  double alpha_delta_delta__r = 0;
  for (int k = 0; k < Kpol; k++)
  {
      alpha_delta_delta__r = alpha_delta_delta__r + (n_o_1_k[k] * d_o_1_k[k] * (d_o_1_k[k] - 1) * pow(delta, d_o_1_k[k] - 2) * pow(taw, t_o_1_k[k]));
  }

  for (int k = Kpol; k < (Kpol + Kexp); k++)
  {
      alpha_delta_delta__r = alpha_delta_delta__r + (n_o_1_k[k] * pow(delta, d_o_1_k[k] - 2) * ((d_o_1_k[k] - c_o_1_k[k] * pow(delta, c_o_1_k[k])) * (d_o_1_k[k] - 1 - c_o_1_k[k] * pow(delta, c_o_1_k[k])) - pow(c_o_1_k[k], 2) * pow(delta, c_o_1_k[k])) * pow(taw, t_o_1_k[k]) * exp(-pow(delta, c_o_1_k[k])));

  }
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_delta_delta__r %f\n ", alpha_delta_delta__r);
  return alpha_delta_delta__r; 
}

double alpha_delta_taw__r__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  double alpha_delta_taw__r = 0;
  for (int k = 0; k < Kpol; k++)
  {
      alpha_delta_taw__r = alpha_delta_taw__r + (n_o_1_k[k] * d_o_1_k[k] * t_o_1_k[k] * pow(delta, d_o_1_k[k] - 1) * pow(taw, t_o_1_k[k] - 1));
  }

  for (int k = Kpol; k < (Kpol + Kexp); k++)
  {
      alpha_delta_taw__r = alpha_delta_taw__r + (n_o_1_k[k] * t_o_1_k[k] * pow(delta, d_o_1_k[k] - 1) * (d_o_1_k[k] - c_o_1_k[k] * pow(delta, c_o_1_k[k])) * pow(taw, t_o_1_k[k] - 1) * exp(-pow(delta, c_o_1_k[k])));

  }
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_delta_taw__r %f\n ", alpha_delta_taw__r);
  return alpha_delta_taw__r;
}

double alpha_taw_taw__r__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  
  double alpha_taw_taw__r = 0;
  for (int k = 0; k < Kpol; k++)
  {
      alpha_taw_taw__r = alpha_taw_taw__r + (n_o_1_k[k] * t_o_1_k[k] * (t_o_1_k[k] - 1) * pow(delta, d_o_1_k[k]) * pow(taw, t_o_1_k[k] - 2));
  }

  for (int k = Kpol; k < (Kpol + Kexp); k++)
  {
      alpha_taw_taw__r = alpha_taw_taw__r + (n_o_1_k[k] * t_o_1_k[k] * (t_o_1_k[k] - 1) * pow(delta, d_o_1_k[k]) * pow(taw, t_o_1_k[k] - 2) * exp(-pow(delta, c_o_1_k[k])));

  }
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_taw_taw__r %f\n ", alpha_taw_taw__r);
  return alpha_taw_taw__r;
}

double alpha_taw_taw__o__function(double density, double T)
{
  double delta = density/rho_c_1;
  double taw = Temp_c_1/T;
  double alpha_taw_taw__o_1 = 0;
  double alpha_taw_taw__o_2 = 0;

  for (int k = 3; k < 7; k++)
  {
      if (k == 3 || k == 5) {
        alpha_taw_taw__o_1 = alpha_taw_taw__o_1 + (n_o_1_k__o[k] * pow(theta_o_1_k__o[k], 2) / pow(sinh(theta_o_1_k__o[k] * Temp_c_1 / T), 2));
      }

      else {
        alpha_taw_taw__o_2 = alpha_taw_taw__o_2 + (n_o_1_k__o[k] * pow(theta_o_1_k__o[k], 2) / pow(cosh(theta_o_1_k__o[k] * Temp_c_1 / T), 2));
      } 
  }
  double alpha_taw_taw__o = R_star / R * (-n_o_1_k__o[2] * pow(T / Temp_c_1, 2) - alpha_taw_taw__o_1 - alpha_taw_taw__o_2);
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_taw_taw__o %f\n ", alpha_taw_taw__o);
  return alpha_taw_taw__o;
}

double alpha_taw__o__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;
  double alpha_taw__o_1 = 0;
  double alpha_taw__o_2 = 0;

  for (int k = 3; k < 7; k++)
  {
      if (k == 3 || k == 5) {
        alpha_taw__o_1 = alpha_taw__o_1 + (n_o_1_k__o[k] * theta_o_1_k__o[k] / tanh(theta_o_1_k__o[k] * Temp_c_1 / Temp));
      }

      else {
        alpha_taw__o_2 = alpha_taw__o_2 + (n_o_1_k__o[k] * theta_o_1_k__o[k] * tanh(theta_o_1_k__o[k] * Temp_c_1 / Temp));
      } 
  }
  double alpha_taw_taw__o = R_star / R * (n_o_1_k__o[1] + n_o_1_k__o[2] * Temp / Temp_c_1 + alpha_taw__o_1 - alpha_taw__o_2);
  printf(" Temp %f\n ", Temp);
  printf(" the value of alpha_taw_taw__o %f\n ", alpha_taw_taw__o);
  return alpha_taw_taw__o;
}

double alpha_taw__r__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;

  double alpha_taw__r = 0;
  for (int i = 0; i<Kpol; ++i) { //1--->6  
    alpha_taw__r = alpha_taw__r + n_o_1_k[i]*t_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i]-1);
    //Message0("alpha_taw__r  first i =  %d   .\n",i);
  }  
  for (int i = Kpol; i<Kpol+Kexp; ++i){ //  7--->24 
    alpha_taw__r = alpha_taw__r + n_o_1_k[i]*t_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i]-1)*exp(-pow(delta,c_o_1_k[i]));
    //Message0("alpha_taw__r  second i =  %d   .\n",i);
  } 
  // printf(" Temp %f\n ", Temp);
  printf(" the value of alpha_taw__r %f\n ", alpha_taw__r);
  return alpha_taw__r; 
}
double alpha_o_i__o__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;
  double alpha_o_i__o_1 = 0; 
  double alpha_o_i__o_2 = 0; 

  for (int k = 3; k < 7; k++)
  {
      if (k == 3 || k == 5) {
        alpha_o_i__o_1 = alpha_o_i__o_1 + (n_o_1_k__o[k] *log (sqrt(   pow(sinh(theta_o_1_k__o[k] * taw),2)    )   )   ); 
      }

      else {
        alpha_o_i__o_2 = alpha_o_i__o_2 + (n_o_1_k__o[k]  * log(cosh(theta_o_1_k__o[k] * taw)));
      } 
  }

  double alpha_o_i__o = log (delta)+ R_star / R * (n_o_1_k__o[0] + n_o_1_k__o[1] * taw + n_o_1_k__o[2] * log (taw) + alpha_o_i__o_1 - alpha_o_i__o_2);
  // printf(" the value of alpha_o_i__o  %f (J/Kg) \n ", alpha_o_i__o);
  return alpha_o_i__o;
}

double alpha_o_i__r__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;

  double alpha_o_i__r = 0;
  for (int i = 0; i<Kpol; ++i) { //1--->6  
    alpha_o_i__r = alpha_o_i__r + n_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i]);
    //Message0("alpha_taw__r  first i =  %d   .\n",i);
  }  
  for (int i = Kpol; i<Kpol+Kexp; ++i){ //  7--->24 
    alpha_o_i__r = alpha_o_i__r + n_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i])*exp(-pow(delta,c_o_1_k[i]));
    //Message0("alpha_taw__r  second i =  %d   .\n",i);
  } 
  // printf(" Temp %f\n ", Temp);
  printf(" the value of alpha_o_i__r %f\n ", alpha_o_i__r);
  return alpha_o_i__r; 
}