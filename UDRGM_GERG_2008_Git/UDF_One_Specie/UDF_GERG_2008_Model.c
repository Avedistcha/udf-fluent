/**********************************************************************/
/* User Defined Real Gas Model : GERG Equation for Real Gas Modeling  */
/* Author: Avedis Tchamitchian                                        */
/* Email: avedistchamitchi@gmail.com                                  */
/* Date: August 2022                                                  */
/* Version: 1.0                                                       */
/* Parameters set for CH4        .                                    */
/* define/user-defined/real-gas-models/user-defined-real-gas-model    */
/**********************************************************************/

/*********************© 2022 All Right Reserved************************/

#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

/**
 * @brief Below are constant parameters relevant for Methane CH4
 * Please consult GERG 2008 manual to get the values of n_o_1_k d_o_1_k c_o_1_k t_o_1_k....
 *
 */
#define MW 16.04246  // (Kg/Kmol) molec. wt. for single gas, In our Case Methane CH4 
#define RGAS (UNIVERSAL_GAS_CONSTANT/MW)// UNIVERSAL_GAS_CONSTANT=8.314*1000
#define PCRIT 4.5992e+06 // (Pa) for Methane
#define M (MW/1000)//Kg./mol

double Temp;
double rho;
double R = RGAS; //J/Kg.K
double Temp_c_1 = 190.564;//K
double rho_c_1 = 10.139342719*M*1000;//Kg/m^3
int Kpol = 6;
int Kexp = 18;
double n_o_1_k [] = {0.57335704239162,-0.16760687523730*10,0.23405291834916,-0.21947376343441,0.16369201404128/10,0.15004406389280/10,0.98990489492918/10,0.58382770929055,-0.74786867560390,0.30033302857974,0.20985543806568,-0.18590151133061/10,-0.15782558339049,0.12716735220791,-0.32019743894346/10,-0.68049729364536/10,0.24291412853736/10,0.51440451639444/100,-0.19084949733532/10,0.55229677241291/100,-0.44197392976085/100,0.40061416708429/10,-0.33752085907575/10,-0.25127658213357/100};
double d_o_1_k []= {1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7}; 
double c_o_1_k []= {0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6};
double t_o_1_k []= {0.125,1.125,0.375,1.125,0.625,1.500,0.625,2.625,2.750,2.125,2.000,1.750,4.500,4.750,5.000,4.000,4.500,7.500,14.000,11.500,26.000,28.000,30.000,16.000};
double n_o_1_k__o [] = { 19.597508817, -83.959667892, 3.00088, 0.76315, 0.00460, 8.74432 , -4.46921 };
double theta_o_1_k__o[] = { 0, 0, 0, 4.306474465, 0.936220902, 5.577233895, 5.722644361 };
double R_star = 8.314510/M; // J/(mol*K)


static int (*usersMessage)(const char *, ...);
static void (*usersError)(const char *, ...);


/* Function decleration STARTS  */
double bisection(double Temp, double press);
double density(double a, double Temp, double press);
double alpha_delta__r__function(double rho, double Temp);
double alpha_delta_delta__r__function(double rho, double Temp);
double alpha_delta_taw__r__function(double rho, double Temp);
double alpha_taw_taw__r__function(double rho, double Temp);
double alpha_taw_taw__o__function(double rho, double Temp);
double alpha_taw__o__function(double rho, double Temp);
double alpha_taw__r__function(double rho, double Temp);
double alpha_o_i__o__function(double rho, double Temp);
double alpha_o_i__r__function(double rho, double Temp);
/* Function decleration Ends  */


DEFINE_ON_DEMAND(I_do_nothing)
{
  /* This is a dummy function to allow us to use */
  /* the Compiled UDFs utility      */
}

void GERG_error(int err, char *f, char *msg)
{
  if (err)
    usersError("GERG_error (%d) from function: %s\n%s\n", err, f, msg);
}

void GERG_Setup(Domain *domain, cxboolean vapor_phase, char *filename,
                 int (*messagefunc)(const char *format, ...),
                 void (*errorfunc)(const char *format, ...))
{
  /* Use this function for any initialization or model setups*/
  usersMessage = messagefunc;
  usersError  = errorfunc;
  usersMessage("\nLoading Real-GERG Library: %s\n", filename);

/* Prints upper triangle STARTS */
  int i,j,rows;
  rows = 15;
  for(i = 0; i < rows; i++) {
      /* Prints one row of triangle */
      for(j = 0; j <= i; ++j) {
          Message0("* ");
      }
      /* move to next row */
      Message0("\n");
  }
/* Prints upper triangle ENDS */
  Message0("Copyright: © 2022 All Right Reserved. TUM Avedis Tchamitchian avedistchamitchi@gmail.com .\n");
/* Prints Lower triangle STARTS */
  for(i = rows; i >= 1; i--)
  {
    for(j = 1; j <= i; j++)
    {
    Message0("* ");
    }
    // ending line after each row
    Message0("\n");
    }
  /* Prints Lower triangle ENDS */
}

double GERG_density(cell_t cell, Thread *thread,
                     cxboolean vapor_phase, double Temp, double press, double yi[])
{
  // double rho = 300;
  double rho = bisection(Temp, press); /* Density at Temp & press */
  // Message0("rho-GERG     %f   .\n",rho);
  // Message0("press        %f   .\n",press);
  // Message0("RGAS_CH4  %f   .\n",RGAS);
  // Message0("Temp         %f   .\n",Temp);
  // Message0("%f, %f, %f\n",press, Temp, rho);
  // Message0("************************* .\n");
  return rho;      /* (Kg/m^3) */
}

double GERG_specific_heat(cell_t cell, Thread *thread,
                           double Temp, double density, double P, double yi[])
{
  double delta = density/rho_c_1;
  double taw = Temp_c_1/Temp;
  double cp = R * (-pow(taw, 2) * (alpha_taw_taw__o__function(density,Temp) + alpha_taw_taw__r__function(density,Temp)) + pow(1 + delta * alpha_delta__r__function(density,Temp) - delta * taw * alpha_delta_taw__r__function(density,Temp), 2) / (1 + 2 * delta * alpha_delta__r__function(density,Temp) + pow(delta, 2)*alpha_delta_delta__r__function(density,Temp)));
  double cp_allowed = 400e+03;
  if (cp > cp_allowed)
  {
    Message0("ERROR: Cp is > %f (J/Kg/K), GERG Equation doesnt cover 2 phase flow! (vapor+liquid) .\n",cp_allowed);
    Message0("(Pressure: %f Pa, Temperature: %f K, density: %f (Kg/m^3), Cp: %f (J/Kg/K))\n",P, Temp, density, cp);
    cp = 2222;//rescue number so that Fluent doesnt crash
  }

  if (cp <= 0)
  { 
    Message0("ERROR: Cp is <0, GERG Equation doesnt cover 2 phase flow! (vapor+liquid) .\n");
    Message0("(Pressure: %f, Temperature: %f, density: %f, Cp: %f)\n",P, Temp, density, cp);
    cp = 2222;//rescue  number so that Fluent doesnt crash
  }
  // cp = 2222;//Methane CH4 
  // Message0("P:%f ,D:%f ,T:%f ,Cp:%f \n",P, density, Temp, cp);
  return cp;     /* (J/Kg/K) */
}

double GERG_enthalpy(cell_t cell, Thread *thread,
                      double Temp, double density, double P, double yi[])
{
  // double h = Temp * GERG_specific_heat(cell, thread, Temp, density, P, yi); // For Ideal Gas
  double delta = density/rho_c_1;
  double taw = Temp_c_1/Temp;
  double enthalpy;
  double h0 = -910990;/* (J/Kg) */ //Refference Enthalpy
  enthalpy = R * Temp*(1 + taw * (alpha_taw__o__function( density, Temp)+ alpha_taw__r__function( density, Temp))+ delta*alpha_delta__r__function( density, Temp))-h0;//H=h+h0 ->> h=H-h0
  return enthalpy;      /* (J/Kg) */
}

// #define TDatum 288.15
// #define PDatum 1.01325e5

double GERG_entropy(cell_t cell, Thread *thread,
                     double Temp, double density, double P, double yi[])
{
  // double s = GERG_specific_heat(cell, thread, Temp, density, P, yi) * log(fabs(Temp / TDatum)) +
  //            RGAS * log(fabs(PDatum / P));
  double delta = density/rho_c_1;
  double taw = Temp_c_1/Temp;
  double s0 = -6676.467652;  /* (J/Kg/K) */ //Refference Entropy
  double entropy;
  if (Temp <= 301.0 && Temp >= 299.0 )
  { 
    entropy = 4332;//rescue  number so that Fluent doesnt crash
    Message0("ERROR: wrong Entropy 4332 (J/Kg/K) will be taken .\n");
    Message0("(Pressure: %f, Temperature: %f, density: %f, entropy: %f)\n",P, Temp, density, entropy);
  }
  else 
  {
    entropy = R * (taw*(alpha_taw__o__function( density, Temp)+alpha_taw__r__function( density, Temp))-alpha_o_i__o__function(density, Temp)-alpha_o_i__r__function(density,Temp))-s0;  /* s= S-s0 */

  }
  return entropy;      /* (J/Kg/K) */
}


double GERG_mw(double yi[])
{
  return MW;     /* (Kg/Kmol) */
}

double GERG_speed_of_sound(cell_t cell, Thread *thread,
                            double Temp, double density, double P, double yi[])
{
  // double cp = GERG_specific_heat(cell, thread, Temp, density, P, yi);
  // return sqrt(Temp * cp * RGAS / (cp - RGAS)); /* m/s */
  double delta = density/rho_c_1;
  double taw = Temp_c_1/Temp;
  double w = R*Temp*(1+2*delta*alpha_delta__r__function(density,Temp)+delta*delta*alpha_delta_delta__r__function(density,Temp)-pow((1+delta*alpha_delta__r__function(density,Temp)-delta*taw*alpha_delta_taw__r__function(density,Temp)),2)/(taw*taw*(alpha_taw_taw__o__function(density,Temp)+alpha_taw_taw__r__function(density,Temp))));
  return sqrt(w);  /* m/s */
}
/**
 * @brief 
 * Calculation of Gas Viscosity by 2 bounded polynomial curve fit for pressure 1*Pc and 2.5*Pc
 * @param Visco1  viscosity Vs Temp fpr P=1*Pc, the equation is generated using polyfit in Matlab
 * @param Visco2  viscosity Vs Temp fpr P=2.5*Pc, the equation is generated using polyfit in Matlab
 * @param Temp 
 * @param P if P = 1.0*PCRIT then viscosity=Visco1 if P = 2.5*PCRIT then viscosity=Visco2 
 *  if P is in between then we do an interpolation 
 * @return double 
 */
double GERG_viscosity(cell_t cell, Thread *thread,
                       double Temp, double density, double P, double yi[])
{

  // Calculation of Gas Viscosity by 2 bounded polynomial curve fit for pressure 1*Pc and 2.5*Pc
  double Visco1[] = {3.66414198468758e-13,-9.29488859860330e-10,9.63315568471851e-07,-0.000521178582330469,0.154945699846419,-23.9241331203175,1504.63781375729};
  double Vis1 = (Visco1[0]*pow(Temp,6)+ Visco1[1]*pow(Temp,5)+ Visco1[2]*pow(Temp,4)+ Visco1[3]*pow(Temp,3)+ Visco1[4]*pow(Temp,2)+ Visco1[5]*Temp+ Visco1[6]);
  double Visco2[] = {-3.64280681827070e-11,8.16190884558272e-08,-7.20666369075405e-05,0.0313191470216509,-6.67263853262221,570.123618954823};
  double Vis2 = (Visco2[0]*pow(Temp,5)+ Visco2[1]*pow(Temp,4)+ Visco2[2]*pow(Temp,3)+ Visco2[3]*pow(Temp,2)+ Visco2[4]*Temp+ Visco2[5]);
  double P1 = 1.0*PCRIT;
  double P2 = 2.5*PCRIT;
  double mu = (P-P2)*(Vis2-Vis1)/(P2-P1)+Vis2;
  // Message0("Temp %f, Vis1 %f, Vis2 %f, mu %f\n",Temp, Vis1, Vis2, mu);
  return mu*1e-06;     /* (Kg/m/s) */
}


/**
 * @brief 
 * Calculation of Gas thermal_conductivity by 3 bounded polynomial curve fit for pressure 1*Pc for Temp <= 192 and Temp > 192 and for 2.5*Pc
 * @param Thermal_Conduct1  thermal_conductivity Vs Temp For P=1*Pc for Temp <= 192 and Temp > 192, the equation is generated using polyfit in Matlab
 * @param Thermal_Conduct2  thermal_conductivity Vs Temp For P=2.5*Pc, the equation is generated using polyfit in Matlab
 * @param Temp 
 * @param P if P = 1.0*PCRIT then thermal_conductivity=Thermal_Conduct1 if P = 2.5*PCRIT then thermal_conductivity=Thermal_Conduct2 
 *  if P is in between then we do an interpolation 
 * @return double 
 */
double GERG_thermal_conductivity(cell_t cell, Thread *thread,
                                  double Temp, double density, double P,
                                  double yi[])
{
  // double ktc = 0.0332; //Methane CH4 ************************************************************************************************
  // Calculation of Gas thermal_conductivity by 3 bounded polynomial curve fit for pressure 1*Pc for Temp <= 200 and Temp > 200 and for 2.5*Pc
  double Thermal_Conduct1;
  if (Temp <= 200) //curve fit for pressure 1*Pc for Temp <= 200 
  {
    double Thermal_Conduct_1[] = {-2.04549326505191e-08,5.59199640835146e-06,-0.00163074177582917,0.323301386207505};
    Thermal_Conduct1 = (Thermal_Conduct_1[0]*pow(Temp,3)+ Thermal_Conduct_1[1]*pow(Temp,2)+ Thermal_Conduct_1[2]*pow(Temp,1)+ Thermal_Conduct_1[3]);
  }
  else //curve fit for pressure 1*Pc for Temp > 200 
  {
    double Thermal_Conduct_1[] = {-2.69500369782704e-14,5.73147260086157e-11,-4.80105164971084e-08,1.99419751429934e-05,-0.00397243698220043,0.331941044163246};
    Thermal_Conduct1 = (Thermal_Conduct_1[0]*pow(Temp,5)+ Thermal_Conduct_1[1]*pow(Temp,4)+ Thermal_Conduct_1[2]*pow(Temp,3)+ Thermal_Conduct_1[3]*pow(Temp,2)+ Thermal_Conduct_1[4]*pow(Temp,1)+ Thermal_Conduct_1[5]);    
  }

  double Thermal_Conduct_2[] = {-9.49432907548459e-21,3.17380346905899e-17,-4.54546200191642e-14,3.63085127662804e-11,-1.76116261050216e-08,5.27565513416557e-06,-0.000942271427550855,0.0899070302007644,-3.32137410604067}; //curve fit for pressure 2.5*Pc
  double Thermal_Conduct2 = (Thermal_Conduct_2[0]*pow(Temp,8)+ Thermal_Conduct_2[1]*pow(Temp,7)+ Thermal_Conduct_2[2]*pow(Temp,6)+ Thermal_Conduct_2[3]*pow(Temp,5)+ Thermal_Conduct_2[4]*pow(Temp,4)+ Thermal_Conduct_2[5]*pow(Temp,3)+ Thermal_Conduct_2[6]*pow(Temp,2)+ Thermal_Conduct_2[7]*pow(Temp,1)+ Thermal_Conduct_2[8]);
  
  double P1 = 1.0*PCRIT;
  double P2 = 2.5*PCRIT;
  double ktc = (P-P2)*(Thermal_Conduct2-Thermal_Conduct1)/(P2-P1)+Thermal_Conduct2;
  // Message0("Temp %f, Thermal_Conduct1 %f, Thermal_Conduct2 %f, ktc %f\n",Temp, Thermal_Conduct1, Thermal_Conduct2, ktc);
  return ktc;      /* W/m/K */
}


//GERG
double GERG_rho_t(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. Temp at constant p */
  double delta = density/rho_c_1;
  double taw = Temp_c_1/Temp;

  double rho_t = density*(1+delta*alpha_delta__r__function(density, Temp)-delta*taw*alpha_delta_taw__r__function( density, Temp)) / (Temp*(1+2*delta*alpha_delta__r__function(density, Temp)+delta*delta*alpha_delta_delta__r__function(density,Temp)));
  double rho_t_2 = -density / Temp;
  Message0("rho_t %f, rho_t_2 %f\n",rho_t, rho_t_2);
  return rho_t;     /* (Kg/m^3/K) */
}

double GERG_rho_p(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. pressure at constant T */
  // Ideal Gas condition will be considered for this code which should be modefied for future version
  double rho_p = 1.0 / (RGAS * Temp);
  return rho_p;    /* (Kg/m^3/Pa) */
}

double GERG_enthalpy_t(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. Temp at constant p */
  return GERG_specific_heat(cell, thread, Temp, density, P, yi);
}



double GERG_enthalpy_p(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. pressure at constant T  */
  /* general form dh/dp|T = (1/rho)*{ 1 + (T/rho)*drho/dT|p} */
  /* but for Ideal gas and GERG equation dh/dp = 0        */
  return 0.0 ;
}

UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
  GERG_Setup,                   /* initialize */
  GERG_density,                 /* density */
  GERG_enthalpy,                /* enthalpy */
  GERG_entropy,                 /* entropy */
  GERG_specific_heat,           /* specific_heat */
  GERG_mw,                      /* molecular_weight */
  GERG_speed_of_sound,          /* speed_of_sound */
  GERG_viscosity,               /* viscosity */
  GERG_thermal_conductivity,    /* thermal_conductivity */
  GERG_rho_t,                   /* drho/dT |const p */
  GERG_rho_p,                   /* drho/dp |const T */
  GERG_enthalpy_t,              /* dh/dT |const p  */
  GERG_enthalpy_p               /* dh/dp |const T  */
};
/**************************************************************/


/**
 * @brief 
 * solving GERG equation for density by using the Bysection method
 * Below will be the bisection method, a ,b and Tcrucial are deducted from plots generated in Matlab.
 * educing the {a b} range will reduce the computing time and will be someimes mandatory if multiple 
 * roots are existing, providing the right bound will assure you a physical root for your equation
 * @param Temp temperature
 * @param press Pressure
 * @param a rho bysection lower bound
 * @param b rho bysection upper bound
 * @param c Will update the density range during the bisection method
 * @param Tcrucial This is deducted visually from Matlab plots
 * @return double 
 */

double bisection(double Temp, double press)
{
  double a=5; 
  double b=378;
  double c;
  int Tcrucial = 177;

  double PCRITI= PCRIT;
  if (Temp < Tcrucial && press >= 1.0*PCRITI) //calibration for choosing the bysection range value since multiple root is detected and depends on which starting interval you choose, so one should choose explicitely define this after consulting the NIST plots for different pressure 
  {
    a = 300;
    b = 378; 
    // Message0("a p>PCRIT %f   .\n",a);
    // Message0("b p>PCRIT %f   .\n",b);
  }
  if (Temp < Tcrucial && press < 1.0*PCRITI) //calibration for choosing the bysection range value since multiple root is detected and depends on which starting interval you choose, so one should choose explicitely define this after consulting the NIST plots for different pressure 
  {
    a = -0.053023256678360*pow(Temp,2) +14.678429720438771*Temp  -6.457093279284205e+02 -5;
    b = -0.028875000000002*pow(Temp,2) +7.037050000000574*Temp -40.199500000048992 +30; 
    // Message0("a p<PCRIT %f   .\n",a);
    // Message0("b p<PCRIT %f   .\n",b);
  }

  double density_a =density(a,Temp,press);
  double density_b =density(b,Temp,press);

  if (density_a * density_b >= 0)
  { 
    Message0("WARNING ERROR in Bisection Method \n");
    Message0("Incorrect a and b, wrong initial guessing, Please check your inicial boundary root guess parameters .\n");
  }
  
  c = a;
  while ((b - a) >= 0.1)
  {
    c = (a + b)/ 2;
    if (density(c,Temp,press) == 0.0)
    {
      break;
    }
    else if (density(c,Temp,press)*density(a,Temp,press) < 0)
    {
      b = c;
    }
    else
    {
      a = c;
    }
  }
  // Message0("press     %f   .\n",press);
  // Message0("RGAS_CH4  %f   .\n",RGAS);
  // Message0("Temp      %f   .\n",Temp);
  // Message0("density_a %f   .\n",density_a);
  // Message0("density_b %f   .\n",density_b);
  // Message0("rho-GERG  %f   .\n",c);
  // Message0("(%d, %d)\n", Temp, c);
  // Message0("************************* .\n");
  return c;
}

/**
 * @brief 
 * Below, the GERG denisty equation is taken on the right 
 * hand side and the roots is being computed, which results the 
 * density after applying the bysection method
 * @param rho Density
 * @param Temp Temperature
 * @param press Pressure
 * @return double 
 */
double density(double rho, double Temp, double press)
{
  double alpha_delta__r = alpha_delta__r__function(rho,Temp);
  //EQUATION (16)Start
  double rho_r = rho_c_1;
  double delta = rho/rho_r;
  //EQUATION (16)ENDS
  double f = rho*R*Temp*(1+delta*alpha_delta__r)-press;
  return f;
}

/**
 * @brief The below fuction alpha_delta__r__function computes and returns alpha_delta__r 
 * knowing the density rho and the temperature Temp 
 * @param rho 
 * @param Temp 
 * @return double 
 */
double alpha_delta__r__function(double rho, double Temp)
{
  //EQUATION (16)Start
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;
  //EQUATION (16)ENDS

  //EQUATION (DalphaOi_r_Ddelta_taw_1)Starts
  double DalphaOi_r_Ddelta_taw_1 = 0;
  for (int i = 0; i<Kpol; ++i) { //1--->6  
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + n_o_1_k[i]*d_o_1_k[i]*pow(delta,d_o_1_k[i]-1)*pow(taw,t_o_1_k[i]);
    //Message0("DalphaOi_r_Ddelta_taw_1  first i =  %d   .\n",i);
  }  
  for (int i = Kpol; i<Kpol+Kexp; ++i){ //  7--->24 
    DalphaOi_r_Ddelta_taw_1 = DalphaOi_r_Ddelta_taw_1 + n_o_1_k[i]*pow(delta,d_o_1_k[i]-1)*(d_o_1_k[i]-c_o_1_k[i]*pow(delta,c_o_1_k[i]))*pow(taw,t_o_1_k[i])*exp(-pow(delta,c_o_1_k[i]));
    //Message0("DalphaOi_r_Ddelta_taw_1  second i =  %d   .\n",i);
  }
  
  //EQUATION (alpha_delta__r)Starts
  double alpha_delta__r = DalphaOi_r_Ddelta_taw_1; 
  //EQUATION (alpha_delta__r)ENDS
  // printf(" Temp %f\n ", Temp);
  // printf(" the value of alpha_delta__r %f\n ", alpha_delta__r);
  return alpha_delta__r;
}

double alpha_delta_delta__r__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  double alpha_delta_delta__r = 0;
  for (int k = 0; k < Kpol; k++)
  {
      alpha_delta_delta__r = alpha_delta_delta__r + (n_o_1_k[k] * d_o_1_k[k] * (d_o_1_k[k] - 1) * pow(delta, d_o_1_k[k] - 2) * pow(taw, t_o_1_k[k]));
  }

  for (int k = Kpol; k < (Kpol + Kexp); k++)
  {
      alpha_delta_delta__r = alpha_delta_delta__r + (n_o_1_k[k] * pow(delta, d_o_1_k[k] - 2) * ((d_o_1_k[k] - c_o_1_k[k] * pow(delta, c_o_1_k[k])) * (d_o_1_k[k] - 1 - c_o_1_k[k] * pow(delta, c_o_1_k[k])) - pow(c_o_1_k[k], 2) * pow(delta, c_o_1_k[k])) * pow(taw, t_o_1_k[k]) * exp(-pow(delta, c_o_1_k[k])));

  }
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_delta_delta__r %f\n ", alpha_delta_delta__r);
  return alpha_delta_delta__r; 
}
double alpha_delta_taw__r__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  double alpha_delta_taw__r = 0;
  for (int k = 0; k < Kpol; k++)
  {
      alpha_delta_taw__r = alpha_delta_taw__r + (n_o_1_k[k] * d_o_1_k[k] * t_o_1_k[k] * pow(delta, d_o_1_k[k] - 1) * pow(taw, t_o_1_k[k] - 1));
  }

  for (int k = Kpol; k < (Kpol + Kexp); k++)
  {
      alpha_delta_taw__r = alpha_delta_taw__r + (n_o_1_k[k] * t_o_1_k[k] * pow(delta, d_o_1_k[k] - 1) * (d_o_1_k[k] - c_o_1_k[k] * pow(delta, c_o_1_k[k])) * pow(taw, t_o_1_k[k] - 1) * exp(-pow(delta, c_o_1_k[k])));

  }
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_delta_taw__r %f\n ", alpha_delta_taw__r);
  return alpha_delta_taw__r;
}
double alpha_taw_taw__r__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  
  double alpha_taw_taw__r = 0;
  for (int k = 0; k < Kpol; k++)
  {
      alpha_taw_taw__r = alpha_taw_taw__r + (n_o_1_k[k] * t_o_1_k[k] * (t_o_1_k[k] - 1) * pow(delta, d_o_1_k[k]) * pow(taw, t_o_1_k[k] - 2));
  }

  for (int k = Kpol; k < (Kpol + Kexp); k++)
  {
      alpha_taw_taw__r = alpha_taw_taw__r + (n_o_1_k[k] * t_o_1_k[k] * (t_o_1_k[k] - 1) * pow(delta, d_o_1_k[k]) * pow(taw, t_o_1_k[k] - 2) * exp(-pow(delta, c_o_1_k[k])));

  }
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_taw_taw__r %f\n ", alpha_taw_taw__r);
  return alpha_taw_taw__r;
}
double alpha_taw_taw__o__function(double rho, double T)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/T;
  double alpha_taw_taw__o_1 = 0;
  double alpha_taw_taw__o_2 = 0;

  for (int k = 3; k < 7; k++)
  {
      if (k == 3 || k == 5) {

          alpha_taw_taw__o_1 = alpha_taw_taw__o_1 + (n_o_1_k__o[k] * pow(theta_o_1_k__o[k], 2) / pow(sinh(theta_o_1_k__o[k] * Temp_c_1 / T), 2));


      }

      else {

          alpha_taw_taw__o_2 = alpha_taw_taw__o_2 + (n_o_1_k__o[k] * pow(theta_o_1_k__o[k], 2) / pow(cosh(theta_o_1_k__o[k] * Temp_c_1 / T), 2));

      } 

  }
  double alpha_taw_taw__o = R_star / R * (-n_o_1_k__o[2] * pow(T / Temp_c_1, 2) - alpha_taw_taw__o_1 - alpha_taw_taw__o_2);
  // printf(" Temp %f\n ", T);
  // printf(" the value of alpha_taw_taw__o %f\n ", alpha_taw_taw__o);
  return alpha_taw_taw__o;
}

double alpha_taw__o__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;
  double alpha_taw__o_1 = 0;
  double alpha_taw__o_2 = 0;

  for (int k = 3; k < 7; k++)
  {
      if (k == 3 || k == 5) {
        alpha_taw__o_1 = alpha_taw__o_1 + (n_o_1_k__o[k] * theta_o_1_k__o[k] / tanh(theta_o_1_k__o[k] * Temp_c_1 / Temp));
      }

      else {
        alpha_taw__o_2 = alpha_taw__o_2 + (n_o_1_k__o[k] * theta_o_1_k__o[k] * tanh(theta_o_1_k__o[k] * Temp_c_1 / Temp));
      } 
  }
  double alpha_taw_taw__o = R_star / R * (n_o_1_k__o[1] + n_o_1_k__o[2] * Temp / Temp_c_1 + alpha_taw__o_1 - alpha_taw__o_2);
  // printf(" Temp %f\n ", Temp);
  // printf(" the value of alpha_taw_taw__o %f\n ", alpha_taw_taw__o);
  return alpha_taw_taw__o;
}

double alpha_taw__r__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;

  double alpha_taw__r = 0;
  for (int i = 0; i<Kpol; ++i) { //1--->6  
    alpha_taw__r = alpha_taw__r + n_o_1_k[i]*t_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i]-1);
    //Message0("alpha_taw__r  first i =  %d   .\n",i);
  }  
  for (int i = Kpol; i<Kpol+Kexp; ++i){ //  7--->24 
    alpha_taw__r = alpha_taw__r + n_o_1_k[i]*t_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i]-1)*exp(-pow(delta,c_o_1_k[i]));
    //Message0("alpha_taw__r  second i =  %d   .\n",i);
  } 
  // printf(" Temp %f\n ", Temp);
  // printf(" the value of alpha_taw__r %f\n ", alpha_taw__r);
  return alpha_taw__r; 
}

double alpha_o_i__o__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;
  double alpha_o_i__o_1 = 0; 
  double alpha_o_i__o_2 = 0; 

  for (int k = 3; k < 7; k++)
  {
      if (k == 3 || k == 5) {
        alpha_o_i__o_1 = alpha_o_i__o_1 + (n_o_1_k__o[k] *log (sqrt(   pow(sinh(theta_o_1_k__o[k] * taw),2)    )   )   ); 
      }

      else {
        alpha_o_i__o_2 = alpha_o_i__o_2 + (n_o_1_k__o[k]  * log(cosh(theta_o_1_k__o[k] * taw)));
      } 
  }

  double alpha_o_i__o = log (delta)+ R_star / R * (n_o_1_k__o[0] + n_o_1_k__o[1] * taw + n_o_1_k__o[2] * log (taw) + alpha_o_i__o_1 - alpha_o_i__o_2);
  // printf(" the value of alpha_o_i__o  %f (J/Kg) \n ", alpha_o_i__o);
  return alpha_o_i__o;
}

double alpha_o_i__r__function(double rho, double Temp)
{
  double delta = rho/rho_c_1;
  double taw = Temp_c_1/Temp;

  double alpha_o_i__r = 0;
  for (int i = 0; i<Kpol; ++i) { //1--->6  
    alpha_o_i__r = alpha_o_i__r + n_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i]);
    //Message0("alpha_taw__r  first i =  %d   .\n",i);
  }  
  for (int i = Kpol; i<Kpol+Kexp; ++i){ //  7--->24 
    alpha_o_i__r = alpha_o_i__r + n_o_1_k[i]*pow(delta,d_o_1_k[i])*pow(taw,t_o_1_k[i])*exp(-pow(delta,c_o_1_k[i]));
    //Message0("alpha_taw__r  second i =  %d   .\n",i);
  } 
  // printf(" Temp %f\n ", Temp);
  // printf(" the value of alpha_o_i__r %f\n ", alpha_o_i__r);
  return alpha_o_i__r; 
}