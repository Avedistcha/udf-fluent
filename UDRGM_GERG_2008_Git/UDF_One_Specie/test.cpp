
#include<stdio.h>
#include <math.h>
#define PCRIT 4.5992e+06 // (Pa) for Methane
int main() {
    double P=1.5*PCRIT;
    double Temp =370.588235;
    double Visco1[] = {4.03072165890651e-07,-0.00100654210667834,1.02852622284534,-549.534050643264,161608.279291020,-24724435.7361364,1542991970.67037};
    double Vis1 = Visco1[1]*pow(Temp,6)+ Visco1[2]*pow(Temp,5)+ Visco1[3]*pow(Temp,4)+ Visco1[4]*pow(Temp,3)+ Visco1[5]*pow(Temp,2)+ Visco1[6]*Temp+ Visco1[7];
    double Visco2[] = {-3.74936615490581e-05,0.0834882835203674,-73.3263963492992,31725.2351116501,-6735022.08648053,573768341.066175};
    double Vis2 = Visco2[1]*pow(Temp,5)+ Visco2[2]*pow(Temp,4)+ Visco2[3]*pow(Temp,3)+ Visco2[4]*pow(Temp,2)+ Visco2[5]*Temp+ Visco2[6];
    double P1 = 1.0*PCRIT;
    double P2 = 2.5*PCRIT;
    double mu = (P2-P)*(Vis2-Vis1)/(P2-P1);
    printf("Temp %f, Vis1 %f, Vis2 %f, mu %f\n",Temp, Vis1, Vis2, mu) ;
    return 0;
}