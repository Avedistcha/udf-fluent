/* */
/* Calculate viscosity and conductivity of a mixture according to kinetic theory. */
/* References: */
/* - Pitsch, 1993, Entwicklung eines Programmpaketes zur Berechnung eindimensionaler Flammen am Beispiel einer Gegenstromdiffusionsflamme. */
/*   Error in Pitsch: G_ij (4.40) -> exponent of visc_i/visc_j should be +0.5 not -0.5. */
/*   Error in Pitsch: visc_i (4.41) -> const factor should be 2.6693e-6 not 2.6693e-5 for SI units. */
/* */

#include "udf.h"

/* USER INPUT */
/* NSP     = Number of species in mixture.                                                   */
/* NONPREM = Switch between non-premixed combustion model (= 1) and species transport (= 0). */
#define NSP 21
#define NONPREM 0

/* define */
#define NSPDB 21
#define DEBUGMAIN 0
#define DEBUGSUB 0

/* global variables */
int assignFlag = 0;
int prepFlag   = 0;
/* pre assigned (i.e. once) */
real potPar[NSP];
real viscCoeff[NSP];
real condCoeff[NSP];
/* run time prepared (i.e. every iteration) */
real visc_i[NSP];
real delta_i[NSP];
real X[NSP],Y[NSP];

/* function prototypes */
/* main */
real calcVisc(int,real,cell_t,Thread*,Material*,Material*);
real calcCond(int,real,cell_t,Thread*,Material*,Material*);
/* tool */
void assignProps(cell_t,Thread*,Material*);
void prepGlobals(cell_t,Thread*,Material*);
void getXY(real*,real*,cell_t,Thread*,Material*);
real omega_visc(real);
real delta(int,real,real[],cell_t,Thread*,Material*,Material*);

/* general purpose define macros */

/* model specific define macros */
DEFINE_PROPERTY(mixViscIG,cell,thread)
{
	/* loop variables */
	int iSp;
	Material *sp; //Species pointer.                       
	Material *mix = THREAD_MATERIAL(thread); //Material pointer,  thread: Pointer to cell thread.
/* 	THREAD_MATERIAL is defined in threads.h and returns real pointer m to the Material that
	is associated with the given cell thread t. */
	
	/* mixture properties */
	real mixVisc = 0.0;
	
	/* assign properties & prepare global variables */
	if (assignFlag == 0) assignProps(cell,thread,mix);
	prepGlobals(cell,thread,mix);
	
	/* calculate mix viscosity */
	mixture_species_loop(mix,sp,iSp)
	{
		mixVisc += Y[iSp]*visc_i[iSp]/delta_i[iSp];// Y[iSp] is species mass fraction
	}
	
	/*
	Carefull by putting the following to 1 because our Macro is looping 
	over all the cells which means you might get stuck in a long console printing time:D
	*/
	/* debug */
	#if DEBUGMAIN == 1
	Message0("\n*DEBUG: DEFINE_PROPERTY - mixViscIG()\n");
	mixture_species_loop(mix,sp,iSp)
	{
		Message0("*species %10s (No.%3i): X = %f [-], Y = %f [-], viscosity = %e [Pa*s].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,X[iSp],Y[iSp],visc_i[iSp]);
	}
	Message0("*mixture: viscosity = %e [Pa*s].\n",mixVisc);
	Message0("*END DEBUG: mixViscIG()\n");
	#endif
	
	return mixVisc;
}

DEFINE_PROPERTY(mixCondIG,cell,thread)
{
	/* loop variables */
	int iSp;
	Material *sp;
	Material *mix = THREAD_MATERIAL(thread);
	
	/* mixture properties */
	real mixCond = 0.0;
	real cond_i[NSP];
	real T = C_T(cell,thread);
	// Message0("real T = %f [K].\n",T);
	/* assign properties & prepare global variables */
	if (assignFlag == 0) assignProps(cell,thread,mix);
	if (prepFlag   == 0) prepGlobals(cell,thread,mix);
	
	/* calculate mix conductivity */
	mixture_species_loop(mix,sp,iSp)
	{
		cond_i[iSp]  = calcCond(iSp,T,cell,thread,sp,mix);
		mixCond     += Y[iSp]*cond_i[iSp]/delta_i[iSp];
	}
	// Message0("**************************NEXT*******************************\n");
	// Y[iSp] is species mass fraction
	/* debug */
	#if DEBUGMAIN == 1
	Message0("\n*DEBUG: DEFINE_PROPERTY - mixCondIG()\n");
	mixture_species_loop(mix,sp,iSp)
	{
		Message0("*species %10s (No.%3i): X = %f [-], Y = %f [-], conductivity = %f [J/m*s*K].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,X[iSp],Y[iSp],cond_i[iSp]);
	}
	Message0("*mixture: conductivity = %f [J/m*s*K].\n",mixCond);
	Message0("*END DEBUG: mixCondIG()\n");
	#endif
	
	prepFlag = 0;
	
	return mixCond;
}

/* main functions */
real calcVisc(int iSp,real T,cell_t cell,Thread *thread,Material *sp,Material *mix)
{
	/* mixture properties */
	real visc = 0.0;
	real T_red = T/potPar[iSp]; 
/* 	real potPar[NSP] is the full array which contains  the Energy Parameter of each spiecie
	T_red is actually T* in the Kinetic formula (Equation 8–27) in Ansys Manual */
	visc = viscCoeff[iSp]*sqrt(T)/omega_visc(T_red);/* [Pa*s] */
	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: calcVisc()\n");
	Message0("*species viscosity\n");
	Message0("*species %10s (No.%3i): viscosity = %e [Pa*s].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,visc);
	Message0("*END DEBUG: calcVisc()\n");
	#endif
	
	return visc;
}

real calcCond(int iSp,real T,cell_t cell,Thread *thread,Material *sp,Material *mix)
{
	/* mixture properties */
	real cond = 0.0;
	real cp;
	
	cp = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_Cp,T);
	// Message0("real cp = %f [].\n",cp);
	

	cond = visc_i[iSp]*(cp+condCoeff[iSp]);/* [J/m*s*K] */

	// real Mw;
	// real R;
	// Mw = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_mwi,T);
	// R  = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_Rgas,T);
	// Message0("generic_property Mw = %f [].\n",Mw);
	// Message0("generic_property R = %f [].\n",R);	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: calcCond()\n");
	Message0("*species conductivity\n");
	Message0("*species %10s (No.%3i): conductivity = %f [J/m*s*K].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,cond);
	Message0("*END DEBUG: calcCond()\n");
	#endif
	
	return cond;
}

/* tool functions */
/* assign global variables (i.e. once) */
void assignProps(cell_t cell,Thread *thread,Material *mix)
{
	/* loop variables */
	int iSp,jSp;
	int spCount = 0;
	int spCheck[NSP];
	Material *sp;
	
	/* mixture properties */
	real T = 333.33;/* only dummy for Fluent property functions */
	real Mw;
	real R;
	
	/* USER INPUT */
	/* id <-> species                                   0,                  1,                  2,                  3,                  4,                  5,                  6,                  7,                  8,                  9,                 10,                 11,                 12,                 13,                 14,                 15,                 16,                 17,                 18,                 19,                 20*/
	char *speciesDB[]              = {               "o2",                "o",                "h",              "ho2",               "oh",               "h2",             "h2o2",              "h2o",              "co2",               "co",              "hco",             "ch2o",              "ch3",             "ch3o",              "ch4",             "hcco",             "c2h3",             "c2h4",             "c2h5",            "ch3o2",             "c2h6"};
	real potParDB[NSPDB]           = {            107.400,             80.000,            145.000,            107.400,             80.000,             38.000,            107.400,            572.400,            244.000,             98.100,            498.000,            498.000,            144.000,            417.000,            141.400,            150.000,            209.000,            280.800,            252.300,            481.800,            252.300};
	real colDimDB[NSPDB]           = {              3.458,              2.750,              2.050,              3.458,              2.750,              2.920,              3.458,              2.605,              3.763,              3.650,              3.590,              3.590,              3.800,              3.690,              3.746,              2.500,              4.100,              3.971,              4.302,              3.626,              4.302};
	
/* 	These parameters are the Lennard-Jones parameters and are referred to by ANSYS Fluent as the 'characteristic length' and the 'energy parameter' respectively.
	potParDB is the Energy Parameter
	colDimDB is the Characteristic Length (angstrom) */

	/* assign properties */
	Message0("\n+++ Assigning species properties. +++\n");
	for(jSp=0;jSp<NSPDB;jSp++)
	{
		mixture_species_loop(mix,sp,iSp)
		{
			if (strcmp(speciesDB[jSp],MIXTURE_SPECIE_NAME(mix,iSp)) == 0)
			{
				Message0("Species %10s added from data base.\n",speciesDB[jSp]);
				
				Mw = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_mwi,T);
				R  = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_Rgas,T);
				// Message0("generic_property Mw = %f [].\n",Mw);
				// Message0("generic_property R = %f [].\n",R);				
				potPar[iSp]    = potParDB[jSp];
				viscCoeff[iSp] = 2.6693e-6*sqrt(Mw)/pow(colDimDB[jSp],2.0);
				condCoeff[iSp] = 5.0/4.0*R;
				
				spCount++;
				spCheck[iSp] = 1;
			}
		}
	}
	
	if(spCount<NSP)
	{
		Message0("\nERROR!!!: %i species not found in data base.\n",NSP-spCount);
		mixture_species_loop(mix,sp,iSp)
		{
			if(spCheck[iSp] == 0) Message0("          Species %s not found.\n",MIXTURE_SPECIE_NAME(mix,iSp));
		}
	}
	else
	{
		Message0("+++ Assigning species properties done. +++\n");
	}
	
	assignFlag = 1;
}

/* prepare global variables (i.e. every iteration) */
void prepGlobals(cell_t cell,Thread *thread,Material *mix)
{
	/* loop variables */
	int iSp;
	Material *sp;
	
	/* mixture properties */
	real T = C_T(cell,thread);
	
	mixture_species_loop(mix,sp,iSp)
	{
		visc_i[iSp] = calcVisc(iSp,T,cell,thread,sp,mix);
	}
	
	getXY(X,Y,cell,thread,mix);
	
	mixture_species_loop(mix,sp,iSp)
	{
		delta_i[iSp] = delta(iSp,T,Y,cell,thread,sp,mix);
	}
	
	prepFlag = 1;
}

/* get mole and mass fractions */
void getXY(real *X,real *Y,cell_t cell,Thread *thread,Material *mix)
{
	/* loop variables */
	int iSp;
	Material *sp;
	
	/* get mole and mass fractions depending on model */
	#if(NONPREM == 1)
	Pdf_XY(cell,thread,X,Y);
	#else
	mixture_species_loop(mix,sp,iSp)
	{
		Y[iSp] = C_YI(cell,thread,iSp);
	}
	Mole_Fraction(mix,Y,X);
	#endif
	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: getXY()\n");
	Message0("*species mole and mass fractions\n");
	mixture_species_loop(mix,sp,iSp)
	{
		Message0("*species %10s (No.%3i): X = [-], %f Y = %f [-].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,X[iSp],Y[iSp]);
	}
	Message0("*END DEBUG: getXY()\n");
	#endif
}

/* collision integral viscosity */
real omega_visc(real T_red)
{
	real m1 = 3.3530622607;
	real m2 = 2.53272006;
	real m3 = 2.9024238575;
	real m4 = 0.11186138893;
	real m5 = 0.8662326188;
	real m6 = 1.3913958626;
	real m7 = 3.158490576;
	real m8 = 0.18973411754;
	real m9 = 0.00018682962894;
	
	real num,den;
	
	num = m1 + T_red*(m2 + T_red*(m3 + T_red*m4));
	den = m5 + T_red*(m6 + T_red*(m7 + T_red*(m8 + T_red*m9)));
	
	return num/den;
}

/* delta */
real delta(int iSp,real T,real Y[],cell_t cell,Thread* thread,Material *sp_i,Material *mix)
{
	/* loop variables */
	int jSp;
	Material *sp_j;
	
	real delta = 0.0;
	real Mw_i,Mw_j;
	real G_ij[NSP];
	
	Mw_i = generic_property(cell,thread,MATERIAL_PROPERTY(sp_i),PROP_mwi,T);
	
	mixture_species_loop(mix,sp_j,jSp)
	{
		Mw_j = generic_property(cell,thread,MATERIAL_PROPERTY(sp_j),PROP_mwi,T);
		
		G_ij[jSp]  = 1.0/sqrt(8.0) * pow(1.0+Mw_i/Mw_j,-0.5) * pow(1.0+pow(visc_i[iSp]/visc_i[jSp],0.5)*pow(Mw_j/Mw_i,0.25),2.0);
		delta     += G_ij[jSp]*Mw_i/Mw_j*Y[jSp];
	}
	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: delta()\n");
	Message0("*species %10s (No.%3i)\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp);
	mixture_species_loop(mix,sp_j,jSp)
	{
		Message0("*G_ij  = %f, i = %i, j = %i.\n",G_ij[jSp],iSp,jSp);
	}
	Message0("*delta = %f.\n",delta);
	Message0("*END DEBUG: delta()\n");
	#endif
	
	return delta;
}