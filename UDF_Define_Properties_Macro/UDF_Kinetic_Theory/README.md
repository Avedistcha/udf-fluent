# UDF-kinetic theory
In Fluent, you may choose to define the following properties using kinetic theory when the ideal gas law is enabled

- viscosity (for fluids).\

- thermal conductivity (for fluids).\

- specific heat capacity (for fluids).\

- mass diffusion coefficients (for multi-species mixtures).\

In the following UDF we intend to calculate only viscosity and Thermal conductivity of a mixture according to kinetic theory.

## Importent note

DEBUGMAIN and DEBUGSUB flag should be treated carfully because the messag0 will be printet at each cell iteration..\
If you mesh has a high resolution it will jam your Fluent Terminal.\
When Importing a Chemkin data base make sure the number and the spicies matches with the code spiecies! .\
Last but most importent NONPREM Switch between non-premixed combustion model (= 1) and species transport (= 0), it should be set same as you set it in Fluent!.\


When loading the UDF please make sure to do it before loading the Chemkin-import!.\
Another way around to solve this is to delete the Chemkin-import then to load your udf then reload the Chemkin-import.\

Do not ever try to use this rare bracket  “ ”  in you commented line, this can happen when compying a text from an external source...
Again this "" or '' are allowded but not  this one “ ” . 

For example:
//  “Hellow World” 
The above will create a build in compile error! 

### UDRGM Vs Fluent version comparision

#### Kinetic Theory for the Thermal Conductivity and Viscosity Physical Properties
The following post processed data shows the difference between Fluent Kinetic Theory and UDF Kinectic Theory for Thermal Conductivity and Viscosity.\
As seen the difference is not above 1.5% for a channel flow involving a hight operating pressure and a hight heated wall Temperature gradient.\
You final post proccessd result difference might differe from a case to another but nevertheless the difference resulted should be small.
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Kinetic_Therm_Cond.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Kinetic_Visc.png" width="300" height="300">
