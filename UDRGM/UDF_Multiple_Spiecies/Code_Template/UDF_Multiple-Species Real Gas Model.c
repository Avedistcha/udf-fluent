
#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

#if RP_DOUBLE
#define SMALL 1.e-20
#else
#define SMALL 1.e-10
#endif

#define NCMAX 20
#define NSPECIE_NAME 80

static int (*usersMessage)(const char *,...);
static void (*usersError)(const char *,...);

static double ref_p, ref_T;
static char  gas[NCMAX][NSPECIE_NAME];
static int  n_specs;

double Mixture_Rgas(double yi[]);
double Mixture_pressure(double Temp, double Rho, double yi[]);
/* yi is Species mass fraction yi[]: ANSYS Fluent solver returns a value of 1.0 for yi[] in single-species flows. For multiple-
species flows, yi[] is a vector array containing species mass fraction in an order defined
by the user setup function
 */
double Mw_i(int i);
double Cp_i(double T, double r, int i);
double K_i(double T, double r, int i);
double Mu_i(double T, double r, int i);
double Rgas_i(double T, double r, int i);
double Gm_i(double T, double r, int i);

DEFINE_ON_DEMAND(I_do_nothing)
{
 /*
  This is a dummy function
  must be included to allow for the use of the
  ANSYS FLUENT UDF compilation utility
 */
}

/*******************************************************************/
/* Mixture Functions
/* These are the only functions called from ANSYS FLUENT Code      */
/*******************************************************************/
void MIXTURE_Setup(Domain *domain, cxboolean vapor_phase, char *specielist,
          int (*messagefunc)(const char *format,...),
          void (*errorfunc)(const char *format,...))
{
 /* This function will be called from ANSYS FLUENT after the
   UDF library has been loaded.
   User must enter the number of species in the mixture
   and the name of the individual species.
 */

 int i;
 usersMessage = messagefunc;
 usersError  = errorfunc;
 ref_p    = ABS_P(RP_Get_Real("reference-pressure"),op_pres);
 ref_T    = RP_Get_Real("reference-temperature");

 if (ref_p == 0.0)
  {
   Message0("\n MIXTURE_Setup: reference-pressure was not set by user \n");
   Message0("\n MIXTURE_Setup: setting reference-pressure to 101325 Pa \n");
   ref_p = 101325.0;
  }
 /*====================================================*/
 /*=========  User Input Section ======================*/
 /*====================================================*/
 /*
  Define Number of species & Species name.
  DO NOT use space for naming species
 */
 n_specs = 4;

 (void)strcpy(gas[0],"H2O");
 (void)strcpy(gas[1],"N2") ;
 (void)strcpy(gas[2],"O2") ;
 (void)strcpy(gas[3],"CO2");

 /*====================================================*/
 /*=========  End Of User Input Section ===============*/
 /*====================================================*/

 Message0("\n MIXTURE_Setup: RealGas mixture initialization \n");
 Message0("\n MIXTURE_Setup: Number of Species = %d \n",n_specs);
 for (i=0; i<n_specs; ++i)
  {
   Message0("\n MIXTURE_Setup: Specie[%d]     = %s \n",i,gas[i]);
  }

 /*
  concatenate species name into one string
  and send back to fluent
 */
 strcat(specielist,gas[0]);
 for (i=1; i<n_specs; ++i)
  {
     strcat(specielist," ");
     strcat(specielist,gas[i]);
  }
}

double MIXTURE_density(cell_t cell, Thread *thread, cxboolean vapor_phase, double Temp, double P, double yi[])
{
 double Rgas = Mixture_Rgas(yi);

 double r  = P/(Rgas*Temp); /* Density at Temp & P */

 return r;   /* (Kg/m^3) */
}

double MIXTURE_specific_heat(cell_t cell, Thread *thread, double Temp, double density, double P,
        double yi[])
{
 double cp=0.0;
 int i;
 for (i=0; i<n_specs; ++i)
    cp += yi[i]*Cp_i(Temp,density,i);

 return cp;   /* (J/Kg/K) */
}

double MIXTURE_enthalpy(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double h=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    h += yi[i]*(Temp*Cp_i(Temp,density,i));

 return h;   /* (J/Kg) */
}

double MIXTURE_entropy(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double s  = 0.0 ;
 double Rgas=0.0;

 Rgas = Mixture_Rgas(yi);

 s  = MIXTURE_specific_heat(cell, thread, Temp,density,P,yi)*log(Temp/ref_T) -
     Rgas*log(P/ref_p);

 return s;   /* (J/Kg/K) */
}

double MIXTURE_mw(double yi[])
{
 double MW, sum=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    sum += (yi[i]/Mw_i(i));

 MW = 1.0/MAX(sum,SMALL)   ;

 return MW;   /* (Kg/Kmol) */
}

double MIXTURE_speed_of_sound(cell_t cell, Thread *thread, double Temp, double density, double P,
               double yi[])
{
 double a, cp, Rgas;

 cp  = MIXTURE_specific_heat(cell, thread, Temp,density,P,yi);
 Rgas = Mixture_Rgas(yi);

 a  = sqrt(Rgas*Temp* cp/(cp-Rgas));

 return a; /* m/s */
}

double MIXTURE_viscosity(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double mu=0;
 int i;

 for (i=0; i<n_specs; ++i)
    mu += yi[i]*Mu_i(Temp,density,i);

 return mu;   /* (Kg/m/s) */
}

double MIXTURE_thermal_conductivity(cell_t cell, Thread *thread, double Temp, double density, double P,
                  double yi[])
{
     double kt=0;
 int i;

 for (i=0; i<n_specs; ++i)
    kt += yi[i]*K_i(Temp,density,i);

 return kt;   /* W/m/K */
}

double MIXTURE_rho_t(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{

 double drdT ; /* derivative of rho w.r.t. Temp */
 double p   ;
 double dT=0.01;

 p   = Mixture_pressure(Temp,density, yi);
 drdT = (MIXTURE_density(cell, thread, TRUE,Temp+dT,p,yi) - MIXTURE_density(cell, thread, TRUE,Temp,p,yi)) /dT;

 return drdT;   /* (Kg/m^3/K) */
}

double MIXTURE_rho_p(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double drdp  ;
 double p   ;
 double dp= 5.0;

 p   = Mixture_pressure(Temp,density, yi);
 drdp = (MIXTURE_density(cell, thread, TRUE,Temp,p+dp,yi) - MIXTURE_density(cell, thread, TRUE,Temp,p,yi)) /dp;

 return drdp;   /* (Kg/m^3/Pa) */
}

double MIXTURE_enthalpy_t(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double dhdT  ;
 double p    ;
 double rho2  ;
 double dT= 0.01;

 p   = Mixture_pressure(Temp,density, yi);
 rho2 = MIXTURE_density(cell, thread, TRUE,Temp+dT,p,yi)  ;

 dhdT = (MIXTURE_enthalpy(cell, thread, Temp+dT,rho2,P,yi) - MIXTURE_enthalpy(cell, thread, Temp,
      density,P,yi)) /dT;

 return dhdT; /* J/(Kg.K) */
}

double MIXTURE_enthalpy_p(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double dhdp  ;
 double p    ;
 double rho2  ;
 double dp= 5.0 ;

 p   = Mixture_pressure(Temp,density, yi);
 rho2 = MIXTURE_density(cell, thread, TRUE,Temp,p+dp,yi)  ;

 dhdp = (MIXTURE_enthalpy(cell, thread, Temp,rho2,P,yi) - MIXTURE_enthalpy(cell, thread, Temp,density,
      P,yi)) /dp;

 return dhdp; /* J/ (Kg.Pascal) */
}

/*******************************************************************/
/* Auxiliary Mixture Functions                                     */

/*******************************************************************/

double Mixture_Rgas(double yi[])
{
 double Rgas=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    Rgas += yi[i]*(UNIVERSAL_GAS_CONSTANT/Mw_i(i));

 return Rgas;
}

double Mixture_pressure(double Temp, double Rho, double yi[])
{
 double Rgas = Mixture_Rgas(yi);

 double P  = Rho*Rgas*Temp  ; /* Pressure at Temp & P */

 return P;   /* (Kg/m^3) */
}

/*******************************************************************/
/* Species Property Functions                                      */
/*******************************************************************/
double Mw_i(int i)
{
 double mi[20];

 mi[0] = 18.01534; /*H2O*/
 mi[1] = 28.01340; /*N2 */
 mi[2] = 31.99880; /*O2 */
 mi[3] = 44.00995; /*CO2*/

 return mi[i];
}

double Cp_i(double T, double r, int i)
{
 double cpi[20];

 cpi[0] = 2014.00; /*H2O*/
 cpi[1] = 1040.67; /*N2 */
 cpi[2] = 919.31; /*O2 */
 cpi[3] = 840.37; /*CO2*/

 return cpi[i];
}

double K_i(double T, double r, int i)
{
 double ki[20];

 ki[0] = 0.02610; /*H2O*/
 ki[1] = 0.02420; /*N2 */
 ki[2] = 0.02460; /*O2 */
 ki[3] = 0.01450; /*CO2*/

 return ki[i];
}

double Mu_i(double T, double r, int i)
{
 double mui[20];

 mui[0] = 1.340E-05; /*H2O*/
 mui[1] = 1.663E-05; /*N2 */
 mui[2] = 1.919E-05; /*O2 */
 mui[3] = 1.370E-05; /*CO2*/
 return mui[i];
}

double Rgas_i(double T, double r, int i)
{
 double Rgasi;

 Rgasi = UNIVERSAL_GAS_CONSTANT/Mw_i(i);

 return Rgasi;
}

double Gm_i(double T, double r, int i)
{
 double gammai;

 gammai = Cp_i(T,r,i)/(Cp_i(T,r,i) - Rgas_i(T,r,i));

 return gammai;

}

/*******************************************************************/
/* Mixture Functions Structure                                     */
/*******************************************************************/
UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
   MIXTURE_Setup,                    /* initialize               */
   MIXTURE_density,                  /* density                  */ 
   MIXTURE_enthalpy,                 /* enthalpy                 */
   MIXTURE_entropy,                  /* entropy                  */
   MIXTURE_specific_heat,            /* specific_heat            */
   MIXTURE_mw,                       /* molecular_weight         */
   MIXTURE_speed_of_sound,           /* speed_of_sound           */
   MIXTURE_viscosity,                /* viscosity                */
   MIXTURE_thermal_conductivity,     /* thermal_conductivity     */
   MIXTURE_rho_t,                    /* drho/dT |const p         */
   MIXTURE_rho_p,                    /* drho/dp |const T         */
   MIXTURE_enthalpy_t,               /* dh/dT |const p           */
   MIXTURE_enthalpy_p                /* dh/dp |const T           */
};

/*
******************************************************************/







