# UDRGM for One Specie (one Material) 

UDRGM for Multiple sample Fluid material will be elaborated here.\
The following code can be used but it serves as a **Template**.\
It should be noted same Physical Properties of the UDRG should be used in Fluent UI in order to receive similar results.\
Once similarity in the output is produced and you feel confortable with the template code you may start to costumize your own  physical properties in the UDRGM. :rocket:

## User Defined Physical properties Used for this sample code

| Properties    | Method or Value |
| ------------- | ------------- |
| **Material:**  |  H2O N2 O2 CO2 |
| **Equation of State:**  |Ideal Gas |
| **speed_of_sound:**  | Ideal Gas SoS |

**MWML:** mass-weighted-mixing-law.\
IGML:** ideal-gas-mixing-law

| Spiecie        | Mollecular Weight (Kg/Kmol)| Cp (J/Kg/K)| Thermal Conductivity(W/m/K) |Viscosity(Kg/m/s)| enthalpy (J/Kg)| entropy(J/Kg/K)| R_gas          |
| -------------  | -------------              |   ---------|   -------------             |   ------------- |  ------------- |  ------------- |  ------------- | 
| **Cp H2O:**    | 18.01534                   | 2014.00    | 0.02610                     | 1.340E-05       |  ------------- |  ------------- |  ------------- |
| **Cp N2:**     | 28.01340                   | 1040.67    | 0.02420                     | 1.663E-05       |  ------------- |  ------------- |  ------------- |
| **Cp O2:**     | 31.99880                   | 919.31     | 0.02460                     | 1.919E-05       |  ------------- |  ------------- |  ------------- |
| **Cp CO2:**    | 44.00995                   | 840.37     | 0.01450                     | 1.370E-05       |  ------------- |  ------------- |  ------------- |
| **MIXTURE:**   | MWML                       | MWML       | IGML                        | IGML            |  MWML          |  MWML          |  MWML          |

| Properties                  | Model/Law 1          | 
| -------------               | -------------        | 
| **Thermal Conductivity**    | Kinetic Theory       | 
| **Viscosity:**              | Kinetic Theory       | 
| **Density:**                | Ideal Gas            | 

## Returned Physical properties to Fluent
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow. (check the code Template)

### Loading the code using TUI
```
define/user-defined/real-gas-models/user-defined-multispecies-real-gas-model
```



### UDRGM Vs Fluent version comparision

#### Kinetic Theory for the Thermal Conductivity and Viscosity Physical Properties
The following post processed data shows the difference between Fluent Kinetic Theory and UDF Kinectic Theory for Thermal Conductivity and Viscosity.\
As seen the difference is not above 1.5% for a channel flow involving a hight operating pressure and a hight heated wall Temperature gradient.\
You final post proccessd result difference might differe from a case to another but nevertheless the difference resulted should be small.
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Kinetic_Therm_Cond.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Kinetic_Visc.png" width="300" height="300">
