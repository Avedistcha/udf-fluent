# UDRGM for Multiple Specie 

UDRGM for Multiple sample Fluid material will be elaborated here.\
The following code can be used but it can also serves as a **Template**.\
It should be noted same Physical Properties of the UDRG should be used in Fluent UI in order to receive similar results. In other words te replicate the Fluent results.\
Once similarity in the output is produced and you feel confortable with the template code you may start to costumize your own  physical properties in the UDRGM. :rocket:

## User Defined Physical properties Used for our sample code

**MWML:** mass-weighted-mixing-law.\
**IGML:** ideal-gas-mixing-law

| Spiecie_Mixture| Mollecular Weight (Kg/Kmol)| Cp (J/Kg/K)| Thermal Conductivity(W/m/K) |Viscosity(Kg/m/s)| enthalpy (J/Kg)| entropy(J/Kg/K)| R_gas          | Density       |
| -------------  | -------------              |   ---------|   -------------             |   ------------- |  ------------- |  ------------- |  ------------- | ------------- |  
|**MIXTURE Law:**| MWML                       | MWML       | IGML                        | IGML            |  MWML          |  MWML          |  MWML          | MWML          |


| Spiecies_Properties         | Model/Law 1                    | 
| -------------               | -------------                  | 
| **Thermal Conductivity**    | Kinetic Theory                 | 
| **Viscosity:**              | Kinetic Theory                 | 
| **Density:**                | Peng And Robinson              | 
| **CP:**                     | piecewise-polynomial using Thermdat_DLR Coeficient|
| **speed_of_sound:**         | Ideal Gas SoS                  |
| **Involved Spiecies:**      |  o2 o h ho2 oh h2 h2o2 h2o co2 co hco ch2o ch3 ch3o ch4 hcco c2h3 c2h4 c2h5 ch3o2 c2h6 n2|

## Returned Physical properties to Fluent
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow. (check the code Template)

### Loading the code using TUI
```
define/user-defined/real-gas-models/user-defined-multispecies-real-gas-model
```
### UDRGM Vs Fluent version comparision

#### Kinetic Theory for the Thermal Conductivity and Viscosity Physical Properties
The following post processed data shows the difference between Fluent Kinetic Theory and UDF Kinectic Theory for Thermal Conductivity and Viscosity.\
As seen the difference is not above 1.5% for a channel flow involving a hight operating pressure and a hight heated wall Temperature gradient.\
You final post proccessd result difference might differe from a case to another but nevertheless the difference resulted should be small.
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Kinetic_Therm_Cond.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Kinetic_Visc.png" width="300" height="300">

#### Peng and Robinson Equation of State comparision for only CH4

The following post processed data shows the difference between Fluent Peng and Robinson and the written UDRG Peng and Robinson.\
As seen the difference is not above 0.34% for a channel flow involving a hight operating pressure and a hight heated wall Temperature gradient.\
You final post proccessd result difference might differe from a case to another but nevertheless the difference resulted should be small.

<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Peng_Robinson_Methane_Density.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Peng_Robinson_Methane_Pressure.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Peng_Robinson_Methane_Temperature.png" width="300" height="300">


Macros used in CFD-Post to generate those differences.\
abs(100*(Density.Difference)/Density).\
abs(100*(Absolute Pressure.Difference)/(Absolute Pressure)).\
abs(100*(Temperature.Difference)/(Temperature)).
