#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"


#if RP_DOUBLE
#define SMALL 1.e-20
#else
#define SMALL 1.e-10
#endif

#define NCMAX 22
#define NSPECIE_NAME 80

//******************************************************************************************************Start Knetic Theory part ************************************************************
/* USER INPUT */
/* NSP     = Number of species in mixture.                                                   */
/* NONPREM = Switch between non-premixed combustion model (= 1) and species transport (= 0). */
#define NSP 22
#define NONPREM 0
#define RGASU UNIVERSAL_GAS_CONSTANT /* 8314.34 SI units: J/Kmol/K */
/* define */
#define NSPDB 22
#define DEBUGMAIN 0
#define DEBUGSUB 0
#define n_specs 22

/* global variables */
int assignFlag = 0;
int prepFlag   = 0;
/* pre assigned (i.e. once) */
real potPar[NSP];
real viscCoeff[NSP];
real condCoeff[NSP];
/* run time prepared (i.e. every iteration) */
real visc_i[NSP];
real delta_i[NSP];
real X[NSP],Y[NSP];

/* function prototypes */
/* main */
real calcVisc(int,real,cell_t,Thread*,Material*,Material*);
real calcCond(int,real,cell_t,Thread*,Material*,Material*);
/* tool */
void assignProps(cell_t,Thread*,Material*);
void prepGlobals(cell_t,Thread*,Material*);
void getXY(real*,real*,cell_t,Thread*,Material*);
real omega_visc(real);
real delta(int,real,real[],cell_t,Thread*,Material*,Material*);
//******************************************************************************************************End Knetic Theory part ************************************************************

static int (*usersMessage)(const char *,...);
static void (*usersError)(const char *,...);

static double ref_p, ref_T;
static char  gas[NCMAX][NSPECIE_NAME];

double Mixture_Rgas(double yi[]);
double Mixture_pressure(double Temp, double Rho, double yi[]);
/* yi is Species mass fraction yi[]: ANSYS Fluent solver returns a value of 1.0 for yi[] in single-species flows. For multiple-
species flows, yi[] is a vector array containing species mass fraction in an order defined
by the user setup function
 */
double Cp_i(double T, double r, int i);
double K_i(double T, double r, int i);
double Mu_i(double T, double r, int i);
double Rgas_i(double T, double r, int i);
double Gm_i(double T, double r, int i);


//*********************************************************Peng-Robinson **************************************************************//
/* Static variables associated with Peng-Robinson */
double CC_i(int c, int e, int i); //Thermdat Cp Coeficient
double RKEOS_spvol(double temp, double press, int i);
static double rgas[n_specs], a0[n_specs], b0[n_specs];
static double tcrit[n_specs]; /* critical temperature */
static double pcrit[n_specs]; /* critical pressure */
static double wi[n_specs]; /* Acentric factor of a Spiecie */
static double ch_len_i[n_specs];/* Charachteristic Length (angstrom)*/
static double en_p_i[n_specs];/* the Energy Parameter of a Spiecie (k) */
static double mi[n_specs];/* Molecular Weight (kg/m.s) */
void Tcrit();
void Pcrit();
void Wfac();
void CharLength();
void En();
void Mw_i();
//*********************************************************Peng-Robinson **************************************************************//

 /*
  This is a dummy function
  must be included to allow for the use of the
  ANSYS FLUENT UDF compilation utility
 */

/*******************************************************************/
/* Mixture Functions                                               */
/* These are the only functions called from ANSYS FLUENT Code      */
/*******************************************************************/
void MIXTURE_Setup(Domain *domain, cxboolean vapor_phase, char *specielist,
          int (*messagefunc)(const char *format,...),
          void (*errorfunc)(const char *format,...))
{
 /* This function will be called from ANSYS FLUENT after the
   UDF library has been loaded.
   User must enter the number of species in the mixture
   and the name of the individual species.
 */

 int i;
 usersMessage = messagefunc;
 usersError  = errorfunc;
 ref_p    = ABS_P(RP_Get_Real("reference-pressure"),op_pres);
 ref_T    = RP_Get_Real("reference-temperature");

 if (ref_p == 0.0)
  {
   Message0("\n MIXTURE_Setup: reference-pressure was not set by user \n");
   Message0("\n MIXTURE_Setup: setting reference-pressure to 101325 Pa \n");
   ref_p = 101325.0;
  }
 /*====================================================*/
 /*=========  User Input Section ======================*/
 /*====================================================*/
 /*
  Define Number of species & Species name.
  DO NOT use space for naming species
 */

 (void)strcpy(gas[0],"o2");
 (void)strcpy(gas[1],"o") ;
 (void)strcpy(gas[2],"h") ;
 (void)strcpy(gas[3],"ho2");
 (void)strcpy(gas[4],"oh") ;
 (void)strcpy(gas[5],"h2");
 (void)strcpy(gas[6],"h2o2");
 (void)strcpy(gas[7],"h2o");
 (void)strcpy(gas[8],"co2");
 (void)strcpy(gas[9],"co");
 (void)strcpy(gas[10],"hco");
 (void)strcpy(gas[11],"ch2o");
 (void)strcpy(gas[12],"ch3");
 (void)strcpy(gas[13],"ch3o");
 (void)strcpy(gas[14],"ch4");
 (void)strcpy(gas[15],"hcco");
 (void)strcpy(gas[16],"c2h3");
 (void)strcpy(gas[17],"c2h4");
 (void)strcpy(gas[18],"c2h5");
 (void)strcpy(gas[19],"ch3o2");
 (void)strcpy(gas[20],"c2h6");
 (void)strcpy(gas[21],"n2");  
 /*====================================================*/
 /*=========  End Of User Input Section ===============*/
 /*====================================================*/

 Message0("\n MIXTURE_Setup: RealGas mixture initialization \n");
 Message0("\n MIXTURE_Setup: Number of Species = %d \n",n_specs);
 for (i=0; i<n_specs; ++i)
  {
   Message0("\n MIXTURE_Setup: Specie[%d]     = %s \n",i,gas[i]);
  }

 /*
  concatenate species name into one string
  and send back to fluent
 */
 strcat(specielist,gas[0]);
 for (i=1; i<n_specs; ++i)
  {
     strcat(specielist," ");
     strcat(specielist,gas[i]);
  }



//*********************************************************Peng-Robinson **************************************************************//
/* initialize */
 Tcrit();
 Pcrit();
 Wfac();
 Mw_i();
 for (i=0; i<n_specs; ++i)
 {		
	 rgas[i] = RGASU/mi[i]; //Constant
	 a0[i] = 0.457247*rgas[i]*rgas[i]*tcrit[i]*tcrit[i]/pcrit[i]; //Constant
	 b0[i] = 0.0778*rgas[i]*tcrit[i]/pcrit[i]; //Constant
 }
 usersMessage("\nLoading Peng-Robinson Library: %s\n", specielist);
//*********************************************************Peng-Robinson **************************************************************//
}

double MIXTURE_density(cell_t cell, Thread *thread, cxboolean vapor_phase, double Temp, double P, double yi[])
{
 double den=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
  {
     if (yi[i]> SMALL)
        den += yi[i]*RKEOS_spvol(Temp, P, i); //Mass Weighted mixing law 
  }

 return 1./den; /* (Kg/m^3) */
}


double MIXTURE_specific_heat(cell_t cell, Thread *thread, double Temp, double density, double P,
        double yi[])
{
 double cp=0.0;
 int i;
 for (i=0; i<n_specs; ++i)
    cp += yi[i]*Cp_i(Temp,density,i);//mass weighted_mixing-law

 return cp;   /* (J/Kg/K) */
}

double MIXTURE_enthalpy(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double h=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    h += yi[i]*(Temp*Cp_i(Temp,density,i));//mixing-law

 return h;   /* (J/Kg) */
}

double MIXTURE_entropy(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double s  = 0.0 ;
 double Rgas=0.0;

 Rgas = Mixture_Rgas(yi);

 s  = MIXTURE_specific_heat(cell, thread, Temp,density,P,yi)*log(Temp/ref_T) -
     Rgas*log(P/ref_p);

 return s;   /* (J/Kg/K) */
}

double MIXTURE_mw(double yi[])
{
 double MW, sum=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    sum += (yi[i]/mi[i]);

 MW = 1.0/MAX(sum,SMALL)   ;//Value Operations - Returns the maximum of two values (it should be so we dont get a archi small return if it happened)

 return MW;   /* (Kg/Kmol) */
}

double MIXTURE_speed_of_sound(cell_t cell, Thread *thread, double Temp, double density, double P,
               double yi[])
{
 double a, cp, Rgas;

 cp  = MIXTURE_specific_heat(cell, thread, Temp,density,P,yi);
 Rgas = Mixture_Rgas(yi);

 a  = sqrt(Rgas*Temp* cp/(cp-Rgas));

 return a; /* m/s */
}

double MIXTURE_viscosity(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 
 //******************************************************************************************************Start Knetic Theory part init ************************************************************
	/* loop variables */
	int iSp;
	Material *sp; //Species pointer.                       
	Material *mix = THREAD_MATERIAL(thread); //Material pointer,  thread: Pointer to cell thread.
/* THREAD_MATERIAL is defined in threads.h and returns real pointer m to the Material that
	is associated with the given cell thread t. */

	//Message0("\n Material *mix = %d \n",mix);

	/* mixture properties */
	real mixVisc = 0.0;
	
	/* assign properties & prepare global variables */
	if (assignFlag == 0) assignProps(cell,thread,mix);
	prepGlobals(cell,thread,mix);
	
	/* calculate mix viscosity */
	mixture_species_loop(mix,sp,iSp)
	{
		mixVisc += Y[iSp]*visc_i[iSp]/delta_i[iSp];// Y[iSp] is species mass fraction
	}
//******************************************************************************************************End Knetic Theory part ************************************************************

 return mixVisc;   /* (Kg/m/s) */

}

















double MIXTURE_thermal_conductivity(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
//******************************************************************************************************Start Knetic Theory part init ************************************************************	
	/* loop variables */
	int iSp;
	Material *sp;
	Material *mix = THREAD_MATERIAL(thread);
	
	/* mixture properties */
	real mixCond = 0.0;
	real cond_i[NSP];
	real T = C_T(cell,thread);
	/* assign properties & prepare global variables */
	if (assignFlag == 0) assignProps(cell,thread,mix);
	if (prepFlag   == 0) prepGlobals(cell,thread,mix);
	
	// Message0("real T = %f [K].\n",T);
	/* calculate mix conductivity */
	// mixture_species_loop is defined in materials.h and loops over all of the species for the given mixture material.

	mixture_species_loop(mix,sp,iSp)
	{
		cond_i[iSp]  = calcCond(iSp,T,cell,thread,sp,mix);
		mixCond     += Y[iSp]*cond_i[iSp]/delta_i[iSp];
	}
	// Message0("**************************NEXT*******************************\n");
	// Y[iSp] is species mass fraction
	/* debug */
	#if DEBUGMAIN == 1
	Message0("\n*DEBUG: DEFINE_PROPERTY - mixCondIG()\n");
	mixture_species_loop(mix,sp,iSp)
	{
		Message0("*species %10s (No.%3i): X = %f [-], Y = %f [-], conductivity = %f [J/m*s*K].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,X[iSp],Y[iSp],cond_i[iSp]);
	}
	Message0("*mixture: conductivity = %f [J/m*s*K].\n",mixCond);
	Message0("*END DEBUG: mixCondIG()\n");
	#endif
	
	prepFlag = 0;
	
	return mixCond;
	// return 0.0454;

//******************************************************************************************************End Knetic Theory part ************************************************************
//  double kt=0;
//  int i;
//  for (i=0; i<n_specs; ++i)
//     kt += yi[i]*K_i(Temp,density,i);
//  kt=0.0454; //dummy but same as Fluent
//  return kt;   /* W/m/K */
}

double MIXTURE_rho_t(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{

 double drdT ; /* derivative of rho w.r.t. Temp */
 double p   ;
 double dT=0.01;

 p   = Mixture_pressure(Temp,density, yi);
 drdT = (MIXTURE_density(cell, thread, TRUE,Temp+dT,p,yi) - MIXTURE_density(cell, thread, TRUE,Temp,p,yi)) /dT;

 return drdT;   /* (Kg/m^3/K) */
}

double MIXTURE_rho_p(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double drdp  ;
 double p   ;
 double dp= 5.0;

 p   = Mixture_pressure(Temp,density, yi);
 drdp = (MIXTURE_density(cell, thread, TRUE,Temp,p+dp,yi) - MIXTURE_density(cell, thread, TRUE,Temp,p,yi)) /dp;

 return drdp;   /* (Kg/m^3/Pa) */
}

double MIXTURE_enthalpy_t(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double dhdT  ;
 double p    ;
 double rho2  ;
 double dT= 0.01;

 p   = Mixture_pressure(Temp,density, yi);
 rho2 = MIXTURE_density(cell, thread, TRUE,Temp+dT,p,yi)  ;

 dhdT = (MIXTURE_enthalpy(cell, thread, Temp+dT,rho2,P,yi) - MIXTURE_enthalpy(cell, thread, Temp,
      density,P,yi)) /dT;

 return dhdT; /* J/(Kg.K) */
}

double MIXTURE_enthalpy_p(cell_t cell, Thread *thread, double Temp, double density, double P, double yi[])
{
 double dhdp  ;
 double p    ;
 double rho2  ;
 double dp= 5.0 ;

 p   = Mixture_pressure(Temp,density, yi);
 rho2 = MIXTURE_density(cell, thread, TRUE,Temp,p+dp,yi)  ;

 dhdp = (MIXTURE_enthalpy(cell, thread, Temp,rho2,P,yi) - MIXTURE_enthalpy(cell, thread, Temp,density,
      P,yi)) /dp;

 return dhdp; /* J/ (Kg.Pascal) */
}

/*******************************************************************/
/* Auxiliary Mixture Functions                                     */

/*******************************************************************/

double Mixture_Rgas(double yi[])
{
 double Rgas=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    Rgas += yi[i]*(UNIVERSAL_GAS_CONSTANT/mi[i]);

 return Rgas;
}

double Mixture_pressure(double Temp, double Rho, double yi[])
{
 double Rgas = Mixture_Rgas(yi);

 double P  = Rho*Rgas*Temp  ; /* Pressure at Temp & P */

 return P;   /* (Kg/m^3) */
}



// double K_i(double T, double r, int i)
// {
//  double ki[20];

//  ki[0] = 0.02610; /*H2O*/
//  ki[1] = 0.02420; /*N2 */
//  ki[2] = 0.02460; /*O2 */
//  ki[3] = 0.01450; /*CO2*/

//  return ki[i];
// }

// double Mu_i(double T, double r, int i)
// {
//  double mui[20];

//  mui[0] = 1.340E-05; /*H2O*/
//  mui[1] = 1.663E-05; /*N2 */
//  mui[2] = 1.919E-05; /*O2 */
//  mui[3] = 1.370E-05; /*CO2*/
//  return mui[i];
// }

// double Rgas_i(double T, double r, int i)
// {
//  double Rgasi;

//  Rgasi = UNIVERSAL_GAS_CONSTANT/mi[i];

//  return Rgasi;
// }

// double Gm_i(double T, double r, int i)
// {
//  double gammai;

//  gammai = Cp_i(T,r,i)/(Cp_i(T,r,i) - Rgas_i(T,r,i));

//  return gammai;

// }

/*******************************************************************/
/* Mixture Functions Structure                                     */
/*******************************************************************/
UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
   MIXTURE_Setup,                    /* initialize               */
   MIXTURE_density,                  /* density                  */ 
   MIXTURE_enthalpy,                 /* enthalpy                 */
   MIXTURE_entropy,                  /* entropy                  */
   MIXTURE_specific_heat,            /* specific_heat            */
   MIXTURE_mw,                       /* molecular_weight         */
   MIXTURE_speed_of_sound,           /* speed_of_sound           */
   MIXTURE_viscosity,                /* viscosity                */
   MIXTURE_thermal_conductivity,     /* thermal_conductivity     */
   MIXTURE_rho_t,                    /* drho/dT |const p         */
   MIXTURE_rho_p,                    /* drho/dp |const T         */
   MIXTURE_enthalpy_t,               /* dh/dT |const p           */
   MIXTURE_enthalpy_p                /* dh/dp |const T           */
};

/*
******************************************************************/







//#******************************************************************************************************Start Knetic Theory part init ************************************************************

/* tool functions */
/* assign global variables (i.e. once) */
void assignProps(cell_t cell,Thread *thread,Material *mix)
{
	/* loop variables */
	int iSp,jSp;
	int spCount = 0;
	int spCheck[NSP];
	Material *sp;
	
	/* mixture properties */
	real T = 333.33;/* only dummy for Fluent property functions */
	real Mw;
	real R;
	
	/* USER INPUT */
	/* id <-> species                                   0,                  1,                  2,                  3,                  4,                  5,                  6,                  7,                  8,                  9,                 10,                 11,                 12,                 13,                 14,                 15,                 16,                 17,                 18,                 19,                 20,       21*/                         
	char *speciesDB[]              = {               "o2",                "o",                "h",              "ho2",               "oh",               "h2",             "h2o2",              "h2o",              "co2",               "co",              "hco",             "ch2o",              "ch3",             "ch3o",              "ch4",             "hcco",             "c2h3",             "c2h4",             "c2h5",            "ch3o2",             "c2h6",      "n2"  };
	real potParDB[NSPDB]           = {            107.400,             80.000,            145.000,            107.400,             80.000,             38.000,            107.400,            572.400,            244.000,             98.100,            498.000,            498.000,            144.000,            417.000,            141.400,            150.000,            209.000,            280.800,            252.300,            481.800,            252.300,       3.621};
	real colDimDB[NSPDB]           = {              3.458,              2.750,              2.050,              3.458,              2.750,              2.920,              3.458,              2.605,              3.763,              3.650,              3.590,              3.590,              3.800,              3.690,              3.746,              2.500,              4.100,              3.971,              4.302,              3.626,              4.302,       97.53};

/* 	These parameters are the Lennard-Jones parameters and are referred to by ANSYS Fluent as the 'characteristic length' and the 'energy parameter' respectively.
	potParDB is the Energy Parameter
	colDimDB is the Characteristic Length (angstrom) */


	/* assign properties */
	Message0("\n+++ Assigning species properties. +++\n");
	for(jSp=0;jSp<NSPDB;jSp++)
	{
		mixture_species_loop(mix,sp,iSp)
		{
			if (strcmp(speciesDB[jSp],MIXTURE_SPECIE_NAME(mix,iSp)) == 0)
			/*
			so for example we look is o2 from the mixture setup in fluent?
		   so id O2 string = o2 string from fluent then we assign the
			below phys prop
			note:
			generic_property is a general purpose function that returns the 
			real value for the given property id for the given thread material. 
			It is defined in prop.h and is used only for species properties.
			*/
			{
				Message0("Species %10s added from data base.\n",speciesDB[jSp]);
				
				Mw = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_mwi,T);
				R  = generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_Rgas,T);
				// Message0("generic_property Mw = %f [].\n",Mw);
				// Message0("generic_property R = %f [].\n",R);
				potPar[iSp]    = potParDB[jSp];
				viscCoeff[iSp] = 2.6693e-6*sqrt(Mw)/pow(colDimDB[jSp],2.0);
				condCoeff[iSp] = 5.0/4.0*R;
				
				spCount++;
				spCheck[iSp] = 1;
			}
		}
	}

	if(spCount<NSP)
	{
		Message0("\nERROR!!!: %i species not found in data base.\n",NSP-spCount);
		mixture_species_loop(mix,sp,iSp)
		{
			if(spCheck[iSp] == 0) Message0("Species %s not found.\n",MIXTURE_SPECIE_NAME(mix,iSp));
		}
	}
	else
	{
		Message0("+++ Assigning species properties done. +++\n");
	}
	
	assignFlag = 1;
	//never come back here in this function if the above statment happens
}

/* prepare global variables (i.e. every iteration) */
void prepGlobals(cell_t cell,Thread *thread,Material *mix)
{
	/* loop variables */
	int iSp;
	Material *sp;
	
	/* mixture properties */
	real T = C_T(cell,thread);
	
	mixture_species_loop(mix,sp,iSp)
	{
		visc_i[iSp] = calcVisc(iSp,T,cell,thread,sp,mix);
	}
	
	getXY(X,Y,cell,thread,mix);
	
	mixture_species_loop(mix,sp,iSp)
	{
		delta_i[iSp] = delta(iSp,T,Y,cell,thread,sp,mix);
	}
	
	prepFlag = 1;
}

/* main functions */
real calcVisc(int iSp,real T,cell_t cell,Thread *thread,Material *sp,Material *mix)
{
	/* mixture properties */
	real visc = 0.0;
	real T_red = T/potPar[iSp];
	visc = viscCoeff[iSp]*sqrt(T)/omega_visc(T_red);/* [Pa*s] */
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: calcVisc()\n");
	Message0("*species viscosity\n");
	Message0("*species %10s (No.%3i): viscosity = %e [Pa*s].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,visc);
	Message0("*END DEBUG: calcVisc()\n");
	#endif

	return visc;
}

/* collision integral viscosity */
real omega_visc(real T_red)
{
	real m1 = 3.3530622607;
	real m2 = 2.53272006;
	real m3 = 2.9024238575;
	real m4 = 0.11186138893;
	real m5 = 0.8662326188;
	real m6 = 1.3913958626;
	real m7 = 3.158490576;
	real m8 = 0.18973411754;
	real m9 = 0.00018682962894;
	
	real num,den;
	
	num = m1 + T_red*(m2 + T_red*(m3 + T_red*m4));
	den = m5 + T_red*(m6 + T_red*(m7 + T_red*(m8 + T_red*m9)));
	
	return num/den;
}

/* get mole and mass fractions */
void getXY(real *X,real *Y,cell_t cell,Thread *thread,Material *mix)
{
	/* loop variables */
	int iSp;
	Material *sp;
	
	/* get mole and mass fractions depending on model */
	#if(NONPREM == 1)
	Pdf_XY(cell,thread,X,Y);
	#else
	mixture_species_loop(mix,sp,iSp)
	{
		Y[iSp] = C_YI(cell,thread,iSp);
	}
	Mole_Fraction(mix,Y,X);
	#endif
	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: getXY()\n");
	Message0("*species mole and mass fractions\n");
	mixture_species_loop(mix,sp,iSp)
	{
		Message0("*species %10s (No.%3i): X = [-], %f Y = %f [-].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,X[iSp],Y[iSp]);
	}
	Message0("*END DEBUG: getXY()\n");
	#endif
}
/* delta */
real delta(int iSp,real T,real Y[],cell_t cell,Thread* thread,Material *sp_i,Material *mix)
{
	/* loop variables */
	int jSp;
	Material *sp_j;
	
	real delta = 0.0;
	real Mw_i,Mw_j;
	real G_ij[NSP];
	
	Mw_i = generic_property(cell,thread,MATERIAL_PROPERTY(sp_i),PROP_mwi,T);
	
	mixture_species_loop(mix,sp_j,jSp)
	{
		Mw_j = generic_property(cell,thread,MATERIAL_PROPERTY(sp_j),PROP_mwi,T);
		
		G_ij[jSp]  = 1.0/sqrt(8.0) * pow(1.0+Mw_i/Mw_j,-0.5) * pow(1.0+pow(visc_i[iSp]/visc_i[jSp],0.5)*pow(Mw_j/Mw_i,0.25),2.0);
		delta     += G_ij[jSp]*Mw_i/Mw_j*Y[jSp];
	}
	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: delta()\n");
	Message0("*species %10s (No.%3i)\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp);
	mixture_species_loop(mix,sp_j,jSp)
	{
		Message0("*G_ij  = %f, i = %i, j = %i.\n",G_ij[jSp],iSp,jSp);
	}
	Message0("*delta = %f.\n",delta);
	Message0("*END DEBUG: delta()\n");
	#endif
	
	return delta;
}

real calcCond(int iSp,real T,cell_t cell,Thread *thread,Material *sp,Material *mix)
{
	/* mixture properties */
	real cond = 0.0;
	real cp;
	int nothing;

	cp = Cp_i(T,nothing,iSp); //generic_property(cell,thread,MATERIAL_PROPERTY(sp),PROP_Cp,T); 
	// generic_property wont work, actually the macro works but retreiving cp from fluent is nonesense when writing a UDRGM since cp is defined internally in the code itself abd should be called within!

/* 'generic_property' is a used auxiliary utilities for custom property UDF	it has a general purpose function 
that returns the real value for the given property id for the given thread material. It is defined in prop.h 
and is used only for species properties. */

	cond = visc_i[iSp]*(cp+condCoeff[iSp]);/* [J/m*s*K] */
	
	/* debug */
	#if DEBUGSUB == 1
	Message0("\n*DEBUG: calcCond()\n");
	Message0("*species conductivity\n");
	Message0("*species %10s (No.%3i): conductivity = %f [J/m*s*K].\n",MIXTURE_SPECIE_NAME(mix,iSp),iSp,cond);
	Message0("*END DEBUG: calcCond()\n");
	#endif
	
	return cond;
}
//********************************************************************************************End Knetic Theory part ************************************************************

/* Stuff that can be improved
Y[ ] can be replace by y[]
check hoe the cp  is extracted and what is being extracted */

//*********************************************************Peng-Robinson **************************************************************//
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_spvol                                      */
/*      Returns specific volume given T and P                 */
/*------------------------------------------------------------*/
double RKEOS_spvol(double T, double p, int i)
{
  double n = 0.37464+1.54226*wi[i]-0.26992*wi[i]*wi[i]; //Constant

  //the following results for v is from Matlab with the help of syms, solve tools and coder tools
  double a_tmp;
  double a_tmp_tmp;
  double b_a_tmp;
  double b_a_tmp_tmp;
  double c_a_tmp;
  double c_a_tmp_tmp;
  double d_a_tmp;
  double e_a_tmp;
  double f_a_tmp;
  double g_a_tmp;
  double h_a_tmp;
  double i_a_tmp;
  double j_a_tmp;
  double valve;

  a_tmp = T * tcrit[i];
  a_tmp_tmp = tcrit[i] * a0[i];
  b_a_tmp = a_tmp_tmp * b0[i];
  c_a_tmp = n * n;
  b_a_tmp_tmp = 2.0 * tcrit[i] * a0[i];
  d_a_tmp = b_a_tmp_tmp * b0[i];
  e_a_tmp = d_a_tmp * n;

  f_a_tmp = sqrt(T / tcrit[i]);
  f_a_tmp = (f_a_tmp > 0) ? f_a_tmp : 0;

  g_a_tmp = T * a0[i];
  h_a_tmp = b0[i] * b0[i];
  i_a_tmp = b_a_tmp_tmp * n;
  j_a_tmp = a_tmp * rgas[i] - tcrit[i] * b0[i] * p;
  a_tmp_tmp = ((((((a_tmp_tmp + i_a_tmp) + g_a_tmp * c_a_tmp) + a_tmp_tmp * c_a_tmp) - 3.0 * tcrit[i] * h_a_tmp * p) - i_a_tmp * f_a_tmp) - b_a_tmp_tmp * c_a_tmp * f_a_tmp) - 2.0 * T * tcrit[i] * b0[i] * rgas[i];
  b_a_tmp_tmp = tcrit[i] * tcrit[i];
  i_a_tmp = p * p;
  c_a_tmp_tmp = pow(j_a_tmp, 3.0) / (27.0 * pow(tcrit[i], 3.0) * pow(p, 3.0));
  b_a_tmp = (((((((b_a_tmp - tcrit[i] * pow(b0[i], 3.0) * p) - a_tmp * h_a_tmp * rgas[i]) + g_a_tmp * b0[i] * c_a_tmp) + b_a_tmp * c_a_tmp) + e_a_tmp) - d_a_tmp * c_a_tmp * f_a_tmp) - e_a_tmp * f_a_tmp) /(2.0 * tcrit[i] * p);
  c_a_tmp = j_a_tmp * a_tmp_tmp / (6.0 * b_a_tmp_tmp * i_a_tmp);
  a_tmp = (c_a_tmp_tmp + b_a_tmp) - c_a_tmp;
  d_a_tmp = 3.0 * tcrit[i] * p;
  i_a_tmp = a_tmp_tmp / d_a_tmp - j_a_tmp * j_a_tmp / (9.0 * b_a_tmp_tmp * i_a_tmp);

  valve = sqrt(a_tmp * a_tmp + pow(i_a_tmp, 3.0));
  valve = (valve > 0) ? valve : 0; //in order to not deal with imaginary numbers which will diverge our results

  b_a_tmp = pow(((valve + c_a_tmp_tmp) + b_a_tmp) - c_a_tmp,0.33333333333333331);

  double v = (b_a_tmp - i_a_tmp / b_a_tmp) + j_a_tmp / d_a_tmp;

  return v;

}



//*********************************************************Peng-Robinson **************************************************************//
/*******************************************************************/
/* Species Property Definitions                                    */ 
/*******************************************************************/
void Mw_i() //Molecular Weight (kg/m.s)
{
 mi[0]  = 31.9988;   /*o2*/ 
 mi[1]  = 15.9994;   /*o*/
 mi[2]  = 1.00797;   /*h */
 mi[3]  = 33.00677;  /*ho2*/
 mi[4]  = 17.00737;  /*oh*/
 mi[5]  = 2.01594;   /*h2*/ 
 mi[6]  = 34.01474;  /*h2o2*/
 mi[7]  = 18.01534;  /*h2o*/
 mi[8]  = 44.00995;  /*co2*/ 
 mi[9]  = 28.01055;  /*co*/
 mi[10] = 29.01852;  /*hco*/
 mi[11] = 30.02649;  /*ch2o*/
 mi[12] = 15.03506;  /*ch3*/
 mi[13] = 31.03446;  /*ch3o*/
 mi[14] = 16.04303;  /*ch4*/
 mi[15] = 41.02967;  /*hcco*/
 mi[16] = 27.04621;  /*c2h3*/
 mi[17] = 28.05418;  /*c2h4*/
 mi[18] = 29.06215;  /*c2h5*/
 mi[19] = 47.03386;  /*ch3o2*/ 
 mi[20] = 30.07012;  /*c2h6*/
 mi[21] = 28.0134;   /*N2 */
}

void CharLength()/* Charachteristic Length (angstrom)*/
{
 ch_len_i[0]  = 3.458;  /*o2*/ 
 ch_len_i[1]  = 2.75;   /*o*/
 ch_len_i[2]  = 2.05;   /*h */
 ch_len_i[3]  = 3.458;  /*ho2*/
 ch_len_i[4]  = 2.75;   /*oh*/
 ch_len_i[5]  = 2.92;   /*h2*/ 
 ch_len_i[6]  = 3.458;  /*h2o2*/
 ch_len_i[7]  = 2.605;  /*h2o*/
 ch_len_i[8]  = 3.763;  /*co2*/ 
 ch_len_i[9]  = 3.65;   /*co*/
 ch_len_i[10] = 3.59;   /*hco*/
 ch_len_i[11] = 3.59;   /*ch2o*/
 ch_len_i[12] = 3.8;    /*ch3*/
 ch_len_i[13] = 3.69;   /*ch3o*/
 ch_len_i[14] = 3.746;  /*ch4*/
 ch_len_i[15] = 2.5;    /*hcco*/
 ch_len_i[16] = 4.1;    /*c2h3*/
 ch_len_i[17] = 3.971;  /*c2h4*/
 ch_len_i[18] = 4.302;  /*c2h5*/
 ch_len_i[19] = 3.626;  /*ch3o2*/
 ch_len_i[20] = 4.302;  /*c2h6*/
 ch_len_i[21] = 3.621;  /*N2 */
}

void En()/* the Energy Parameter of a Spiecie (k) */
{
 en_p_i[0]  = 107.4;   /*o2*/ 
 en_p_i[1]  = 80;   /*o*/
 en_p_i[2]  = 145;   /*h */
 en_p_i[3]  = 107.4;  /*ho2*/
 en_p_i[4]  = 80;  /*oh*/
 en_p_i[5]  = 38;   /*h2*/ 
 en_p_i[6]  = 107.4;  /*h2o2*/
 en_p_i[7]  = 572.4;  /*h2o*/
 en_p_i[8]  = 244;  /*co2*/ 
 en_p_i[9]  = 98.1;  /*co*/
 en_p_i[10] = 498;  /*hco*/
 en_p_i[11] = 498;  /*ch2o*/
 en_p_i[12] = 144;  /*ch3*/
 en_p_i[13] = 417;  /*ch3o*/
 en_p_i[14] = 141.4;  /*ch4*/
 en_p_i[15] = 150;  /*hcco*/
 en_p_i[16] = 209;  /*c2h3*/
 en_p_i[17] = 280.8;  /*c2h4*/
 en_p_i[18] = 252.3;  /*c2h5*/
 en_p_i[19] = 481.8;  /*ch3o2*/
 en_p_i[20] = 252.3;  /*c2h6*/
 en_p_i[21] = 97.53; /*N2 */
}

void Tcrit() /* critical temperature */
{ /* K */
 tcrit[0]  = 154.58;   /*o2*/ 
 tcrit[1]  = 44.5;   /*o*/
 tcrit[2]  = 33.2;   /*h */
 tcrit[3]  = 400;  /*ho2*/
 tcrit[4]  = 400;  /*oh*/
 tcrit[5]  = 32.98;   /*h2*/ 
 tcrit[6]  = 545.2;  /*h2o2*/
 tcrit[7]  = 647.14;  /*h2o*/
 tcrit[8]  = 304.12;  /*co2*/ 
 tcrit[9]  = 132.85;  /*co*/
 tcrit[10] = 420.62;  /*hco*/
 tcrit[11] = 340.98;  /*ch2o*/
 tcrit[12] = 93.76;  /*ch3*/
 tcrit[13] = 338.3;  /*ch3o*/
 tcrit[14] = 190.56;  /*ch4*/
 tcrit[15] = 475.33;  /*hcco*/
 tcrit[16] = 292.02;  /*c2h3*/
 tcrit[17] = 282.34;  /*c2h4*/
 tcrit[18] = 297.57;  /*c2h5*/
 tcrit[19] = 10;  /*ch3o2*/
 tcrit[20] = 305.32;  /*c2h6*/
 tcrit[21] = 126.2; /*N2 */
}

void Pcrit() /* critical pressure */
{ /* Pa */
 pcrit[0]  = 5043000;   /*o2*/ 
 pcrit[1]  = 2690000;   /*o*/
 pcrit[2]  = 1360000;   /*h */
 pcrit[3]  = 1.49e+07;  /*ho2*/
 pcrit[4]  = 8200000;  /*oh*/
 pcrit[5]  = 1293000;   /*h2*/ 
 pcrit[6]  = 9353000;  /*h2o2*/
 pcrit[7]  = 2.2064e+07;  /*h2o*/
 pcrit[8]  = 7374000;  /*co2*/ 
 pcrit[9]  = 3494000;  /*co*/
 pcrit[10] = 7790000;  /*hco*/
 pcrit[11] = 7027000;  /*ch2o*/
 pcrit[12] = 7070000;  /*ch3*/
 pcrit[13] = 7010000;  /*ch3o*/
 pcrit[14] = 4599000;    /*ch4*/
 pcrit[15] = 6580000;  /*hcco*/
 pcrit[16] = 6520000;  /*c2h3*/
 pcrit[17] = 5041000;  /*c2h4*/
 pcrit[18] = 5990000;  /*c2h5*/
 pcrit[19] = 1e+10;  /*ch3o2*/
 pcrit[20] = 4872000;  /*c2h6*/
 pcrit[21] = 3398000;   /*N2 */
}

void Wfac()/* Acentric factor of a Spiecie */
{
 wi[0]  = 0.021;   /*o2*/ 
 wi[1]  = 0;   /*o*/
 wi[2]  = 0;   /*h */
 wi[3]  = 0.1;  /*ho2*/
 wi[4]  = 0.2;  /*oh*/
 wi[5]  = -0.217;   /*h2*/ 
 wi[6]  = 1.0384;  /*h2o2*/
 wi[7]  = 0.344;  /*h2o*/
 wi[8]  = 0.225;  /*co2*/ 
 wi[9]  = 0.045;  /*co*/
 wi[10] = 0.28;  /*hco*/
 wi[11] = 0.218;  /*ch2o*/
 wi[12] = 0.06;  /*ch3*/
 wi[13] = 0.09;  /*ch3o*/
 wi[14] = 0.011;  /*ch4*/
 wi[15] = 0.1;  /*hcco*/
 wi[16] = 0.08;  /*c2h3*/
 wi[17] = 0.087;  /*c2h4*/
 wi[18] = 0.11;  /*c2h5*/
 wi[19] = 0;  /*ch3o2*/
 wi[20] = 0.099;  /*c2h6*/
 wi[21] = 0.037; /*N2 */
}

// double Cp_i(double T, double r, int i)//obliged to define cant be called externally from Fluent !
// {
//  double cpi[22];

//  cpi[0]  = 919.31; /*o2*/ //----->Active
//  cpi[1]  = 1368.75;/*o*/
//  cpi[2]  = 20621;  /*h */
//  cpi[3]  = 1273;   /*ho2*/
//  cpi[4]  = 1713.21;/*oh*/
//  cpi[5]  = 14283;  /*h2*/ //----->Active
//  cpi[6]  = 1235.42;/*h2o2*/
//  cpi[7]  = 4182;   /*h2o*/    // To be checked checkk!!c
//  cpi[8]  = 840.37; /*co2*/ //----->Active
//  cpi[9]  = 1043;   /*co*/
//  cpi[10] = 29.018; /*hco*/ //This is to be modefied when this spiecie is present 
//  cpi[11] = 30.049; /*ch2o*/ //This is to be modefied when this spiecie is present 
//  cpi[12] = 15.506; /*ch3*/ //This is to be modefied when this spiecie is present 
//  cpi[13] = 31.446; /*ch3o*/ //This is to be modefied when this spiecie is present 
//  cpi[14] = 2222;   /*ch4*/
//  cpi[15] = 41.027; /*hcco*/ //This is to be modefied when this spiecie is present 
//  cpi[16] = 2233;   /*c2h3*/ //This is to be modefied when this spiecie is present 
//  cpi[17] = 2233;   /*c2h4*/
//  cpi[18] = 29.062; /*c2h5*/ //This is to be modefied when this spiecie is present 
//  cpi[19] = 47.086; /*ch3o2*/ //This is to be modefied when this spiecie is present 
//  cpi[20] = 30.012; /*c2h6*/ //This is to be modefied when this spiecie is present 
//  cpi[21] = 30.012; /*N2 */
//  return cpi[i];
// }










double CC_i(int c, int e, int i) //Thermdat Cp Coeficient
{
    //****************************  To be filled ****************************

    //           c  e   i     ---->>>  c:Polynomial Coeficient     e:piece-wiseequation number     i:Spieci ID
    double CC[5][2][22];

    /*o2*/
    CC[0][0][0] = 3.78245636e+00;
    CC[1][0][0] = -2.99673415e-03;
    CC[2][0][0] = 9.84730200e-06;
    CC[3][0][0] = -9.68129508e-09;
    CC[4][0][0] = 3.24372836e-12;

    CC[0][1][0] = 3.66096083e+00;
    CC[1][1][0] = 6.56365523e-04;
    CC[2][1][0] = -1.41149485e-07;
    CC[3][1][0] = 2.05797658e-11;
    CC[4][1][0] = -1.29913248e-15;


    /*o*/
    CC[0][0][1] = 3.16826710e+00;
    CC[1][0][1] = -3.27931884e-03;
    CC[2][0][1] = 6.64306396e-06;
    CC[3][0][1] = -6.12806624e-09;
    CC[4][0][1] = 2.11265971e-12;

    CC[0][1][1] = 2.54363697e+00;
    CC[1][1][1] = -2.73162486e-05;
    CC[2][1][1] = -4.19029520e-09;
    CC[3][1][1] = 4.95481845e-12;
    CC[4][1][1] = -4.79553694e-16;


    /*h*/
    CC[0][0][2] = 2.50000000e+00;
    CC[1][0][2] = 00000000;
    CC[2][0][2] = 00000000;
    CC[3][0][2] = 00000000;
    CC[4][0][2] = 00000000;

    CC[0][1][2] = 2.50000000e+00;
    CC[1][1][2] = 00000000;
    CC[2][1][2] = 00000000;
    CC[3][1][2] = 00000000;
    CC[4][1][2] = 00000000;


    /*ho2*/
    CC[0][0][3] = 4.30178800e+00;
    CC[1][0][3] = -4.74902010e-03;
    CC[2][0][3] = 2.11579530e-05;
    CC[3][0][3] = -2.42759610e-08;
    CC[4][0][3] = 9.29206700e-12;

    CC[0][1][3] = 4.17226590e+00;
    CC[1][1][3] = 1.88120980e-03;
    CC[2][1][3] = -3.46292970e-07;
    CC[3][1][3] = 1.94685160e-11;
    CC[4][1][3] = 1.76091530e-16;


    /*oh*/
    CC[0][0][4] = 3.99198424e+00;
    CC[1][0][4] = -2.40106655e-03;
    CC[2][0][4] = 4.61664033e-06;
    CC[3][0][4] = -3.87916306e-09;
    CC[4][0][4] = 1.36319502e-12;

    CC[0][1][4] = 2.83853033e+00;
    CC[1][1][4] = 1.10741289e-03;
    CC[2][1][4] = -2.94000209e-07;
    CC[3][1][4] = 4.20698729e-11;
    CC[4][1][4] = -2.42289890e-15;


    /*h2*/
    CC[0][0][5] = 2.34430290e+00;
    CC[1][0][5] = 7.98042480e-03;
    CC[2][0][5] = -1.94779170e-05;
    CC[3][0][5] = 2.01569670e-08;
    CC[4][0][5] = -7.37602890e-12;

    CC[0][1][5] = 2.93283050e+00;
    CC[1][1][5] = 8.26598020e-04;
    CC[2][1][5] = -1.46400570e-07;
    CC[3][1][5] = 1.54098510e-11;
    CC[4][1][5] = -6.88796150e-16;


    /*h2o2*/
    CC[0][0][6] = 4.31515149e+00;
    CC[1][0][6] = -8.47390622e-04;
    CC[2][0][6] = 1.76404323e-05;
    CC[3][0][6] = -2.26762944e-08;
    CC[4][0][6] = 9.08950158e-12;

    CC[0][1][6] = 4.57977305e+00;
    CC[1][1][6] = 4.05326003e-03;
    CC[2][1][6] = -1.29844730e-06;
    CC[3][1][6] = 1.98211400e-10;
    CC[4][1][6] = -1.13968792e-14;


    /*h2o*/
    CC[0][0][7] = 4.19863520e+00;
    CC[1][0][7] = -2.03640170e-03;
    CC[2][0][7] = 6.52034160e-06;
    CC[3][0][7] = -5.48792690e-09;
    CC[4][0][7] = 1.77196800e-12;

    CC[0][1][7] = 2.67703890e+00;
    CC[1][1][7] = 2.97318160e-03;
    CC[2][1][7] = -7.73768890e-07;
    CC[3][1][7] = 9.44335140e-11;
    CC[4][1][7] = -4.26899910e-15;


    /*co2*/
    CC[0][0][8] = 2.35681300e+00;
    CC[1][0][8] = 8.98412990e-03;
    CC[2][0][8] = -7.12206320e-06;
    CC[3][0][8] = 2.45730080e-09;
    CC[4][0][8] = -1.42885480e-13;

    CC[0][1][8] = 4.63651110e+00;
    CC[1][1][8] = 2.74145690e-03;
    CC[2][1][8] = -9.95897590e-07;
    CC[3][1][8] = 1.60386660e-10;
    CC[4][1][8] = -9.16198570e-15;


    /*co*/
    CC[0][0][9] = 3.57953350e+00;
    CC[1][0][9] = -6.10353690e-04;
    CC[2][0][9] = 1.01681430e-06;
    CC[3][0][9] = 9.07005860e-10;
    CC[4][0][9] = -9.04424490e-13;

    CC[0][1][9] = 3.04848590e+00;
    CC[1][1][9] = 1.35172810e-03;
    CC[2][1][9] = -4.85794050e-07;
    CC[3][1][9] = 7.88536440e-11;
    CC[4][1][9] = -4.69807460e-15;


    /*hco*/
    CC[0][0][10] = 4.23754610e+00;
    CC[1][0][10] = -3.32075257e-03;
    CC[2][0][10] = 1.40030264e-05;
    CC[3][0][10] = -1.34239995e-08;
    CC[4][0][10] = 4.37416208e-12;

    CC[0][1][10] = 3.92001542e+00;
    CC[1][1][10] = 2.52279324e-03;
    CC[2][1][10] = -6.71004164e-07;
    CC[3][1][10] = 1.05615948e-10;
    CC[4][1][10] = -7.43798261e-15;


    /*ch2o*/
    CC[0][0][11] = 4.79370360e+00;
    CC[1][0][11] = -9.90815180e-03;
    CC[2][0][11] = 3.73214590e-05;
    CC[3][0][11] = -3.79279020e-08;
    CC[4][0][11] = 1.31770150e-11;

    CC[0][1][11] = 3.16948070e+00;
    CC[1][1][11] = 6.19327420e-03;
    CC[2][1][11] = -2.25059810e-06;
    CC[3][1][11] = 3.65982450e-10;
    CC[4][1][11] = -2.20154100e-14;


    /*ch3*/
    CC[0][0][12] = 3.65717970e+00;
    CC[1][0][12] = 2.12659790e-03;
    CC[2][0][12] = 5.45838830e-06;
    CC[3][0][12] = -6.61810030e-09;
    CC[4][0][12] = 2.46570740e-12;

    CC[0][1][12] = 2.97812060e+00;
    CC[1][1][12] = 5.79785200e-03;
    CC[2][1][12] = -1.97558000e-06;
    CC[3][1][12] = 3.07297900e-10;
    CC[4][1][12] = -1.79174160e-14;


    /*ch3o*/
    CC[0][0][13] = 3.71180502e+00;
    CC[1][0][13] = -2.80463306e-03;
    CC[2][0][13] = 3.76550971e-05;
    CC[3][0][13] = -4.73072089e-08;
    CC[4][0][13] = 1.86588420e-11;

    CC[0][1][13] = 4.75779238e+00;
    CC[1][1][13] = 7.44142474e-03;
    CC[2][1][13] = -2.69705176e-06;
    CC[3][1][13] = 4.38090504e-10;
    CC[4][1][13] = -2.63537098e-14;


    /*ch4*/
    CC[0][0][14] = 5.14825732e+00;
    CC[1][0][14] = -1.37002410e-02;
    CC[2][0][14] = 4.93749414e-05;
    CC[3][0][14] = -4.91952339e-08;
    CC[4][0][14] = 1.70097299e-11;

    CC[0][1][14] = 1.91178600e+00;
    CC[1][1][14] = 9.60267960e-03;
    CC[2][1][14] = -3.38387841e-06;
    CC[3][1][14] = 5.38797240e-10;
    CC[4][1][14] = -3.19306807e-14;


    /*hcco*/
    CC[0][0][15] = 2.33501180e+00;
    CC[1][0][15] = 1.70100830e-02;
    CC[2][0][15] = -2.20188670e-05;
    CC[3][0][15] = 1.54064470e-08;
    CC[4][0][15] = -4.34550970e-12;

    CC[0][1][15] = 5.84690060e+00;
    CC[1][1][15] = 3.64059600e-03;
    CC[2][1][15] = -1.29590070e-06;
    CC[3][1][15] = 2.07969190e-10;
    CC[4][1][15] = -1.24000220e-14;


    /*c2h3*/
    CC[0][0][16] = 3.36377642e+00;
    CC[1][0][16] = 2.65765722e-04;
    CC[2][0][16] = 2.79620704e-05;
    CC[3][0][16] = -3.72986942e-08;
    CC[4][0][16] = 1.51590176e-11;

    CC[0][1][16] = 4.15026763e+00;
    CC[1][1][16] = 7.54021341e-03;
    CC[2][1][16] = -2.62997847e-06;
    CC[3][1][16] = 4.15974048e-10;
    CC[4][1][16] = -2.45407509e-14;


    /*c2h4*/
    CC[0][0][17] = 3.95920063e+00;
    CC[1][0][17] = -7.57051373e-03;
    CC[2][0][17] = 5.70989993e-05;
    CC[3][0][17] = -6.91588352e-08;
    CC[4][0][17] = 2.69884190e-11;

    CC[0][1][17] = 3.99182724e+00;
    CC[1][1][17] = 1.04833908e-02;
    CC[2][1][17] = -3.71721342e-06;
    CC[3][1][17] = 5.94628366e-10;
    CC[4][1][17] = -3.53630386e-14;


    /*c2h5*/
    CC[0][0][18] = 4.30646568e+00;
    CC[1][0][18] = -4.18658892e-03;
    CC[2][0][18] = 4.97142807e-05;
    CC[3][0][18] = -5.99126606e-08;
    CC[4][0][18] = 2.30509004e-11;

    CC[0][1][18] = 1.95465642e+00;
    CC[1][1][18] = 1.73972722e-02;
    CC[2][1][18] = -7.98206668e-06;
    CC[3][1][18] = 1.75217689e-09;
    CC[4][1][18] = -1.49641576e-13;


    /*ch3o2*/
    CC[0][0][19] = 3.80497590e+00;
    CC[1][0][19] = 9.80784660e-03;
    CC[2][0][19] = -3.90940624e-07;
    CC[3][0][19] = -2.23072602e-09;
    CC[4][0][19] = 6.43310820e-13;

    CC[0][1][19] = 6.34718801e+00;
    CC[1][1][19] = 7.92089358e-03;
    CC[2][1][19] = -2.76601913e-06;
    CC[3][1][19] = 4.35360631e-10;
    CC[4][1][19] = -2.54984762e-14;


    /*c2h6*/
    CC[0][0][20] = 4.29142572e+00;
    CC[1][0][20] = -5.50154901e-03;
    CC[2][0][20] = 5.99438458e-05;
    CC[3][0][20] = -7.08466469e-08;
    CC[4][0][20] = 2.68685836e-11;

    CC[0][1][20] = 4.04666411e+00;
    CC[1][1][20] = 1.53538802e-02;
    CC[2][1][20] = -5.47039485e-06;
    CC[3][1][20] = 8.77826544e-10;
    CC[4][1][20] = -5.23167531e-14;


    /*N2*/
    CC[0][0][21] = 3.29867700e+00;
    CC[1][0][21] = 1.40824040e-03;
    CC[2][0][21] = -3.96322200e-06;
    CC[3][0][21] = 5.64151500e-09;
    CC[4][0][21] = -2.44485400e-12;

    CC[0][1][21] = 2.92664000e+00;
    CC[1][1][21] = 1.48797680e-03;
    CC[2][1][21] = -5.68476000e-07;
    CC[3][1][21] = 1.00970380e-10;
    CC[4][1][21] = -6.75335100e-15;



    return CC[c][e][i];


}


double Cp_i(double T, double r, int i)//forget about r, T is the temperature and 'i' is the spiecie index for example 0 is O2
{
    double cpi;
    int T_min = 100;
    int T_max = 6000;

    double R = 8314.4621; // J / kg - kmol

    int T_split = 1000; //from Thermdat for all spiecies expect for T_split_ch3o2
    int T_split_ch3o2 = 1365;



    //if condition statment depending on T

    if (i == 19) {
        if (T < T_split_ch3o2) {
            //  code expression Statment ****************************  To be filled ****************************

            cpi = (R / mi[i]) * (CC_i(0, 0, i) * pow(T, 0) + CC_i(1, 0, i) * pow(T, 1) + CC_i(2, 0, i) * pow(T, 2) + CC_i(3, 0, i) * pow(T, 3) + CC_i(4, 0, i) * pow(T, 4));
        }

        else if (T >= T_split_ch3o2) {
            //  code expression Statment ****************************  To be filled ****************************

            cpi = (R / mi[i]) * (CC_i(0, 1, i) * pow(T, 0) + CC_i(1, 1, i) * pow(T, 1) + CC_i(2, 1, i) * pow(T, 2) + CC_i(3, 1, i) * pow(T, 3) + CC_i(4, 1, i) * pow(T, 4));
        }


        else {
             //  printf("\n Temperature out of range!= %d \n", T);
        }
    }

    else {
        if (T < T_split) {
            //  code expression Statment ****************************  To be filled ****************************

            cpi = (R / mi[i]) * (CC_i(0, 0, i) * pow(T, 0) + CC_i(1, 0, i) * pow(T, 1) + CC_i(2, 0, i) * pow(T, 2) + CC_i(3, 0, i) * pow(T, 3) + CC_i(4, 0, i) * pow(T, 4));
        }

        else if (T >= T_split) {
            //  code expression Statment ****************************  To be filled ****************************

            cpi = (R / mi[i]) * (CC_i(0, 1, i) * pow(T, 0) + CC_i(1, 1, i) * pow(T, 1) + CC_i(2, 1, i) * pow(T, 2) + CC_i(3, 1, i) * pow(T, 3) + CC_i(4, 1, i) * pow(T, 4));

        }

        else {
          //   printf("\n Temperature out of range!= %d \n", T);
        }
    }

    return cpi;
}
