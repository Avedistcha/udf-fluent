# UDRGM for One Specie (one Material) 

UDRGM for one sample Fluid material will be elaborated here and compared to the fluent version.\
The following code can be used but it serves as a **Template**.\
It should be noted same Physical Properties of the UDRG should be used in Fluent UI in order to receive similar results.\
Once similarity in the output is produced and you feel confortable with the template code you may start to costumize your own  physical properties in the UDRGM. :rocket:

## Custom_UDRGM_Multiple_Spiecies note
In this section we look forward to develop our own custom or exactly similar Physical Properties as Fluent..\
You might ask yourself why are we re-writing the same model code for a specific Physical properties if it exist in Fluent?.\
The answer this: once a UDRGM is loaded inside Fluent, you wont be able to select any Physical properties from Fluent, and you need to defince all Physical properties for the Spiecies as well as for the mixture.\
In other words if you want to use ideal Gas same as Fluent for density but customise your own Thermal Conductivity, in this case it is also required for you to re-write and define the Ideal gas law in you UDRGM eventho this model exist in Fluent.

### A good news for the above problem
Most of the already existing Physical Properties in Fluent will be defined and re-programed here so you don't have to panic and figure out how to write the code of some Physical properties models you just need to check the specific code covered under your intended Physical Properties, extracte it and use it, specifically the following will be elaborated tested and post proccesed with Fluent:

### User Defined Physical properties Used for our sample codes 

| Properties                  | Model/Law 1          | Model/Law 2              | Model/Law  3              |Model/Law  4   |
| -------------               | -------------        | -------------            | -------------             | ------------- | 
| **Thermal Conductivity**    | Kinetic Theory       | -------------            | -------------             | ------------- |
| **Viscosity:**              | Kinetic Theory       | -------------            | -------------             | ------------- |
| **Density:**                | Ideal Gas            | Peng And Robinson        | -------------             | ------------- |



### Returned Physical properties to Fluent which is common to all code
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow.

### Loading the code using TUI common to all one spiecies UDRGM
```
define/user-defined/real-gas-models/user-defined-real-gas-model
```
```
use user-defined real gas? [no] 
```
type yes
```
use user-defined real gas? [no] yes
```
```
User-Defined Realgas Library Name [""] 
```
type the generated lib folder name, by default it is libudf unless you named it differently
```
User-Defined Realgas Library Name [""] libudf
```
<img src="Images/rocket-launch.svg" width="300" height="300">
