#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

#if RP_DOUBLE
#define SMLL 1.e-20
#else
#define SMLL 1.e-10
#endif

#define NSPECIE_NAME 80 
#define RGASU UNIVERSAL_GAS_CONSTANT /* 8314.34 SI units: J/Kmol/K */
#define PI  3.141592654
/* Here input the number of species in the mixture */
/* THIS IS A USER INPUT */
#define n_specs 5

static int (*usersMessage)(const char *,...);
static void (*usersError)(const char *,...);

static double ref_p, ref_T;
static char  gas[n_specs][NSPECIE_NAME];

/* static property parameters */
static double cp[5][n_specs]; /* specific heat polynomial coefficients */
static double mw[n_specs];  /* molecular weights */
static double hf[n_specs];  /* formation enthalpy */
static double tcrit[n_specs]; /* critical temperature */

static double pcrit[n_specs]; /* critical pressure */
static double vcrit[n_specs]; /* critical specific volume */
static double nrk[n_specs];  /* exponent n of function a(T) in Redlich-Kwong 
                 equation of state */
static double omega[n_specs]; /* acentric factor */

/* Static variables associated with Redlich-Kwong Model */
static double rgas[n_specs], a0[n_specs], b0[n_specs], c0[n_specs], 
 bb[n_specs], cp_int_ref[n_specs];

void Mw();
void Cp_Parameters(); 
void Hform();
void Tcrit();
void Pcrit();
void Vcrit();
void NRK();
void Omega();

double RKEOS_spvol(double temp, double press, int i);
double RKEOS_dvdp(double temp, double density, int i);
double RKEOS_dvdt(double temp, double density, int i);
double RKEOS_H_ideal_gas(double temp, int i);
double RKEOS_specific_heat(double temp, double density, int i);
double RKEOS_enthalpy(double temp, double density, int i);
double RKEOS_entropy(double temp, double density, int i);
double RKEOS_viscosity(double temp, int i);
double RKEOS_thermal_conductivity(double temp, int i);
double RKEOS_vol_specific_heat(double temp, double density, int i);

DEFINE_ON_DEMAND(I_do_nothing)
{
 /*
  This is a dummy function
  must be included to allow for the use of the
  ANSYS FLUENT UDF compilation utility
 */
}

/*******************************************************************/
/* Mixture Functions                                               */ 
/* These are the only functions called from ANSYS FLUENT Code      */ 
/*******************************************************************/
void MIXTURE_Setup(Domain *domain, cxboolean vapor_phase, char *specielist,
          int (*messagefunc)(const char *format,...),
          void (*errorfunc)(const char *format,...))
{
 /* This function will be called from ANSYS FLUENT after the
   UDF library has been loaded.
   User must enter the number of species in the mixture
   and the name of the individual species.
 */

 int i;
 usersMessage = messagefunc;
 usersError  = errorfunc;
 ref_p    = ABS_P(RP_Get_Real("reference-pressure"),op_pres);
 ref_T    = 298.15;

 Message0("\n MIXTURE_Setup: Redlich-Kwong equation of State"
      " with ideal-gas mixing rules \n");
 Message0("\n MIXTURE_Setup: reference-temperature is %f \n", ref_T);

 if (ref_p == 0.0)
  {
     Message0("\n MIXTURE_Setup: reference-pressure was not set by user \n");
     Message0("\n MIXTURE_Setup: setting reference-pressure to 101325 Pa \n");
     ref_p = 101325.0;
  }
 /*====================================================*/
 /*=========  User Input Section ======================*/
  /*====================================================*/
 /*
  Define Species name.
  DO NOT use space for naming species
 */
 (void)strcpy(gas[0],"H2O"); 
 (void)strcpy(gas[1],"CH4");
 (void)strcpy(gas[2],"O2") ; 
 (void)strcpy(gas[3],"CO2");
 (void)strcpy(gas[4],"N2") ; 

 /*====================================================*/
 /*=========  End Of User Input Section ===============*/
 /*====================================================*/

 Message0("\n MIXTURE_Setup: RealGas mixture initialization \n");
 Message0("\n MIXTURE_Setup: Number of Species = %d \n",n_specs);
 for (i=0; i<n_specs; ++i)
  {   
     Message0("\n MIXTURE_Setup: Specie[%d]     = %s \n",i,gas[i]);
  }

 /*
  concatenate species name into one string
  and send back to fluent
 */
 strcat(specielist,gas[0]);
 for (i=1; i<n_specs; ++i)
  {
     strcat(specielist," ");
     strcat(specielist,gas[i]);
  }

 /* initialize */
 Mw();
 Cp_Parameters(); 
 Hform();
 Tcrit();
 Pcrit();
 Vcrit();
 Omega();
 NRK();

 for (i=0; i<n_specs; ++i)
  {
   rgas[i] = RGASU/mw[i];
   a0[i] = 0.42747*rgas[i]*rgas[i]*tcrit[i]*tcrit[i]/pcrit[i];
   b0[i] = 0.08664*rgas[i]*tcrit[i]/pcrit[i];
   c0[i] = rgas[i]*tcrit[i]/(pcrit[i]+a0[i]/(vcrit[i]*(vcrit[i]+b0[i])))
       +b0[i]-vcrit[i];
   bb[i] = b0[i]-c0[i];
   cp_int_ref[i] = cp[0][i]*log(ref_T)+ref_T*(cp[1][i]+ref_T*(0.5*cp[2][i]
           +ref_T*(0.333333*cp[3][i]+0.25*cp[4][i]*ref_T))); 
  }
}

double MIXTURE_mw(double yi[])
{
 double MW, sum=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
    sum += yi[i]/mw[i];

 MW = 1.0/MAX(sum,SMLL)   ;

 return MW;   /* (Kg/Kmol) */
}

double MIXTURE_density(cell_t cell, Thread *thread, cxboolean vapor_phase, double temp, double P, double yi[])
{
 double den=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
  {
     if (yi[i]> SMLL)
        den += yi[i]*RKEOS_spvol(temp, P, i); 
  }

 return 1./den; /* (Kg/m^3) */
}

double MIXTURE_specific_heat(cell_t cell, Thread *thread, double temp, double density, double P, 
               double yi[])
{
 double cp=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
     if (yi[i]> SMLL)
        cp += yi[i]*RKEOS_specific_heat(temp,mw[i]*density/MIXTURE_mw(yi),i);

 return cp;   /* (J/Kg/K) */
}

double MIXTURE_enthalpy(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
 double h=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
      h += yi[i]*RKEOS_enthalpy(temp, mw[i]*density/MIXTURE_mw(yi), i);

 return h;   /* (J/Kg) */
}

double MIXTURE_enthalpy_prime(cell_t cell, Thread *thread, double temp, double density, double P, 
               double yi[], double hi[])
{
 double h=0.0;
 int i;

 for (i=0; i<n_specs; ++i)
{
  hi[i] = hf[i]/mw[i] + RKEOS_enthalpy(temp, mw[i]*density/MIXTURE_mw(yi), 
      i);
   if (yi[i]> SMLL)
      h += yi[i]*(hf[i]/mw[i] + RKEOS_enthalpy(temp, mw[i]*density/MIXTURE_mw(yi), i));
}

 return h;   /* (J/Kg) */
}

double MIXTURE_entropy(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
 double s  = 0.0 ;
 double sum = 0.0;
 double xi[n_specs];
 int i;

 for (i=0; i<n_specs; ++i)
  {
     xi[i] = yi[i] / mw[i];
     sum  += xi[i];
  }
 for (i=0; i<n_specs; ++i)
    xi[i] /= sum;

 for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
      s += yi[i]*RKEOS_entropy(temp,mw[i]*density/MIXTURE_mw(yi), i)-
          UNIVERSAL_GAS_CONSTANT/MIXTURE_mw(yi)* xi[i] * log(xi[i]);

 return s;   /* (J/Kg/K) */
}

double MIXTURE_viscosity(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
 double mu=0.;
 int i;

 for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
        mu += yi[i]*RKEOS_viscosity(temp,i);

 return mu;   /* (Kg/m/s) */
}

double MIXTURE_thermal_conductivity(cell_t cell, Thread *thread, double temp, double density, double P, 
                  double yi[])
{
 double kt=0.;
 int i;

 for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
        kt += yi[i]* RKEOS_thermal_conductivity(temp,i);

 return kt;   /* W/m/K */
}

/*------------------------------------------------------------*/
/* FUNCTION: MIXTURE_speed_of_sound                           */
/*      Returns s.o.s given T and rho                         */
/*------------------------------------------------------------*/

double MIXTURE_speed_of_sound(cell_t cell, Thread *thread, double temp, double density, double P, 
               double yi[])
{
  double dvdp = 0.; 
  double cv = 0.;
  double v = 1./density;  
  int i;
  double cp = MIXTURE_specific_heat(cell, thread, temp, density, P, yi);

  for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
   {
      dvdp += yi[i]*RKEOS_dvdp(temp, mw[i]*density/MIXTURE_mw(yi),i);
      cv += yi[i]*RKEOS_vol_specific_heat(temp, mw[i]*density/MIXTURE_mw(yi), 
           i);
   }

  return sqrt(- cp/cv/dvdp)*v; 
}

/*------------------------------------------------------------*/
/* FUNCTION: MIXTURE_rho_t                                    */
/*------------------------------------------------------------*/

double MIXTURE_rho_t(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
 double rho_t = 0.;
 int i;

 for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
        rho_t -= yi[i]*density*density*RKEOS_dvdt(temp,
              mw[i]*density/MIXTURE_mw(yi), i);
 return rho_t;
}
/*------------------------------------------------------------*/
/* FUNCTION: MIXTURE_rho_p                                    */
/*------------------------------------------------------------*/

double MIXTURE_rho_p(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
 double rho_p = 0.;
 int i;

 for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
          rho_p -= yi[i]*density*density*RKEOS_dvdp(temp, 
              mw[i]*density/MIXTURE_mw(yi), i);
 return rho_p;
}

/*------------------------------------------------------------*/
/* FUNCTION: MIXTURE_enthalpy_t                               */
/*------------------------------------------------------------*/

double MIXTURE_enthalpy_t(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
  return MIXTURE_specific_heat(cell, thread, temp, density, P, yi);
}

/*------------------------------------------------------------*/
/* FUNCTION: MIXTURE_enthalpy_p                               */
/*------------------------------------------------------------*/

double MIXTURE_enthalpy_p(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
  double v = 1./density;
  double dvdt = 0.0;
  int i;

  for (i=0; i<n_specs; ++i)
   if (yi[i]> SMLL)
       dvdt += yi[i]*RKEOS_dvdt(temp, mw[i]*density/MIXTURE_mw(yi), i);

  return v-temp*dvdt;
}

/*******************************************************************/
/* Species Property Definitions                                    */ 
/*******************************************************************/

void Mw() /* molecular weight */
{ /* Kg/Kmol */
 mw[0] = 18.01534; /*H2O*/
 mw[1] = 16.04303; /*CH4*/
 mw[2] = 31.99880; /*O2 */
 mw[3] = 44.00995; /*CO2*/
 mw[4] = 28.01340; /*N2 */
}

void Pcrit() /* critical pressure */
{ /* Pa */
 pcrit[0] = 220.64e5; /*H2O*/
 pcrit[1] = 4.48e6; /*CH4*/
 pcrit[2] = 5066250.; /*O2 */
 pcrit[3] = 7.3834e6; /*CO2*/
 pcrit[4] = 33.98e5; /*N2 */
}

void Tcrit() /* critical temperature */
{ /* K */
 tcrit[0] = 647.; /*H2O*/
 tcrit[1] = 191.; /*CH4*/
 tcrit[2] = 155.; /*O2 */
 tcrit[3] = 304.; /*CO2*/

  tcrit[4] = 126.2; /*N2 */
}

void Vcrit() /* critical specific volume */
{ /* m3/Kg */
 vcrit[0] = 0.003111; /*H2O*/
 vcrit[1] = 0.006187; /*CH4*/
 vcrit[2] = 0.002294; /*O2 */
 vcrit[3] = 0.002136; /*CO2*/
 vcrit[4] = 0.003196; /*N2 */
}

void NRK() /* exponent n of function a(T) in Redlich-Kwong equation of 
       state */
{
 int i;
 for (i=0; i<n_specs; ++i)
      nrk[i]= 0.4986 + 1.1735*omega[i] + 0.475*omega[i]*omega[i];
}
void Omega() /* acentric factor */
{
 omega[0] = 0.348; /*H2O*/
 omega[1] = 0.007; /*CH4*/
 omega[2] = 0.021; /*O2 */
 omega[3] = 0.225; /*CO2*/
 omega[4] = 0.040; /*N2 */
}

void Hform() /* formation enthalpy */
{
 /*J/Kmol*/
 hf[0] = -2.418379e+08; /*H2O*/
 hf[1] = -74895176.;  /*CH4*/
 hf[2] = 0.;      /*O2 */
 hf[3] = -3.9353235e+08;/*CO2*/
 hf[4] = 0.;      /*N2 */
}

void Cp_Parameters() /* coefficients of specific heat polynomials */
{ /* J/Kg/K */
 cp[0][0] = 1609.791 ; /*H2O*/
 cp[1][0] = 0.740494; 
 cp[2][0] =-9.129835e-06;
 cp[3][0] =-3.813924e-08;
 cp[4][0] =4.80227e-12;

 cp[0][1] = 872.4671 ; /*CH4*/
 cp[1][1] = 5.305473; 
 cp[2][1] = -0.002008295;
 cp[3][1] = 3.516646e-07;
 cp[4][1] = -2.33391e-11 ;

 cp[0][2] = 811.1803 ; /*O2 */
 cp[1][2] =0.4108345; 
 cp[2][2] =-0.0001750725;
 cp[3][2] = 3.757596e-08;
 cp[4][2] =-2.973548e-12;

 cp[0][3] = 453.577; /*CO2*/
 cp[1][3] = 1.65014; 
 cp[2][3] = -1.24814e-3;
 cp[3][3] = 3.78201e-7;
 cp[4][3] = 0.;

 cp[0][4] = 938.8992; /*N2 */
 cp[1][4] = 0.3017911; 
 cp[2][4] = -8.109228e-05;
 cp[3][4] = 8.263892e-09 ;
 cp[4][4] = -1.537235e-13;
}

/*************************************************************/
/*                                                           */
/* User-Defined Function: Redlich-Kwong Equation of State    */
/*            for Real Gas Modeling                          */
/*                                                           */
/* Author: Frank Kelecy                                      */
/*  Date: May 2003                                           */
/*Modified: Rana Faltsi                                      */
/*  Date: December 2006                                      */
/*                                                           */
/*                                                           */
/*************************************************************/
/* OPTIONAL REFERENCE (OFFSET) VALUES FOR ENTHALPY AND ENTROPY */

#define H_REF 0.0
#define S_REF 0.0
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_pressure of species i                      */
/*      Returns pressure given T and density                  */
/*------------------------------------------------------------*/

double RKEOS_pressure(double temp, double density, int i)
{
  double v = 1./density;
  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);
  return rgas[i]*temp/(v-bb[i])-afun/(v*(v+b0[i]));
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_spvol of species i                         */
/*      Returns specific volume given T and P                 */
/*------------------------------------------------------------*/

double RKEOS_spvol(double temp, double press, int i)
{
  double a1,a2,a3;
  double vv,vv1,vv2,vv3;
  double qq,qq3,sqq,rr,tt,dd;

  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);

  a1 = c0[i]-rgas[i]*temp/press;
  a2 = -(bb[i]*b0[i]+rgas[i]*temp*b0[i]/press-afun/press);
  a3 = -afun*bb[i]/press;

  /* Solve cubic equation for specific volume */

  qq = (a1*a1-3.*a2)/9.;
  rr = (2*a1*a1*a1-9.*a1*a2+27.*a3)/54.;
  qq3 = qq*qq*qq;
  dd = qq3-rr*rr;

  /* If dd < 0, then we have one real root */
  /* If dd >= 0, then we have three roots -> choose largest root */

  if (dd < 0.) {
     tt = -SIGN(rr)*(pow(sqrt(-dd)+fabs(rr),0.333333));
     vv = (tt+qq/tt)-a1/3.;
  } else {
     if (rr/sqrt(qq3)<-1) {
        tt = PI;
     } else if (rr/sqrt(qq3)>1) {
        tt = 0;
     } else {
        tt = acos(rr/sqrt(qq3));
     }
   sqq = sqrt(qq);
   vv1 = -2.*sqq*cos(tt/3.)-a1/3.;
   vv2 = -2.*sqq*cos((tt+2.*PI)/3.)-a1/3.;
   vv3 = -2.*sqq*cos((tt+4.*PI)/3.)-a1/3.;
   vv = (vv1 > vv2) ? vv1 : vv2;

      vv = (vv > vv3) ? vv : vv3;
   /*Message0("Three roots %f %f %f \n",vv1, vv2, vv3);*/
  }

  return vv;
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_dvdp                                       */
/*      Returns dv/dp given T and rho                         */
/*------------------------------------------------------------*/

double RKEOS_dvdp(double temp, double density, int i)
{
  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);
  double dterm1,dterm2;
  double v   = 1./ density;

  dterm1  = -rgas[i]*temp*pow((v-b0[i]+c0[i]), -2.0);
  dterm2  = afun*(2.0*v+b0[i])*pow(v*(v+b0[i]),-2.0);

  return 1./(dterm1+dterm2);
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_dvdt                                       */
/*      Returns dv/dT given T and rho                         */
/*------------------------------------------------------------*/

double RKEOS_dvdt(double temp, double density, int i)
{
  double dpdT, dterm1, dterm2;
  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);
  double v   = 1./density;

  dterm1  = rgas[i]/(v-b0[i]+c0[i]);
  dterm2  = nrk[i]*afun/((v*(v+b0[i]))*temp);
  dpdT = dterm1+dterm2;

  return - RKEOS_dvdp(temp, density, i)* dpdT;
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_Cp_ideal_gas                               */
/*      Returns ideal gas specific heat given T               */
/*------------------------------------------------------------*/

double RKEOS_Cp_ideal_gas(double temp, int i)
{
 double cpi=(cp[0][i]+temp*(cp[1][i]+temp*(cp[2][i]+temp*(cp[3][i]
       +temp*cp[4][i]))));
 if (cpi<SMLL)
    cpi = 1.0;
 return cpi;
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_H_ideal_gas                                */
/*      Returns ideal gas specific enthalpy given T           */
/*------------------------------------------------------------*/

double RKEOS_H_ideal_gas(double temp, int i)
{
 double h = temp*(cp[0][i]+temp*(0.5*cp[1][i]+temp*(0.333333*cp[2][i]
       +temp*(0.25*cp[3][i]+temp*0.2*cp[4][i]))));
 if (h<SMLL)
    h = 1.0;
 return h;
}

/*-----------------------------------------------------------------*/

/* FUNCTION: RKEOS_vol_specific_heat                               */
/*      Returns constant volume specific heat given T and rho      */
/*-----------------------------------------------------------------*/

double RKEOS_vol_specific_heat(double temp, double density, int i)
{
  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);
  double v = 1./density;
  double Cv0  = RKEOS_Cp_ideal_gas(temp, i) - rgas[i];
  int np1 = (nrk[i]+1.)/b0[i];
  if (Cv0<SMLL)
     Cv0 = 1.;
  return Cv0 + nrk[i]*np1*afun*log(1.0+b0[i]/v)/temp;

}
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_specific_heat                              */
/*      Returns specific heat given T and rho                 */
/*------------------------------------------------------------*/

double RKEOS_specific_heat(double temp, double density, int i)
{
  double delta_Cp,press,v,dvdt,dadt;
  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);

  press = RKEOS_pressure(temp, density, i);
  v = 1./density;
  dvdt = RKEOS_dvdt(temp, density, i);
  dadt = -nrk[i]*afun/temp;
  delta_Cp = press*dvdt-rgas[i]-dadt*(1.+nrk[i])/b0[i]*log((v+b0[i])/v)
      + afun*(1.+nrk[i])*dvdt/(v*(v+b0[i]));

  return RKEOS_Cp_ideal_gas(temp, i)+delta_Cp; /* (J/Kg-K) */

}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_enthalpy                                   */
/*      Returns specific enthalpy given T and rho             */
/*------------------------------------------------------------*/

double RKEOS_enthalpy(double temp, double density, int i)
{
  double delta_h,press, v;

  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);

  press = RKEOS_pressure(temp, density, i);
  v = 1./density;
  delta_h = press*v-rgas[i]*temp-afun*(1+nrk[i])/b0[i]*log((v+b0[i])/v);

  return H_REF+RKEOS_H_ideal_gas(temp,i)+delta_h; /* (J/Kg) */
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_entropy                                    */
/*      Returns entropy given T and rho                       */
/*------------------------------------------------------------*/

double RKEOS_entropy(double temp, double density, int i)
{
  double delta_s,v,v0,dadt,cp_integral;

  double afun = a0[i]*pow(tcrit[i]/temp,nrk[i]);

  cp_integral = cp[0][i]*log(temp)+temp*(cp[1][i]+temp*(0.5*cp[2][i]
         +temp*(0.333333*cp[3][i]+0.25*cp[4][i]*temp)))
        - cp_int_ref[i];
  if (cp_integral<SMLL)
     cp_integral = 1.0;
  v = 1./density;


    v0 = rgas[i]*temp/ref_p;
  dadt = -nrk[i]*afun/temp;
  delta_s = rgas[i]*log((v-bb[i])/v0)+dadt/b0[i]*log((v+b0[i])/v);

  return S_REF+cp_integral+delta_s; /* (J/Kg-K) */
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_viscosity                                  */
/*------------------------------------------------------------*/

double RKEOS_viscosity(double temp, int i)
{
  double mu,tr,tc,pcatm;

  tr = temp/tcrit[i];
  tc = tcrit[i];
  pcatm = pcrit[i]/101325.;

  mu = 6.3e-7*sqrt(mw[i])*pow(pcatm,0.6666)/pow(tc,0.16666)
    *(pow(tr,1.5)/(tr+0.8));

  return mu;
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_thermal_conductivity                       */
/*------------------------------------------------------------*/

double RKEOS_thermal_conductivity(double temp,int i)
{
  double cp, mu;

  cp = RKEOS_Cp_ideal_gas(temp, i);
  mu = RKEOS_viscosity(temp, i);

  return (cp+1.25*rgas[i])*mu;
}

/*******************************************************************/
/* Mixture Functions Structure                                     */ 
/*******************************************************************/
UDF_EXPORT RGAS_Functions RealGasFunctionList = 
{
 MIXTURE_Setup,                    /* initialize            */
 MIXTURE_density,                  /* density               */
 MIXTURE_enthalpy,                 /* sensible enthalpy     */
 MIXTURE_entropy,                  /* entropy               */
 MIXTURE_specific_heat,            /* specific_heat         */
 MIXTURE_mw,                       /* molecular_weight      */
 MIXTURE_speed_of_sound,           /* speed_of_sound        */
 MIXTURE_viscosity,                /* viscosity             */
 MIXTURE_thermal_conductivity,     /* thermal_conductivity  */
 MIXTURE_rho_t,                    /* drho/dT |const p      */
 MIXTURE_rho_p,                    /* drho/dp |const T      */
 MIXTURE_enthalpy_t,               /* dh/dT |const p        */
 MIXTURE_enthalpy_p,               /* dh/dp |const T        */
 MIXTURE_enthalpy_prime            /* enthalpy              */
};