/**********************************************************************/
/* User Defined Real Gas Model :                                      */
/* For Peng-Robinson Equation of State                                    */
/* define/user-defined/real-gas-models/user-defined-real-gas-model    */
/**********************************************************************/

#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

#define MW 44.00995  // /* molec. wt. for single gas (Kg/Kmol) In our Case CO2 */***********************************************************************************
#define RGAS (UNIVERSAL_GAS_CONSTANT/MW)


//*********************************************************Peng-Robinson **************************************************************//
/* The variables below need to be set for a particular gas */

/* CO2 */

/* REAL GAS EQUATION OF STATE MODEL - BASIC VARIABLES */
/* ALL VARIABLES ARE IN SI UNITS! */

#define RGASU UNIVERSAL_GAS_CONSTANT
#define PI  3.141592654

#define MWT 44.00995
#define PCRIT 7374000
#define TCRIT 304.12
#define ZCRIT 0.2769
#define VCRIT 0.002137
#define NRK 0.77

/* IDEAL GAS SPECIFIC HEAT CURVE FIT */

#define CC1 840.37 //429.9289
#define CC2 0//1.874473
#define CC3 0//-0.001966485
#define CC4 0//1.297251e-06
#define CC5 0//-3.999956e-10

/* REFERENCE STATE */

#define P_REF 101325
#define T_REF 298.15

/* OPTIONAL REFERENCE (OFFSET) VALUES FOR ENTHALPY AND ENTROPY */

#define H_REF 0.0
#define S_REF 0.0

static double rgas, a0, b0, c0, bb, cp_int_ref;

// defined by  avedis
double RKEOS_spvol(double temp, double press);
double RKEOS_pressure(double temp, double density);
double RKEOS_Cp_ideal_gas(double temp);
double RKEOS_dvdt(double temp, double density);

//*********************************************************Peng-Robinson **************************************************************//


static int (*usersMessage)(const char *, ...);
static void (*usersError)(const char *, ...);


DEFINE_ON_DEMAND(I_do_nothing)
{
  /* This is a dummy function to allow us to use */
  /* the Compiled UDFs utility      */
}


void IDEAL_error(int err, char *f, char *msg)
{
  if (err)
    usersError("IDEAL_error (%d) from function: %s\n%s\n", err, f, msg);
}






// void IDEAL_Setup(Domain *domain, cxboolean vapor_phase, char *filename,
//                  int (*messagefunc)(const char *format, ...),
//                  void (*errorfunc)(const char *format, ...))
// {
//   /* Use this function for any initialization or model setups*/
//   usersMessage = messagefunc;
//   usersError  = errorfunc;
//   usersMessage("\nLoading Peng-Robinson Library: %s\n", filename);
// }
//*********************************************************Peng-Robinson **************************************************************//
void IDEAL_Setup(Domain *domain, cxboolean vapor_phase, char *species_list,
                 int(*messagefunc)(const char *format,...),
                 void (*errorfunc)(const char *format,...))
{

  rgas = RGASU/MWT;
  a0 = 0.42747*rgas*rgas*TCRIT*TCRIT/PCRIT;
  b0 = 0.08664*rgas*TCRIT/PCRIT;
  c0 = rgas*TCRIT/(PCRIT+a0/(VCRIT*(VCRIT+b0)))+b0-VCRIT;
  bb = b0-c0;
  cp_int_ref = CC1*log(T_REF)+T_REF*(CC2+
             T_REF*(0.5*CC3+T_REF*(0.333333*CC4+0.25*CC5*T_REF)));

  usersMessage = messagefunc;
  usersError = errorfunc;
  usersMessage("\nLoading Peng-Robinson Library: %s\n", species_list);
}
//*********************************************************Peng-Robinson **************************************************************//








// double IDEAL_density(cell_t cell, Thread *thread,
//                      cxboolean vapor_phase, double Temp, double press, double yi[])
// {
//   double r = press / (RGAS * Temp); /* Density at Temp & press */
//   return r;      /* (Kg/m^3) */
// }

//*********************************************************Peng-Robinson **************************************************************//
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_density                                    */
/*      Returns density given T and P                         */
/*------------------------------------------------------------*/

double IDEAL_density(cell_t cell, Thread *thread, cxboolean vapor_phase, double temp, double press, double yi[])
{
    return 1./RKEOS_spvol(temp, press); /* (Kg/m3) */
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_dvdp                                       */
/*      Returns dv/dp given T and rho                         */
/*------------------------------------------------------------*/

//*********************************************************Peng-Robinson **************************************************************//







// double IDEAL_specific_heat(cell_t cell, Thread *thread,
//                            double Temp, double density, double P, double yi[])
// {
//   double cp = 840.37;//CO2*******************************************************************************************************************
//   return cp;     /* (J/Kg/K) */
// }

//*********************************************************Peng-Robinson **************************************************************//
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_specific_heat                              */
/*      Returns specific heat given T and rho                 */
/*------------------------------------------------------------*/

double IDEAL_specific_heat(cell_t cell, Thread *thread, double temp, double density, double P, double yi[])
{
    double delta_Cp,press,v,dvdt,dadt;
    double afun = a0*pow(TCRIT/temp,NRK);

    press = RKEOS_pressure(temp, density);
    v = 1./density;
    dvdt = RKEOS_dvdt(temp, density);
    dadt = -NRK*afun/temp;
    delta_Cp = press*dvdt-rgas-dadt*(1.+NRK)/b0*log((v+b0)/v)
              + afun*(1.+NRK)*dvdt/(v*(v+b0));

    return RKEOS_Cp_ideal_gas(temp)+delta_Cp; /* (J/Kg-K) */
}
//*********************************************************Peng-Robinson **************************************************************//






double IDEAL_enthalpy(cell_t cell, Thread *thread,
                      double Temp, double density, double P, double yi[])
{
  double h = Temp * IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return h;      /* (J/Kg) */
}

#define TDatum 288.15
#define PDatum 1.01325e5

double IDEAL_entropy(cell_t cell, Thread *thread,
                     double Temp, double density, double P, double yi[])
{
  double s = IDEAL_specific_heat(cell, thread, Temp, density, P, yi) * log(fabs(Temp / TDatum)) +
             RGAS * log(fabs(PDatum / P));

  // return s;      /* (J/Kg/K) */
  printf("IDEAL_entropy: %g\n", s);
  Message0("\n IDEAL_entropy= %d \n",s);
  return 213720.2;//Iwas testing something
}


double IDEAL_mw(double yi[])
{
  return MW;     /* (Kg/Kmol) */
}

double IDEAL_speed_of_sound(cell_t cell, Thread *thread,
                            double Temp, double density, double P, double yi[])
{
  double cp = IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return sqrt(Temp * cp * RGAS / (cp - RGAS)); /* m/s */
}

double IDEAL_viscosity(cell_t cell, Thread *thread,
                       double Temp, double density, double P, double yi[])
{
  double mu = 1.37e-05;//Methane CH4 *******************************************************************************************************
  return mu;     /* (Kg/m/s) */
}

double IDEAL_thermal_conductivity(cell_t cell, Thread *thread,
                                  double Temp, double density, double P,
                                  double yi[])
{
  double ktc = 0.0145; //Methane CH4 ************************************************************************************************
  return ktc;      /* W/m/K */
}
double IDEAL_rho_t(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. Temp at constant p */
  double rho_t = -density / Temp;
  return rho_t;     /* (Kg/m^3/K) */
}

double IDEAL_rho_p(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. pressure at constant T */
  double rho_p = 1.0 / (RGAS * Temp);
  return rho_p;    /* (Kg/m^3/Pa) */
}

double IDEAL_enthalpy_t(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. Temp at constant p */
  return IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
}

double IDEAL_enthalpy_p(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. pressure at constant T  */
  /* general form dh/dp|T = (1/rho)*[ 1 + (T/rho)*drho/dT|p] */
  /* but for ideal gas dh/dp = 0        */
  return 0.0 ;
}

UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
  IDEAL_Setup,                   /* initialize */
  IDEAL_density,                 /* density */
  IDEAL_enthalpy,                /* enthalpy */
  IDEAL_entropy,                 /* entropy */
  IDEAL_specific_heat,           /* specific_heat */
  IDEAL_mw,                      /* molecular_weight */
  IDEAL_speed_of_sound,          /* speed_of_sound */
  IDEAL_viscosity,               /* viscosity */
  IDEAL_thermal_conductivity,    /* thermal_conductivity */
  IDEAL_rho_t,                   /* drho/dT |const p */
  IDEAL_rho_p,                   /* drho/dp |const T */
  IDEAL_enthalpy_t,              /* dh/dT |const p  */
  IDEAL_enthalpy_p               /* dh/dp |const T  */
};
/**************************************************************/




//*********************************************************Peng-Robinson **************************************************************//
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_spvol                                      */
/*      Returns specific volume given T and P                 */
/*------------------------------------------------------------*/
double RKEOS_spvol(double temp, double press)
{
    double a1,a2,a3;
    double vv,vv1,vv2,vv3;
    double qq,qq3,sqq,rr,tt,dd;
    double afun = a0*pow(TCRIT/temp,NRK);

    a1 = c0-rgas*temp/press;
    a2 = -(bb*b0+rgas*temp*b0/press-afun/press);
    a3 = -afun*bb/press;

    /* Solve cubic equation for specific volume */

    qq = (a1*a1-3.*a2)/9.;
    rr = (2*a1*a1*a1-9.*a1*a2+27.*a3)/54.;
    qq3 = qq*qq*qq;
    dd = qq3-rr*rr;

  /* If dd < 0, then we have one real root */
  /* If dd >= 0, then we have three roots -> choose largest root */

  if (dd < 0.) {
     tt = pow(sqrt(-dd)+fabs(rr),0.333333);
     vv = (tt+qq/tt)-a1/3.;
  } else {
     tt = acos(rr/sqrt(qq3));
     sqq = sqrt(qq);
     vv1 = -2.*sqq*cos(tt/3.)-a1/3.;
     vv2 = -2.*sqq*cos((tt+2.*PI)/3.)-a1/3.;
     vv3 = -2.*sqq*cos((tt+4.*PI)/3.)-a1/3.;
     vv = (vv1 > vv2) ? vv1 : vv2;
     vv = (vv > vv3) ? vv : vv3;
  }

  return vv;
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_pressure                                   */
/*      Returns pressure given T and density                  */
/*------------------------------------------------------------*/

double RKEOS_pressure(double temp, double density)
{
    double v = 1./density;
    double afun = a0*pow(TCRIT/temp,NRK);
    return rgas*temp/(v-bb)-afun/(v*(v+b0));
}

/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_Cp_ideal_gas                               */
/*      Returns ideal gas specific heat given T               */
/*------------------------------------------------------------*/

double RKEOS_Cp_ideal_gas(double temp)
{
    return (CC1+temp*(CC2+temp*(CC3+temp*(CC4+temp*CC5))));
}


/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_dvdt                                       */
/*      Returns dv/dT given T and rho                         */
/*------------------------------------------------------------*/

double RKEOS_dvdt(double temp, double density)
{
    double a1,a2,dadt,a1t,a2t,a3t,v,press;
    double afun = a0*pow(TCRIT/temp,NRK);

    press = RKEOS_pressure(temp, density);
    v = 1./density;

    dadt = -NRK*afun/temp;
    a1 = c0-rgas*temp/press;
    a2 = -(bb*b0+rgas*temp*b0/press-afun/press);
    a1t = -rgas/press;
    a2t = a1t*b0+dadt/press;
    a3t = -dadt*bb/press;

    return -(a3t+v*(a2t+v*a1t))/(a2+v*(2.*a1+3.*v));
}
//*********************************************************Peng-Robinson **************************************************************//