/**********************************************************************/
/* User Defined Real Gas Model :                                      */
/* For Ideal Gas Equation of State                                    */
/* define/user-defined/real-gas-models/user-defined-real-gas-model    */
/**********************************************************************/

#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

#define MW 16.04303  // /* molec. wt. for single gas (Kg/Kmol) In our Case Methane CH4 */***********************************************************************************
#define RGAS (UNIVERSAL_GAS_CONSTANT/MW)

static int (*usersMessage)(const char *, ...);
static void (*usersError)(const char *, ...);



//*********************************************************Peng-Robinson **************************************************************//
/* The variables below need to be set for a particular gas */

/* CH4 */

/* REAL GAS EQUATION OF STATE MODEL - BASIC VARIABLES */
/* ALL VARIABLES ARE IN SI UNITS! */

#define RGASU UNIVERSAL_GAS_CONSTANT
#define PI  3.141592654

#define PCRIT 4599000
#define TCRIT 190.56

/* REFERENCE STATE */

// #define P_REF 101325
// #define T_REF 298.15

/* OPTIONAL REFERENCE (OFFSET) VALUES FOR ENTHALPY AND ENTROPY */

#define H_REF 0.0
#define S_REF 0.0

static double rgas, a0, b0, c0, bb, cp_int_ref;

// defined by  avedis
double RKEOS_spvol(double temp, double press);
double RKEOS_pressure(double temp, double density);
double RKEOS_Cp_ideal_gas(double temp);
double RKEOS_dvdt(double temp, double density);
double w_i(int i);


double Cp_i(double T, double r);//GERG only Methane



//*********************************************************Peng-Robinson **************************************************************//




DEFINE_ON_DEMAND(I_do_nothing)
{
  /* This is a dummy function to allow us to use */
  /* the Compiled UDFs utility      */
}


void IDEAL_error(int err, char *f, char *msg)
{
  if (err)
    usersError("IDEAL_error (%d) from function: %s\n%s\n", err, f, msg);
}






//*********************************************************Peng-Robinson **************************************************************//
void PENG_ROBINSON_Setup(Domain *domain, cxboolean vapor_phase, char *species_list,
                 int(*messagefunc)(const char *format,...),
                 void (*errorfunc)(const char *format,...))
{
  rgas = RGASU/MW; //Constant
  a0 = 0.457247*rgas*rgas*TCRIT*TCRIT/PCRIT; //Constant
  b0 = 0.0778*rgas*TCRIT/PCRIT; //Constant

  usersMessage = messagefunc;
  usersError = errorfunc;
  usersMessage("\nLoading Peng-Robinson Library: %s\n", species_list);
}
//*********************************************************Peng-Robinson **************************************************************//





//*********************************************************Peng-Robinson **************************************************************//
double PENG_ROBINSON_density(cell_t cell, Thread *thread,
                     cxboolean vapor_phase, double Temp, double press, double yi[])
{
  double density = 1/RKEOS_spvol(Temp, press); /* (Kg/m3) */
  // Message0("Temp %f .\n",Temp);
  // Message0("press %f .\n",press);
  // Message0("density %f .\n",density);
  
  return density;
}
//*********************************************************Peng-Robinson **************************************************************//






double IDEAL_specific_heat(cell_t cell, Thread *thread,
                           double Temp, double density, double P, double yi[])
{
  // double cp = 2222;//Methane CH4 
  
  return Cp_i(density, Temp);     /* (J/Kg/K) */ //GERG_MEthane
}

double IDEAL_enthalpy(cell_t cell, Thread *thread,
                      double Temp, double density, double P, double yi[])
{
  double h = Temp * IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return h;      /* (J/Kg) */
}

#define TDatum 288.15
#define PDatum 1.01325e5

double IDEAL_entropy(cell_t cell, Thread *thread,
                     double Temp, double density, double P, double yi[])
{
  double s = IDEAL_specific_heat(cell, thread, Temp, density, P, yi) * log(fabs(Temp / TDatum)) +
             RGAS * log(fabs(PDatum / P));

  return s;      /* (J/Kg/K) */
  printf("IDEAL_entropy: %g\n", s);
  //return 186040.1;//Iwas testing something
}


double IDEAL_mw(double yi[])
{
  return MW;     /* (Kg/Kmol) */
}

double IDEAL_speed_of_sound(cell_t cell, Thread *thread,
                            double Temp, double density, double P, double yi[])
{
  double cp = IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return sqrt(Temp * cp * RGAS / (cp - RGAS)); /* m/s */
}

double IDEAL_viscosity(cell_t cell, Thread *thread,
                       double Temp, double density, double P, double yi[])
{
  double mu = 1.087e-05;//Methane CH4 
  return mu;     /* (Kg/m/s) */
}

double IDEAL_thermal_conductivity(cell_t cell, Thread *thread,
                                  double Temp, double density, double P,
                                  double yi[])
{
  double ktc = 0.0332; //Methane CH4 
  return ktc;      /* W/m/K */
}
double IDEAL_rho_t(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. Temp at constant p */
  double rho_t = -density / Temp;
  return rho_t;     /* (Kg/m^3/K) */
}

double IDEAL_rho_p(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. pressure at constant T */
  double rho_p = 1.0 / (RGAS * Temp);
  return rho_p;    /* (Kg/m^3/Pa) */
}

double IDEAL_enthalpy_t(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. Temp at constant p */
  return IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
}

double IDEAL_enthalpy_p(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. pressure at constant T  */
  /* general form dh/dp|T = (1/rho)*[ 1 + (T/rho)*drho/dT|p] */
  /* but for ideal gas dh/dp = 0        */
  return 0.0 ;
}

UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
  PENG_ROBINSON_Setup,           /* initialize */
  PENG_ROBINSON_density,         /* density */
  IDEAL_enthalpy,                /* enthalpy */
  IDEAL_entropy,                 /* entropy */
  IDEAL_specific_heat,           /* specific_heat */
  IDEAL_mw,                      /* molecular_weight */
  IDEAL_speed_of_sound,          /* speed_of_sound */
  IDEAL_viscosity,               /* viscosity */
  IDEAL_thermal_conductivity,    /* thermal_conductivity */
  IDEAL_rho_t,                   /* drho/dT |const p */
  IDEAL_rho_p,                   /* drho/dp |const T */
  IDEAL_enthalpy_t,              /* dh/dT |const p  */
  IDEAL_enthalpy_p               /* dh/dp |const T  */
};
/**************************************************************/






//*********************************************************Peng-Robinson **************************************************************//
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_spvol                                      */
/*      Returns specific volume given T and P                 */
/*------------------------------------------------------------*/
double RKEOS_spvol(double T, double p)
{
  double n = 0.37464+1.54226*w_i(0)-0.26992*w_i(0)*w_i(0); //Constant

  //the following results for v is from Matlab with the help of syms, solve tools and coder tools
  double a_tmp;
  double a_tmp_tmp;
  double b_a_tmp;
  double b_a_tmp_tmp;
  double c_a_tmp;
  double c_a_tmp_tmp;
  double d_a_tmp;
  double e_a_tmp;
  double f_a_tmp;
  double g_a_tmp;
  double h_a_tmp;
  double i_a_tmp;
  double j_a_tmp;
  double valve;

  a_tmp = T * TCRIT;
  a_tmp_tmp = TCRIT * a0;
  b_a_tmp = a_tmp_tmp * b0;
  c_a_tmp = n * n;
  b_a_tmp_tmp = 2.0 * TCRIT * a0;
  d_a_tmp = b_a_tmp_tmp * b0;
  e_a_tmp = d_a_tmp * n;

  f_a_tmp = sqrt(T / TCRIT);
  f_a_tmp = (f_a_tmp > 0) ? f_a_tmp : 0;

  g_a_tmp = T * a0;
  h_a_tmp = b0 * b0;
  i_a_tmp = b_a_tmp_tmp * n;
  j_a_tmp = a_tmp * rgas - TCRIT * b0 * p;
  a_tmp_tmp = ((((((a_tmp_tmp + i_a_tmp) + g_a_tmp * c_a_tmp) + a_tmp_tmp * c_a_tmp) - 3.0 * TCRIT * h_a_tmp * p) - i_a_tmp * f_a_tmp) - b_a_tmp_tmp * c_a_tmp * f_a_tmp) - 2.0 * T * TCRIT * b0 * rgas;
  b_a_tmp_tmp = TCRIT * TCRIT;
  i_a_tmp = p * p;
  c_a_tmp_tmp = pow(j_a_tmp, 3.0) / (27.0 * pow(TCRIT, 3.0) * pow(p, 3.0));
  b_a_tmp = (((((((b_a_tmp - TCRIT * pow(b0, 3.0) * p) - a_tmp * h_a_tmp * rgas) + g_a_tmp * b0 * c_a_tmp) + b_a_tmp * c_a_tmp) + e_a_tmp) - d_a_tmp * c_a_tmp * f_a_tmp) - e_a_tmp * f_a_tmp) /(2.0 * TCRIT * p);
  c_a_tmp = j_a_tmp * a_tmp_tmp / (6.0 * b_a_tmp_tmp * i_a_tmp);
  a_tmp = (c_a_tmp_tmp + b_a_tmp) - c_a_tmp;
  d_a_tmp = 3.0 * TCRIT * p;
  i_a_tmp = a_tmp_tmp / d_a_tmp - j_a_tmp * j_a_tmp / (9.0 * b_a_tmp_tmp * i_a_tmp);

  valve = sqrt(a_tmp * a_tmp + pow(i_a_tmp, 3.0));
  valve = (valve > 0) ? valve : 0;

  b_a_tmp = pow(((valve + c_a_tmp_tmp) + b_a_tmp) - c_a_tmp,0.33333333333333331);

  double v = (b_a_tmp - i_a_tmp / b_a_tmp) + j_a_tmp / d_a_tmp;

  return v;

}

/*------------------------------------------------------------*/
/* FUNCTION: w_i                                              */
/*      Returns the Acentric factor of a Spiecie              */
/*------------------------------------------------------------*/
double w_i(int i)
{
  double wi[0];
  wi[0] =0.011;

  return wi[i];
}





//GERG for only METHANE

double Cp_i(double rho, double T)

{

    double n_o_oi[7] = { 19.597508817, -83.959667892, 3.00088, 0.76315, 0.00460, 8.74432 , -4.46921 };

    double theta_o_oi[7] = { 0, 0, 0, 4.306474465, 0.936220902, 5.577233895, 5.722644361 };

    
    double n_oi[24] = { 0.57335704239162,
    -0.16760687523730 * 10,
    0.23405291834916,
    -0.21947376343441,
    0.16369201404128 / 10,
    0.15004406389280 / 10,
    0.98990489492918 / 10,
    0.58382770929055,
    -0.74786867560390,
    0.30033302857974,
    0.20985543806568,
    -0.18590151133061 / 10,
    -0.15782558339049,
    0.12716735220791,
    -0.32019743894346 / 10,
    -0.68049729364536 / 10,
    0.24291412853736 / 10,
    0.51440451639444 / 100,
    -0.19084949733532 / 10,
    0.55229677241291 / 100,
    -0.44197392976085 / 100,
    0.40061416708429 / 10,
    -0.33752085907575 / 10,
    -0.25127658213357/100,
    };

    

    double c_oi[24] = { 0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,3,6,6,6,6 };

    double d_oi[24] = { 1,1,2,2,4,4,1,1,1,2,3,6,2,3,3,4,4,2,3,4,5,6,6,7 };

    double t_oi[24] = { 0.125, 1.125 , 0.375 ,1.125 ,0.625 ,1.500 ,0.625 ,2.625 ,2.750 ,2.125 ,2.000 ,1.750 ,4.500 ,4.750 ,5.000 ,4.000 ,4.500 ,7.500 ,14.000 ,11.500 ,26.000 ,28.000 ,30.000 ,16.000 };

    double K_Pol_i = 6;

    double K_Exp_i = 18;

    double R = 8.314472; // J/(mol*K)

    double R_star = 8.314510; // J/(mol*K)

    double T_ci = 190.564;

    double M_ci = 16.04246;

    double rho_ci = 162.66;

    double delta = rho/rho_ci;

    double tau = T_ci/T;


    double a_r_dd = 0;

    for (int k = 0; k < K_Pol_i; k++)
    {
        a_r_dd = a_r_dd + (n_oi[k] * d_oi[k] * (d_oi[k] - 1) * pow(delta, d_oi[k] - 2) * pow(tau, t_oi[k]));
    }

    for (int k = K_Pol_i; k < (K_Pol_i + K_Exp_i); k++)
    {
        a_r_dd = a_r_dd + (n_oi[k] * pow(delta, d_oi[k] - 2) * ((d_oi[k] - c_oi[k] * pow(delta, c_oi[k])) * (d_oi[k] - 1 - c_oi[k] * pow(delta, c_oi[k])) - pow(c_oi[k], 2) * pow(delta, c_oi[k])) * pow(tau, t_oi[k]) * exp(-pow(delta, c_oi[k])));

    }


    double a_r_d = 0;

    for (int k = 0; k < K_Pol_i; k++)
    {
        a_r_d = a_r_d + (n_oi[k] * d_oi[k] * pow(delta, d_oi[k] - 1) * pow(tau, t_oi[k]));
    }

    for (int k = K_Pol_i; k < (K_Pol_i + K_Exp_i); k++)
    {
        a_r_d = a_r_d + ( n_oi[k] * pow(delta, d_oi[k] - 1) * (d_oi[k] - c_oi[k] * pow(delta, c_oi[k])) * pow(tau, t_oi[k]) * exp(-pow(delta, c_oi[k])));

    }


    double a_r_dt = 0;

    for (int k = 0; k < K_Pol_i; k++)
    {
        a_r_dt = a_r_dt + (n_oi[k] * d_oi[k] * t_oi[k] * pow(delta, d_oi[k] - 1) * pow(tau, t_oi[k] - 1));
    }

    for (int k = K_Pol_i; k < (K_Pol_i + K_Exp_i); k++)
    {
        a_r_dt = a_r_dt + (n_oi[k] * t_oi[k] * pow(delta, d_oi[k] - 1) * (d_oi[k] - c_oi[k] * pow(delta, c_oi[k])) * pow(tau, t_oi[k] - 1) * exp(-pow(delta, c_oi[k])));

    }


    double a_r_tt = 0;

    for (int k = 0; k < K_Pol_i; k++)
    {
        a_r_tt = a_r_tt + (n_oi[k] * t_oi[k] * (t_oi[k] - 1) * pow(delta, d_oi[k]) * pow(tau, t_oi[k] - 2));
    }

    for (int k = K_Pol_i; k < (K_Pol_i + K_Exp_i); k++)
    {
        a_r_tt = a_r_tt + (n_oi[k] * t_oi[k] * (t_oi[k] - 1) * pow(delta, d_oi[k]) * pow(tau, t_oi[k] - 2) * exp(-pow(delta, c_oi[k])));

    }


    double a_o_tt_1 = 0;
    double a_o_tt_2 = 0;

    for (int k = 3; k < 7; k++)
    {
        if (k == 3 || k == 5) {

            a_o_tt_1 = a_o_tt_1 + (n_o_oi[k] * pow(theta_o_oi[k], 2) / pow(sinh(theta_o_oi[k] * T_ci / T), 2));


        }

        else {

            a_o_tt_2 = a_o_tt_2 + (n_o_oi[k] * pow(theta_o_oi[k], 2) / pow(cosh(theta_o_oi[k] * T_ci / T), 2));

        } 

    }

    double a_o_tt = R_star / R * (-n_o_oi[2] * pow(T / T_ci, 2) - a_o_tt_1 - a_o_tt_2);



   double cpi = R * (-pow(tau, 2) * (a_o_tt + a_r_tt) + pow(1 + delta * a_r_d - delta * tau * a_r_dt, 2) / (1 + 2 * delta * a_r_d + pow(delta, 2)*a_r_dd));

   cpi = cpi / 16.04246 * 1000;
   

   return cpi;

}
