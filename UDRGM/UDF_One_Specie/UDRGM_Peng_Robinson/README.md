# UDRGM for One Specie (one Material) 

UDRGM for one sample Fluid material will be elaborated here and compared to the fluent version.\
The following code can be used but it serves as a **Template**.\
It should be noted same Physical Properties of the UDRG should be used in Fluent UI in order to receive similar results.\
Once similarity in the output is produced and you feel confortable with the template code you may start to costumize your own  physical properties in the UDRGM. :rocket:

## User Defined Physical properties Used for our sample code

| Properties    | Method or Value |
| ------------- | ------------- |
| **Material:**  | Methane CH4  |
| **Equation of State for Density:**  |Peng And Robinson |
| **Material:**  | Methane CH4  |
| **Cp:**  | 2222 (J/Kg/K)  |
| **speed_of_sound:**  | Ideal Gas SoS |
| **viscosity:**  | 1.087e-05 (Kg/m/s)  |
|**thermal_conductivity**  | 0.0332 (W/m/K)  |


## Returned Physical properties to Fluent
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow.

### Loading the code using TUI
```
define/user-defined/real-gas-models/user-defined-real-gas-model
```


### Note 
It should be noted that Peng and Robinson code was generated in an implicit manner for density using the great tools of Matlab which are Syms, Solve and Coder C generator.

### UDRGM Vs Fluent version comparision

#### Peng and Robinson Equation of State comparision

The following post processed data shows the difference between Fluent Peng and Robinson and the written UDRG Peng and Robinson.\
As seen the difference is not above 0.34% for a channel flow involving a hight operating pressure and a hight heated wall Temperature gradient.\
You final post proccessd result difference might differe from a case to another but nevertheless the difference resulted should be small.

<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Peng_Robinson_Methane_Density.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Peng_Robinson_Methane_Pressure.png" width="300" height="300">
<img src="Images/UDRGM_Images/UDRGM_VS_Fluent_Peng_Robinson_Methane_Temperature.png" width="300" height="300">


Macros used in CFD-Post to generate those differences.\
abs(100*(Density.Difference)/Density).\
abs(100*(Absolute Pressure.Difference)/(Absolute Pressure)).\
abs(100*(Temperature.Difference)/(Temperature)).