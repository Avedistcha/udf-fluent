/**********************************************************************/
/* User Defined Real Gas Model :                                      */
/* For Ideal Gas Equation of State                                    */
/* define/user-defined/real-gas-models/user-defined-real-gas-model    */
/**********************************************************************/

#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

#define MW 16.04303  // /* molec. wt. for single gas (Kg/Kmol) In our Case Methane CH4 */***********************************************************************************
#define RGAS (UNIVERSAL_GAS_CONSTANT/MW)

static int (*usersMessage)(const char *, ...);
static void (*usersError)(const char *, ...);



//*********************************************************Peng-Robinson **************************************************************//
/* The variables below need to be set for a particular gas */

/* CH4 */

/* REAL GAS EQUATION OF STATE MODEL - BASIC VARIABLES */
/* ALL VARIABLES ARE IN SI UNITS! */

#define RGASU UNIVERSAL_GAS_CONSTANT
#define PI  3.141592654

#define PCRIT 4599000
#define TCRIT 190.56

/* REFERENCE STATE */

// #define P_REF 101325
// #define T_REF 298.15

/* OPTIONAL REFERENCE (OFFSET) VALUES FOR ENTHALPY AND ENTROPY */

#define H_REF 0.0
#define S_REF 0.0

static double rgas, a0, b0, c0, bb, cp_int_ref;

// defined by  avedis
double RKEOS_spvol(double temp, double press);
double RKEOS_pressure(double temp, double density);
double RKEOS_Cp_ideal_gas(double temp);
double RKEOS_dvdt(double temp, double density);
double w_i(int i);
//*********************************************************Peng-Robinson **************************************************************//




DEFINE_ON_DEMAND(I_do_nothing)
{
  /* This is a dummy function to allow us to use */
  /* the Compiled UDFs utility      */
}


void IDEAL_error(int err, char *f, char *msg)
{
  if (err)
    usersError("IDEAL_error (%d) from function: %s\n%s\n", err, f, msg);
}






//*********************************************************Peng-Robinson **************************************************************//
void PENG_ROBINSON_Setup(Domain *domain, cxboolean vapor_phase, char *species_list,
                 int(*messagefunc)(const char *format,...),
                 void (*errorfunc)(const char *format,...))
{
  rgas = RGASU/MW; //Constant
  a0 = 0.457247*rgas*rgas*TCRIT*TCRIT/PCRIT; //Constant
  b0 = 0.0778*rgas*TCRIT/PCRIT; //Constant

  usersMessage = messagefunc;
  usersError = errorfunc;
  usersMessage("\nLoading Peng-Robinson Library: %s\n", species_list);
}
//*********************************************************Peng-Robinson **************************************************************//





//*********************************************************Peng-Robinson **************************************************************//
double PENG_ROBINSON_density(cell_t cell, Thread *thread,
                     cxboolean vapor_phase, double Temp, double press, double yi[])
{
  double density = 1/RKEOS_spvol(Temp, press); /* (Kg/m3) */
  // Message0("Temp %f .\n",Temp);
  // Message0("press %f .\n",press);
  // Message0("density %f .\n",density);
  
  return density;
}
//*********************************************************Peng-Robinson **************************************************************//






double IDEAL_specific_heat(cell_t cell, Thread *thread,
                           double Temp, double density, double P, double yi[])
{
  double cp = 2222;//Methane CH4 
  return cp;     /* (J/Kg/K) */
}

double IDEAL_enthalpy(cell_t cell, Thread *thread,
                      double Temp, double density, double P, double yi[])
{
  double h = Temp * IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return h;      /* (J/Kg) */
}

#define TDatum 288.15
#define PDatum 1.01325e5

double IDEAL_entropy(cell_t cell, Thread *thread,
                     double Temp, double density, double P, double yi[])
{
  double s = IDEAL_specific_heat(cell, thread, Temp, density, P, yi) * log(fabs(Temp / TDatum)) +
             RGAS * log(fabs(PDatum / P));

  return s;      /* (J/Kg/K) */
  printf("IDEAL_entropy: %g\n", s);
  //return 186040.1;//Iwas testing something
}


double IDEAL_mw(double yi[])
{
  return MW;     /* (Kg/Kmol) */
}

double IDEAL_speed_of_sound(cell_t cell, Thread *thread,
                            double Temp, double density, double P, double yi[])
{
  double cp = IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return sqrt(Temp * cp * RGAS / (cp - RGAS)); /* m/s */
}

double IDEAL_viscosity(cell_t cell, Thread *thread,
                       double Temp, double density, double P, double yi[])
{
  double mu = 1.087e-05;//Methane CH4 
  return mu;     /* (Kg/m/s) */
}

double IDEAL_thermal_conductivity(cell_t cell, Thread *thread,
                                  double Temp, double density, double P,
                                  double yi[])
{
  double ktc = 0.0332; //Methane CH4 
  return ktc;      /* W/m/K */
}
double IDEAL_rho_t(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. Temp at constant p */
  double rho_t = -density / Temp;
  return rho_t;     /* (Kg/m^3/K) */
}

double IDEAL_rho_p(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. pressure at constant T */
  double rho_p = 1.0 / (RGAS * Temp);
  return rho_p;    /* (Kg/m^3/Pa) */
}

double IDEAL_enthalpy_t(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. Temp at constant p */
  return IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
}

double IDEAL_enthalpy_p(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. pressure at constant T  */
  /* general form dh/dp|T = (1/rho)*[ 1 + (T/rho)*drho/dT|p] */
  /* but for ideal gas dh/dp = 0        */
  return 0.0 ;
}

UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
  PENG_ROBINSON_Setup,           /* initialize */
  PENG_ROBINSON_density,         /* density */
  IDEAL_enthalpy,                /* enthalpy */
  IDEAL_entropy,                 /* entropy */
  IDEAL_specific_heat,           /* specific_heat */
  IDEAL_mw,                      /* molecular_weight */
  IDEAL_speed_of_sound,          /* speed_of_sound */
  IDEAL_viscosity,               /* viscosity */
  IDEAL_thermal_conductivity,    /* thermal_conductivity */
  IDEAL_rho_t,                   /* drho/dT |const p */
  IDEAL_rho_p,                   /* drho/dp |const T */
  IDEAL_enthalpy_t,              /* dh/dT |const p  */
  IDEAL_enthalpy_p               /* dh/dp |const T  */
};
/**************************************************************/






//*********************************************************Peng-Robinson **************************************************************//
/*------------------------------------------------------------*/
/* FUNCTION: RKEOS_spvol                                      */
/*      Returns specific volume given T and P                 */
/*------------------------------------------------------------*/
double RKEOS_spvol(double T, double p)
{
  double n = 0.37464+1.54226*w_i(0)-0.26992*w_i(0)*w_i(0); //Constant

  //the following results for v is from Matlab with the help of syms, solve tools and coder tools
  double a_tmp;
  double a_tmp_tmp;
  double b_a_tmp;
  double b_a_tmp_tmp;
  double c_a_tmp;
  double c_a_tmp_tmp;
  double d_a_tmp;
  double e_a_tmp;
  double f_a_tmp;
  double g_a_tmp;
  double h_a_tmp;
  double i_a_tmp;
  double j_a_tmp;
  double valve;

  a_tmp = T * TCRIT;
  a_tmp_tmp = TCRIT * a0;
  b_a_tmp = a_tmp_tmp * b0;
  c_a_tmp = n * n;
  b_a_tmp_tmp = 2.0 * TCRIT * a0;
  d_a_tmp = b_a_tmp_tmp * b0;
  e_a_tmp = d_a_tmp * n;

  f_a_tmp = sqrt(T / TCRIT);
  f_a_tmp = (f_a_tmp > 0) ? f_a_tmp : 0;

  g_a_tmp = T * a0;
  h_a_tmp = b0 * b0;
  i_a_tmp = b_a_tmp_tmp * n;
  j_a_tmp = a_tmp * rgas - TCRIT * b0 * p;
  a_tmp_tmp = ((((((a_tmp_tmp + i_a_tmp) + g_a_tmp * c_a_tmp) + a_tmp_tmp * c_a_tmp) - 3.0 * TCRIT * h_a_tmp * p) - i_a_tmp * f_a_tmp) - b_a_tmp_tmp * c_a_tmp * f_a_tmp) - 2.0 * T * TCRIT * b0 * rgas;
  b_a_tmp_tmp = TCRIT * TCRIT;
  i_a_tmp = p * p;
  c_a_tmp_tmp = pow(j_a_tmp, 3.0) / (27.0 * pow(TCRIT, 3.0) * pow(p, 3.0));
  b_a_tmp = (((((((b_a_tmp - TCRIT * pow(b0, 3.0) * p) - a_tmp * h_a_tmp * rgas) + g_a_tmp * b0 * c_a_tmp) + b_a_tmp * c_a_tmp) + e_a_tmp) - d_a_tmp * c_a_tmp * f_a_tmp) - e_a_tmp * f_a_tmp) /(2.0 * TCRIT * p);
  c_a_tmp = j_a_tmp * a_tmp_tmp / (6.0 * b_a_tmp_tmp * i_a_tmp);
  a_tmp = (c_a_tmp_tmp + b_a_tmp) - c_a_tmp;
  d_a_tmp = 3.0 * TCRIT * p;
  i_a_tmp = a_tmp_tmp / d_a_tmp - j_a_tmp * j_a_tmp / (9.0 * b_a_tmp_tmp * i_a_tmp);

  valve = sqrt(a_tmp * a_tmp + pow(i_a_tmp, 3.0));
  valve = (valve > 0) ? valve : 0;

  b_a_tmp = pow(((valve + c_a_tmp_tmp) + b_a_tmp) - c_a_tmp,0.33333333333333331);

  double v = (b_a_tmp - i_a_tmp / b_a_tmp) + j_a_tmp / d_a_tmp;

  return v;

}

/*------------------------------------------------------------*/
/* FUNCTION: w_i                                              */
/*      Returns the Acentric factor of a Spiecie              */
/*------------------------------------------------------------*/
double w_i(int i)
{
  double wi[0];
  wi[0] =0.011;

  return wi[i];
}