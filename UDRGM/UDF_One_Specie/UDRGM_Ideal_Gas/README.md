# UDRGM for One Specie (one Material) 

UDRGM for one sample Fluid material will be elaborated here and compared to the fluent version.\
The following code can be used but it serves as a **Template**.\
It should be noted same Physical Properties of the UDRG should be used in Fluent UI in order to receive similar results.\
Once similarity in the output is produced and you feel confortable with the template code you may start to costumize your own  physical properties in the UDRGM. :rocket:

## User Defined Physical properties Used for our sample code

| Properties    | Method or Value |
| ------------- | ------------- |
| **Material:**  | Methane CH4  |
| **Equation of State:**  |Ideal Gas |
| **Material:**  | Methane CH4  |
| **Cp:**  | 2222 (J/Kg/K)  |
| **speed_of_sound:**  | Ideal Gas SoS |
| **viscosity:**  | 1.087e-05 (Kg/m/s)  |
|**thermal_conductivity**  | 0.0332 (W/m/K)  |


## Returned Physical properties to Fluent
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow.

### Loading the code using TUI
```
define/user-defined/real-gas-models/user-defined-real-gas-model
```

<img src="Images/rocket-launch.svg" width="300" height="300">
