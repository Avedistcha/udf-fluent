/**********************************************************************/
/* User Defined Real Gas Model :                                      */
/* For Ideal Gas Equation of State                                    */
/* define/user-defined/real-gas-models/user-defined-real-gas-model    */
/**********************************************************************/

#include "udf.h"
#include "stdio.h"
#include "ctype.h"
#include "stdarg.h"

#define MW 16.04303  // /* molec. wt. for single gas (Kg/Kmol) In our Case Methane CH4 */***********************************************************************************
#define RGAS (UNIVERSAL_GAS_CONSTANT/MW)

static int (*usersMessage)(const char *, ...);
static void (*usersError)(const char *, ...);


DEFINE_ON_DEMAND(I_do_nothing)
{
  /* This is a dummy function to allow us to use */
  /* the Compiled UDFs utility      */
}


void IDEAL_error(int err, char *f, char *msg)
{
  if (err)
    usersError("IDEAL_error (%d) from function: %s\n%s\n", err, f, msg);
}

void IDEAL_Setup(Domain *domain, cxboolean vapor_phase, char *filename,
                 int (*messagefunc)(const char *format, ...),
                 void (*errorfunc)(const char *format, ...))
{
  /* Use this function for any initialization or model setups*/
  usersMessage = messagefunc;
  usersError  = errorfunc;
  usersMessage("\nLoading Real-Ideal Library: %s\n", filename);
}

double IDEAL_density(cell_t cell, Thread *thread,
                     cxboolean vapor_phase, double Temp, double press, double yi[])
{
  double r = press / (RGAS * Temp); /* Density at Temp & press */
  Message0("r     %f   .\n",r);
  Message0("press %f   .\n",press);
  Message0("RGAS  %f   .\n",RGAS);
  Message0("Temp  %f   .\n",Temp);
  return r;      /* (Kg/m^3) */
}

double IDEAL_specific_heat(cell_t cell, Thread *thread,
                           double Temp, double density, double P, double yi[])
{
  double cp = 2222;//Methane CH4 *******************************************************************************************************************
  return cp;     /* (J/Kg/K) */
}

double IDEAL_enthalpy(cell_t cell, Thread *thread,
                      double Temp, double density, double P, double yi[])
{
  double h = Temp * IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return h;      /* (J/Kg) */
}

#define TDatum 288.15
#define PDatum 1.01325e5

double IDEAL_entropy(cell_t cell, Thread *thread,
                     double Temp, double density, double P, double yi[])
{
  double s = IDEAL_specific_heat(cell, thread, Temp, density, P, yi) * log(fabs(Temp / TDatum)) +
             RGAS * log(fabs(PDatum / P));

  return s;      /* (J/Kg/K) */
  printf("IDEAL_entropy: %g\n", s);
  //return 186040.1;//Iwas testing something
}


double IDEAL_mw(double yi[])
{
  return MW;     /* (Kg/Kmol) */
}

double IDEAL_speed_of_sound(cell_t cell, Thread *thread,
                            double Temp, double density, double P, double yi[])
{
  double cp = IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
  return sqrt(Temp * cp * RGAS / (cp - RGAS)); /* m/s */
}

double IDEAL_viscosity(cell_t cell, Thread *thread,
                       double Temp, double density, double P, double yi[])
{
  double mu = 1.087e-05;//Methane CH4 *******************************************************************************************************
  return mu;     /* (Kg/m/s) */
}

double IDEAL_thermal_conductivity(cell_t cell, Thread *thread,
                                  double Temp, double density, double P,
                                  double yi[])
{
  double ktc = 0.0332; //Methane CH4 ************************************************************************************************
  return ktc;      /* W/m/K */
}
double IDEAL_rho_t(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. Temp at constant p */
  double rho_t = -density / Temp;
  return rho_t;     /* (Kg/m^3/K) */
}

double IDEAL_rho_p(cell_t cell, Thread *thread,
                   double Temp, double density, double P, double yi[])
{
  /* derivative of rho wrt. pressure at constant T */
  double rho_p = 1.0 / (RGAS * Temp);
  return rho_p;    /* (Kg/m^3/Pa) */
}

double IDEAL_enthalpy_t(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. Temp at constant p */
  return IDEAL_specific_heat(cell, thread, Temp, density, P, yi);
}

double IDEAL_enthalpy_p(cell_t cell, Thread *thread,
                        double Temp, double density, double P, double yi[])
{
  /* derivative of enthalpy wrt. pressure at constant T  */
  /* general form dh/dp|T = (1/rho)*[ 1 + (T/rho)*drho/dT|p] */
  /* but for ideal gas dh/dp = 0        */
  return 0.0 ;
}

UDF_EXPORT RGAS_Functions RealGasFunctionList =
{
  IDEAL_Setup,                   /* initialize */
  IDEAL_density,                 /* density */
  IDEAL_enthalpy,                /* enthalpy */
  IDEAL_entropy,                 /* entropy */
  IDEAL_specific_heat,           /* specific_heat */
  IDEAL_mw,                      /* molecular_weight */
  IDEAL_speed_of_sound,          /* speed_of_sound */
  IDEAL_viscosity,               /* viscosity */
  IDEAL_thermal_conductivity,    /* thermal_conductivity */
  IDEAL_rho_t,                   /* drho/dT |const p */
  IDEAL_rho_p,                   /* drho/dp |const T */
  IDEAL_enthalpy_t,              /* dh/dT |const p  */
  IDEAL_enthalpy_p               /* dh/dp |const T  */
};
/**************************************************************/
