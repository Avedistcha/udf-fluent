# UDRGM for One Specie (one Material) 

This section describes another example of a user-defined real gas model. You can use this example as the basis for your own UDRGM code. In this example, the Redlich-Kwong equation of state is used in the UDRGM. :rocket:

## User Defined Physical properties Used for our sample code

| Properties    | Method or Value |
| ------------- | ------------- |
| **Material:**  |  CO2  |
| **Equation of State:**  | Redlich-Kwong |
| **Cp:**  | 2222 (J/Kg/K)  |
| **speed_of_sound:**  | Ideal Gas SoS |
| **viscosity:**  | 1.087e-05 (Kg/m/s)  |
|**thermal_conductivity**  | 0.0332 (W/m/K)  |


## Returned Physical properties to Fluent
Multiple physical properties functions will be found in the UDF and these functions will end up returning the following physical properties to Fluent.

**density**\
**enthalpy**\
**entropy**\
**specific_heat**\
**molecular_weight**\
**speed_of_sound**\
**viscosity**\
**thermal_conductivity**\
**drho/dT |const p**\
**drho/dp |const T**\
**dh/dT |const p**\
**dh/dp |const T**

Under the function that return the above quatities one should write his costum model equation as follow.

### Loading the code using TUI
```
define/user-defined/real-gas-models/user-defined-real-gas-model
```

<img src="Images/rocket-launch.svg" width="300" height="300">
